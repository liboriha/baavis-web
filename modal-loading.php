<!-- LOADING MODAL -->
<div id="loadingModal" class="modal fade" data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body">

            <div class="text-center">
              <i class="fa fa-spin fa-spinner"></i>&nbsp;
              <h5 id="loadingText"></h5>
            </div>

          </div>
          <!-- end modal body -->

        </div>
      </div>
    </div>
    <!-- END LOADING MODAL -->

<script>
// SHOW LOADING MODAL ------------------------------------
function showLoadingModal(text) {
  // Show loading modal
  if(!text)
    text = "Probíhá načítání dat, prosím, počkejte...";
  document.getElementById("loadingText").innerHTML = text;
  $('#loadingModal').modal('show');
}

// HIDE LOADING MODAL ------------------------------------
function hideLoadingModal() {
  // Hide loading modal
  $('#loadingModal').modal('hide');
}
</script>
