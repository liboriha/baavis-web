  <div id="registrationModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="registrationModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button id="registrationModal_buttonClose" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="registrationModalLabel">Registrujte se</h4>
          <p>Vyplňte formulář a vytvořte si nový účet.</p>
          <p>Vyplňte všechno</p>
        </div>

        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="subhead-part-1__left-title">Registrace</h3>
                    <form id="formPremiumRegistrationNewUserModal" action="?sendRegistrationEmail">
                        <ul class="subhead-part-1-list clearfix">
                            <li class="subhead-part-1__item modal-li">
                                <select name="registration_select" id="registration_select">
                                    <option name="organizer" value="organizer">Organizátor</option>
                                    <option name="reader" value="reader">Čtenář</option>
                                </select>
                            </li>
                            <li class="subhead-part-1__item modal-li">
                                <input type="text" class="input-login" name="cFirstName" placeholder="Vaše jméno" required>
                            </li>
                            <li class="subhead-part-1__item modal-li">
                                <input type="text" class="input-login" name="cLastName" placeholder="Vaše příjmení" required>
                            </li>
                            <li class="subhead-part-1__item modal-li">
                                <input type="password" class="input-login" name="cPassword" placeholder="Heslo" required>
                            </li>
                            <li class="subhead-part-1__item modal-li">
                                <input type="password" class="input-login" name="cPasswordCheck" placeholder="Znovu vaše heslo pro kontrolu" required>
                            </li>
                            <li class="subhead-part-1__item modal-li">
                                <input type="text" class="input-login" name="cEmail" placeholder="Váš email" required>
                            </li>
                            <li>
                                <input type="checkbox" id="marketing_checkbox_modal" name="cMarketingAccept">
                                <input type="text" name="cUserRole" value="reader" hidden required>
                                <label for="marketing_checkbox_modal">Souhlasím s <a href="/assets/files/obchodni-podminky.pdf" target="_blank">podmínkami</a></label>
                                <br>
                                <input type="checkbox" id="gdpr_checkbox_modal" required name="cGDPRAccept">
                                <label for="gdpr_checkbox_modal">Souhlasím s <a href="/assets/files/gdpr.pdf" target="_blank">GDPR</a></label>
                            </li>
                            <li class="subhead-part-1__item modal-li">
                                <button type="button" onclick="registration()" class="universal-button universal-button--red">Zaregistrujte se</button>
<!--                                <button type="button" onclick="registerNewUser()" class="universal-button universal-button--red">Zaregistrujte se</button>-->
                            </li>
                        </ul>
                    </form>
                </div>
<!--                <div class="col-lg-6">
                    <h3 class="subhead-part-1__right-title">Registrace organizátora</h3>
                    <form id="formNotModalRegistrationNewUserModal" action="/registrationform" method="post">
                        <ul class="subhead-part-1-list clearfix">
                            <li class="subhead-part-1__item modal-li">
                                <input type="text" class="input-login" name="cFirstName" placeholder="Vaše jméno">
                            </li>
                            <li class="subhead-part-1__item modal-li">
                                <input type="text" class="input-login" name="cLastName" placeholder="Vaše příjmení">
                            </li>
                            <li class="subhead-part-1__item modal-li">
                                <input type="password" class="input-login" name="cPassword" placeholder="Heslo">
                            </li>
                            <li class="subhead-part-1__item modal-li">
                                <input type="password" class="input-login" name="cPasswordCheck" placeholder="Znovu vaše heslo pro kontrolu">
                            </li>
                            <li class="subhead-part-1__item modal-li">
                                <input type="text" class="input-login" name="cEmail" placeholder="Váš email">
                            </li>
                            <li>
                                <input type="checkbox" id="marketing_checkbox_o_modal" name="cMarketingAccept">
                                <input type="text" name="cUserRole" value="organizer" hidden required>
                                <label for="marketing_checkbox_o_modal">Souhlasím s <a href="/assets/files/obchodni-podminky.pdf" target="_blank">podmínkami</a></label>
                                <br>
                                <input type="checkbox" id="gdpr_checkbox_modal" required name="cGDPRAccept">
                                <label for="gdpr_checkbox_modal">Souhlasím s <a href="/assets/files/obchodni-podminky.pdf" target="_blank">GDPR</a></label>
                            </li>
                            <li class="subhead-part-1__item modal-li">
                                <button type="button" onclick="registerNewOrganizer()" class="universal-button universal-button--red">Zaregistrujte se</button>
                            </li>
                        </ul>
                    </form>
                </div>-->
            </div>
        </div>
        <!-- end modal body -->


        <!-- CLOSE BUTTON -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>
        </div>

      </div>
    </div>
  </div>
  <!-- END REGISTRATION MODAL -->

  <script>

function registration(){
    $('#registration_select select').each(function() {
        if ($(this).val() === 'organizer') {

                $('#formNotModalRegistrationNewUserModal').submit();

        }
        else{
            var regForm = document.getElementById("formPremiumRegistrationNewUserModal");
            if (!regForm.checkValidity()) {
                regForm.reportValidity();
            }
            else {
                $.ajax({
                    url: "registrationnewsave",
                    type: 'POST',
                    data: $("#formPremiumRegistrationNewUserModal").serialize(),
                    success: function (data) {
                        data = data.trim();
                        if (data.includes('ERROR:')) {
                            if(data.includes('Account already exists'))
                            {
                                alert('Přihlašovací jméno nebo email už existuje. Změňte a zkuste znova.');
                            }
                            else if(data.includes('PASSWORD'))
                            {
                                alert('Password nebyl zkontrolovan');
                            }
                            else
                            {
                                alert('Došlo k chybě na serveru - zkuste akci opakovat později.');
                            }
                        }
                        else {
                            alert("Váš účet byl vytvořen - během chvíle Vám přijde ověřovací email!");
                            $('#registrationModal_buttonClose').trigger('click');
                        }
                    }, error: function (e) {
                        alert("Něco se pokazilo, prosíme, zkuste akci opakovat!");
                    }
                });
            }
        }
    });
}







  </script>