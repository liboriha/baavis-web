<?php
use Parse\ParseException;
use Parse\ParseGeoPoint;
use Parse\ParseQuery;

/* Variables */

$banners = array();
    $query = new ParseQuery("Banners");
    $query->equalTo("show", true);

    // Find objects
    $bannerArray = $query->find();
    for ($i = 0; $i < count($bannerArray); $i++) {
        // Get Parse Object
        $bannerObject = $bannerArray[$i];
        $banners[$bannerObject->get('type')] = $bannerObject;
    }

$searchLocation = $_POST['searchLocation'];
if(empty($searchLocation) || is_null($searchLocation))
    $searchLocation = $_GET['searchLocation'];
$category = "";
$currDate = new DateTime();
$eventsCount= 0;
// QUERY EVENTS -----------------------------------
try {
if (isset($_GET["category"]))
    $category = $_GET["category"];

if (isset($_GET["eventsCount"]))
    $eventsCount = $_GET["eventsCount"];
$lng = null;
$lat = null;
// Find premium
$queryGetEvents = new ParseQuery("Events");
$queryGetEvents->greaterThanOrEqualTo("endDate", $currDate);

if(isset($category))
    $queryGetEvents->ascending("categoryPremiumSpot");
else
    $queryGetEvents->ascending("premiumSpot");
$queryGetEvents->ascending("startDate");

//Set search by location
if (!is_null($searchLocation) && !empty($searchLocation)) {
    $url = str_replace('<ADDRESS>', urlencode($searchLocation), GEOCODING_API_URL);
    $response = file_get_contents($url);
    $json = json_decode($response, true);

    $lat = $json['results'][0]['geometry']['location']['lat'];
    $lng = $json['results'][0]['geometry']['location']['lng'];

    if ($lat && $lng) {
        try {
            $searchGeoPoint = new ParseGeoPoint($lat, $lng);
            $queryGetEvents->withinKilometers('locationGps', $searchGeoPoint, 0);
        } catch (Exception $e) {
        }
    }
}
//Set category filter
if (!empty($category)) {
    $catFilter = new ParseQuery("EventCategories");
    $catFilter->equalTo("name", $category);
    $categoryArr = $catFilter->find();
}

if ($categoryArr) {
    $queryGetEvents->equalTo("categories", $categoryArr[0]);
}

if (EVENT_MUST_BE_ACCEPTED_BY_ADMIN) {
    $queryGetEvents->equalTo("isPending", false);
}

$evEventsArray = $queryGetEvents->find();
$evPremiumArray = [];
$evNonPremiumArray = [];

//Group premium and not premium events
for($i = 0; $i<count($evEventsArray);$i++) {
    $eObj = $evEventsArray[$i];
    $isPremiumPaid = $eObj->get('premiumSpotPayed');
    $isPremium = false;
    if (!$isPremiumPaid) {
        $evNonPremiumArray[] = $eObj;
        continue;
    }
    if ($isPremiumPaid) {
        if (!empty($category))
            $premiumMonthsArr = $eObj->get('paidPremiumCategory');
        else
            $premiumMonthsArr = $eObj->get('paidPremiumCommon');
        for ($j = 0; $j < count($premiumMonthsArr); $j++) {
            $premiumMonth = date("M-yyyy", strtotime($premiumMonthsArr[$j]));
            $currentMonth = $currDate->format("M-yyyy");
            if ($premiumMonth == $currentMonth) {
                $eObj->set('showAsPremium', true);
                $evPremiumArray[] = $eObj;
                $isPremium = true;
            }
            break;
        }
    }
    if(!$isPremium)
        $evNonPremiumArray[] = $eObj;
}
$premiumEventsCount = count($evPremiumArray);
$evArray = array_merge($evPremiumArray, $evNonPremiumArray);
?>

<script>
    $( document ).ready(function() {
        $('input[name=searchLocation]').val("<?= $searchLocation ?>");
        if($('input[name=searchLocation]').val()!=='') {
            $('#searchForm').css('display','none');
            $('#addressLabel').css('display','block');
            $('#addressLbl').text('<?=$searchLocation?>');
            $("#searchBar").css('width','auto');
        }
    });

</script>

<?php
    if(empty($evArray) || (is_null($lat) && is_null($lng) && !empty($searchLocation) && !is_null($searchLocation)))
    {
        echo 'No results was found ';
        // Find premium
        $queryGetEvents = new ParseQuery("Events");
        $queryGetEvents->greaterThanOrEqualTo("endDate", $currDate);

        $queryGetEvents->ascending("premiumSpot");
        $queryGetEvents->ascending("startDate");

        //Set category filter
        if (!empty($category)) {
            $catFilter = new ParseQuery("EventCategories");
            $catFilter->equalTo("name", $category);
            $categoryArr = $catFilter->find();
        }

        if ($categoryArr) {
            $queryGetEvents->equalTo("categories", $categoryArr[0]);
        }

        if (EVENT_MUST_BE_ACCEPTED_BY_ADMIN) {
            $evEventsArray->equalTo("isPending", false);
        }
        $evEventsArray = $queryGetEvents->find();
        $evPremiumArray = [];
        $evNonPremiumArray = [];
        //Group premium and not premium events
        for($i = 0; $i<count($evEventsArray);$i++) {
            $eObj = $evEventsArray[$i];
            $isPremiumPaid = $eObj->get('premiumSpotPayed');
            $isPremium = false;
            if (!$isPremiumPaid) {
                $evNonPremiumArray[] = $eObj;
                continue;
            }
            if ($isPremiumPaid) {
                if (!empty($category))
                    $premiumMonthsArr = $eObj->get('paidPremiumCategory');
                else
                    $premiumMonthsArr = $eObj->get('paidPremiumCommon');
                for ($j = 0; $j < count($premiumMonthsArr); $j++) {
                    $premiumMonth = date("M-yyyy", strtotime($premiumMonthsArr[$j]));
                    $currentMonth = $currDate->format("M-yyyy");

                    if ($premiumMonth == $currentMonth) {
                        $eObj->set('showAsPremium', true);
                        $evPremiumArray[] = $eObj;
                        $isPremium = true;
                    }
                    break;
                }
            }
            if (!$isPremium)
                $evNonPremiumArray[] = $eObj;
        }

        $evArray = array_merge($evPremiumArray,$evNonPremiumArray);
        $premiumEventsCount = count($evPremiumArray);
    }
    ?>
    <div class="section-content">
    <?php
    if(count($evArray)>6) {
        if ($eventsCount != 0)
            $count = 6;
        else
            $count = 5;
    }
    else
        $count = count($evArray);
    for ($i = 0; $i < $count; $i++) {
        // Get Parse Object
        $eObj = $evArray[$i];
        $eObjID = $eObj->getObjectId();

        // Get image
        $file = $eObj->get('image');
        $imageURL = $file->getURL();
        // Get title
        $title = $eObj->get('title');
        $title = substr($title, 0, 25);

        // Get location
        $location = $eObj->get('location');

        // Get cost
        $cost = $eObj->get('cost');

        // Get start date
        $sDate = $eObj->get('startDate');
        $startDate = date_format($sDate, "d.m.Y h:i\h");

        // Get end date
        $eDate = $eObj->get('endDate');
        $endDate = date_format($eDate, "d.m.Y h:i\h");

        $cat = $eObj->get('categories');

        // Get description
        $description = $eObj->get('description');
        $description = substr($description, 0, 80);

        $likesCount = $eObj->get("likes");

        $liked = false;

        $currentUser = \Parse\ParseUser::getCurrentUser();
        if(isset($currentUser)) {
            $query = new ParseQuery("UserEventLikes");
            $query->equalTo("user_id", $currentUser->getObjectId());
            $query->equalTo("event_id", $eObj->getObjectId());

            $result = $query->find();

            if (count($result) > 0) {
                $liked = true;
            }
        }
        if($i==$premiumEventsCount || ($i%6==0 && $i!=0))
        {
        ?>
            <!-- Advertisement cell -->
            <div class="event-tab">
            <?php
                    if (isset($banners['leaderboard-top'])) {
                        echo '<a href="//' . $banners['leaderboard-top']->get('link') . '"><img style="width:100%" src="' . $banners['leaderboard-top']->get('image')->getUrl() . '" alt="' . $banners['leaderboard-top']->get('alternative_text') . '" /></a>';
                    }?>
            </div><!-- end cell -->
        <? }


        $fromDate = date('d.m.Y H:i',strtotime($startDate));
        $toDate = date('d.m.Y H:i',strtotime($endDate));

        ?>
							<!-- Event cell -->
							<div class="<?php print ($eObj->get('showAsPremium'))? 'premium-event ': ' '; ?> event-tab">
                                <div class="premium-img"></div>
                                <img class="event-tab__image" src="<?php print $imageURL;?>">
                                <div class="event-tab-center">
                                    <a href="/eventdetail/<?php print $eObjID;?>"><h3 class="event-tab-center__title"><?php print $title;?></h3></a>
                                    <p class="event-tab-center__location">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i><?php print ' '.$location.' ';?>
                                       <span class="event-tab-center__time">
                                           <i class="fa fa-clock-o"></i><?php print ' '.$fromDate.' ';?> -
                                           <i class="fa fa-clock-o"></i><?php print ' '.$toDate;?>
                                       </span>
                                    </p>
                                    <p class="event-tab-center_p"><?php print $description;?></p>
                                </div>
                                <div class="event-tab-right">
                                    <a href="/eventdetail/<?php print $eObjID;?>"><button class="universal-button universal-button--red">Zobrazit detail</button></a>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-primary" style="margin: 3px" data-eventId="<?=$eObjID?>" data-likes="<?=$likesCount?>" data-like="<?=$liked?'true':'false'?>" onclick="likeBtnClick(this)"><i class="fa <?= $liked?'fa-check':'fa-heart-o'?>"></i> <?=$likesCount?></button>
                                </div><!-- end Event cell -->
                            </div>
<?php
    } // end FOR loop

    // error in query
} catch (ParseException $e) {echo $e->getMessage();}?>
        <?php if($eventsCount==0)
            {?>
        <input id="pageNumber" hidden value="0">
        <button class="btn btn-info" onclick="loadMore()">Načíst další <i class="fa fa-angle-down""></i></button>
        <?php } ?>
    </div>

<script>
    function likeBtnClick(e){
        const event_id = e.dataset.eventid;
        const like_count = parseInt(e.dataset.likes);
        const liked = e.dataset.like;

        $.ajax({
            url: "likeEvent",
            type: "get",
            data: {
                event:event_id,
                liked:liked
            },
            contentType:false,
            success: function (data) {
                if(data === "OK") {
                    console.log(liked);
                    if(liked === "true")
                    {
                        e.dataset.likes = like_count - 1;
                        e.dataset.liked = "false";
                        e.innerHTML = "<i class=\"fa fa-heart-o\"></i> " + e.dataset.likes;
                    }
                    else
                    {
                        e.dataset.likes = like_count + 1;
                        e.dataset.liked = "true";
                        e.innerHTML = "<i class=\"fa fa-check\"></i> " + e.dataset.likes;
                    }
                }
                else if (data==="NOTLOGGED")
                {
                    $('#loginModal').modal('show');
                }
            }, error: function (e) {
                alert("Něco se pokazilo, prosíme, zkuste akci opakovat! ");
                console.log(e);
            }
        });
    }

    function loadMore(){
        var pageNumber = Number($("#pageNumber").val());
        var category = $('.menu-categories-list').find('li.active').text();
        var searchLocation = getQueryVariable('searchLocation');
        if(searchLocation === '')
            searchLocation = $('input[name=searchLocation]').val();
        if (category=='Všechny')
            category = '';
        var loadIcon = $("#loader");
        loadIcon.modal('show');
        $.ajax({
            url: "showMoreEvents",
            type: 'POST',
            datatype:'html',
            data: {
                loadMore:true,
                pageNumber:pageNumber,
                eventsCount:6,
                searchLocation:searchLocation,
                category: category
            },
            success: function (data) {
                console.log(data);
                $(data).insertBefore($("#pageNumber"));
                FB.XFBML.parse();
            }, error: function (e) {
                console.log(e);
            },
            complete: function(){
                loadIcon.modal('hide');
            }
        });
        $("#pageNumber").val(pageNumber+1);
    }
</script>