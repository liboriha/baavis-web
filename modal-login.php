<?php
use Parse\ParseException;
use Parse\ParseUser;

if (AJAXREQUEST) {
    if (isset($_POST['login'])) {
        $login = $_POST['login'];
        $password = $_POST['password'];
        if (empty($login) || empty($password)) {
            die('ERROR:NOLOGINORPASSWORD');
        }

        try {
            $user = ParseUser::logIn($login, $password);
            if ($user->get('firstname') && $user->get('lastname')) {
                $isActivated = $user->get('emailVerified');
                if ($isActivated) {
                    die('OK:' . $user->get('firstname') . ':' . $user->get('lastname') . ':' . $user->get('photo') . ':' . $user->get('userrole'));
                }
                else {
                    $user->logOut();
                    die("ERROR:NOTVERIFIED");
                }
            } else {
                die('ERROR:USERNOTFOUND');
            }

        } catch (ParseException $e) {
            die('ERROR:USERNOTFOUND'.$e);
        }
    } elseif (isset($_POST['email'])) {
        //Send reset password mail
        $email = $_POST['email'];
        if (empty($email)) {
            die("ERROR:NOEMAILSEND");
        }

        try {
            ParseUser::requestPasswordReset($email);
            die("OK");
        } catch (ParseException $ex) {
            {
                die("ERROR:SOMETHINGWRONG");
            }
        }
    }
}
else {
    ?>
  <!-- LOGIN MODAL -->
  <script>
  function loginUser() {
      // Attend
      $.ajax({
          url: "/login",
          type: 'POST',
          data: {
              'login': $('#formLogin input[name=cLogin]').val(),
              'password': $('#formLogin input[name=cPassword]').val()
          },
          success: function (data) {
              data = data.trim();
              if (data.substring(0, 6) == 'ERROR:') {
                  switch (data.substring(6)) {
                      case'NOLOGINORPASSWORD': {
                          alert('Je třeba vyplnit přihlašovací jméno i heslo.');
                          break;
                      }
                      case 'USERNOTFOUND': {
                          console.log(data);
                          alert('Bohužel uživatel s těmito přihlašovacími údaji nebyl nalezen.');
                          break;
                      }
                      case 'NOTVERIFIED': {
                          alert('Musíte ověřovat svúj email!');
                          break;
                      }
                      default: {
                          console.log(data);
                          alert('Bylo zadané špatné uživatelské jméno nebo heslo.');
                          break;
                      }
                  }
              }
              else {
                  tmp = data.split(':');
                  userLoggedIn(tmp[1], tmp[2], tmp[3], tmp[4]);
                  changeFormType(false);
                  $('#loginModal_buttonClose').trigger('click');
                  location.reload();
              }
          },
          error: function (e) {
              alert("Něco se pokazilo, prosíme, zkuste akci opakovat později.");
          }
      });
  }
  </script>
  <div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button id="loginModal_buttonClose" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="loginModalLabel">Přihlašte se</h4>
          <P id="titleText">Zadejte svoje přihlašovací údaje.</p>
        </div>

        <div class="modal-body" id="loginFormDiv">

          <!-- LOGIN FORM -->
          <form id="formLogin" class="form-horizontal">

            <!-- YOUR FIRST NAME -->
            <div class="form-group">
              <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                <strong>Vaše emailová adresa</strong>
                <br>
                <input type="text" class="form-control" name="cLogin" placeholder="Vaše přihlašovací jméno">
              </div>
            </div>

            <!-- YOUR PASSWORD -->
            <div class="form-group">
              <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                <strong>Vaše heslo</strong>
                <br>
                <input type="password" class="form-control" name="cPassword" placeholder="Vaše heslo">
              </div>
            </div>

            <!-- LOGIN BUTTON -->
            <div class="form-group">
              <div class="text-center">
                <button type="button" class="btn btn-success" onclick="loginUser()">PŘIHLÁSIT</button>
                <button type="button" class="btn btn-primary" onclick="changeFormType()">Nemůžete se přihlásit?</button>
              </div>
            </div>

          </form>
          <!-- END LOGIN FORM -->


        </div>
        <!-- end modal body -->
        <!-- Reset password modal body -->
          <div class="modal-body" id="resetPasswordDiv" style="display: none">
              <form id="resetPasswordForm" class="form-horizontal">
                  <!-- YOUR E-MAIL -->
                  <div class="form-group">
                      <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                          <strong>VAŠ E-MAIL</strong>
                          <br>
                          <input type="text" class="form-control" name="cEmail" placeholder="Vaš e-mail" required>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="text-center">
                          <button type="button" class="btn btn-success" onclick="resetPasswordByEmail()">Pokračovat</button>
                          <button type="button" class="btn btn-primary" onclick="changeFormType(false)">Vrátit se</button>
                      </div>
                  </div>
              </form>
          </div>

        <!-- CLOSE BUTTON -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>
        </div>

      </div>
    </div>
  </div>
  <!-- END LOGIN MODAL -->
  <?php
}
?>

<script>
    function changeFormType(showResetPwdForm = true) {
        if (showResetPwdForm) {
            $("#loginFormDiv").hide();
            $("#resetPasswordDiv").show();
            $("#loginModalLabel").text("Zapomenuté heslo");
            $("#titleText").text("Pokusíme se Vám vrátit přístup do účtu.");
        }
        else {
            $("#loginFormDiv").show();
            $("#resetPasswordDiv").hide();
            $("#loginModalLabel").text("Přihlašte se");
            $("#titleText").text("Zadejte svoje přihlašovací údaje.");
        }

    }
    function resetPasswordByEmail(){
        var resPasForm = document.getElementById("resetPasswordForm");
        if (!resPasForm.checkValidity()) {
            resPasForm.reportValidity();
        }
        else {
            $.ajax({
                url: "/login",
                type: 'POST',
                data: {
                    'email': $('#resetPasswordForm input[name=cEmail]').val()
                },
                success: function (data) {
                    data = data.trim();
                    if (data.substring(0, 6) == 'ERROR:') {
                        alert('Tento e-mail není přidělen k žádnému existujícímu účtu!');
                    }
                    else {
                        alert("Na vás email jsme poslali linku");
                        $('#loginModal_buttonClose').trigger('click');
                    }
                },
                error: function (e) {
                    alert("Něco se pokazilo, prosíme, zkuste akci opakovat později.");
                }
            });
        }
    }
        $('#loginModal').on('hidden.bs.modal', function(event) {
            changeFormType(false);
        });


</script>