<?php
use Parse\ParseException;
use Parse\ParseGeoPoint;
use Parse\ParseQuery;

try {

    $banners = array();
    $query = new ParseQuery("Banners");
    $query->equalTo("show", true);

    // Find objects
    $bannerArray = $query->find();
    for ($i = 0; $i < count($bannerArray); $i++) {
        // Get Parse Object
        $bannerObject = $bannerArray[$i];
        $banners[$bannerObject->get('type')] = $bannerObject;
    }

    $currDate = new DateTime();
    $eventsCount = 0;
// QUERY EVENTS -----------------------------------
    if (isset($_GET["category"]))
        $category = $_GET["category"];
    $lng = null;
    $lat = null;
// Find premium
    $queryGetEvents = new ParseQuery("Events");
    $queryGetEvents->greaterThanOrEqualTo("endDate", $currDate);

    $queryGetEvents->ascending("premiumSpot");
    $queryGetEvents->ascending("startDate");

    $queryGetEvents->limit(5);
//Set category filter
    if (!empty($category)) {
        $catFilter = new ParseQuery("EventCategories");
        $catFilter->equalTo("name", $category);
        $categoryArr = $catFilter->find();
    }

    if ($categoryArr) {
        $queryGetEvents->equalTo("categories", $categoryArr[0]);
    }
    $evEventsArray = $queryGetEvents->find();
    $evPremiumArray = [];
    $evNonPremiumArray = [];

    // echo '<pre>';
    // var_dump($evEventsArray);
    // echo '</pre>';

//Group premium and not premium events
    for ($i = 0; $i < count($evEventsArray); $i++) {
        $eObj = $evEventsArray[$i];
        $isPremiumPaid = $eObj->get('premiumSpotPayed');
        $isPremium = false;
        if (!$isPremiumPaid) {
            $evNonPremiumArray[] = $eObj;
            continue;
        }
        if ($isPremiumPaid) {

            if (!empty($category)) {
                $premiumMonthsArr = $eObj->get('paidPremiumCategory');
            } else {
                $premiumMonthsArr = $eObj->get('paidPremiumCommon');
            }

            for ($j = 0; $j < count($premiumMonthsArr); $j++) {
                $premiumMonth = date("d-m-y", strtotime($premiumMonthsArr[$j]));
                $currentMonth = $currDate->format("d-m-y");

                // echo $eObj->title;
                // echo '<br>';
                // echo '$premiumMonth';
                // echo '<br>';
                // var_dump($premiumMonth);
                // echo '<br>';

                // echo '$currentMonth';
                // echo '<br>';
                // var_dump($currentMonth);
                // echo '<br>';
                // echo '<br>';

                if ($premiumMonth == $currentMonth) {
                    $eObj->set('showAsPremium', true);
                    $evPremiumArray[] = $eObj;
                    $isPremium = true;
                }

                // break;
            }

            // echo '<br>';
            // echo '<br>';
            // echo '---';
            // echo '<br>';
            // echo '<br>';
        }
        if (!$isPremium)
            $evNonPremiumArray[] = $eObj;
    }
    $evArray = array_merge($evPremiumArray, $evNonPremiumArray);
    $premiumEventsCount = count($evPremiumArray);
}
catch (Exception $exception)
{}
    ?>
<div class="section-event">
        <div class="section-container">
            <div class="section-content">



                <?php
                $count = count($evArray);
                for ($i = 0; $i < $count; $i++) {
                // Get Parse Object
                $eObj = $evArray[$i];
                $eObjID = $eObj->getObjectId();

                // Get image
                $file = $eObj->get('image');
                $imageURL = $file->getURL();

                // Get title
                $title = $eObj->get('title');
                $title = substr($title, 0, 25);

                // Get location
                $location = $eObj->get('location');

                // Get cost
                $cost = $eObj->get('cost');

                // Get start date
                $sDate = $eObj->get('startDate');
                $startDate = date_format($sDate, "d.m.Y h:i\h");

                // Get end date
                $eDate = $eObj->get('endDate');
                $endDate = date_format($eDate, "d.m.Y h:i\h");

                $cat = $eObj->get('categories');

                // Get description
                $description = $eObj->get('description');
                $description = substr($description, 0, 80);

                $likesCount = $eObj->get("likes");
                $liked = false;

                $currentUser = \Parse\ParseUser::getCurrentUser();
                if(isset($currentUser)) {
                    $query = new ParseQuery("UserEventLikes");
                    $query->equalTo("user_id", $currentUser->getObjectId());
                    $query->equalTo("event_id", $eObj->getObjectId());

                    $result = $query->find();

                    if (count($result) > 0) {
                        $liked = true;
                    }
                }

                if($i==$premiumEventsCount || ($i%6==0 && $i!=0))
                {
                ?>
                    <!-- Advertisement cell -->
                    <div class="event-tab">
                        <?php
                        $statusTop = intval($banners['leaderboard-top']->get("status"));

                        if($statusTop==1){
                            echo '<div style="color:' . $banners['leaderboard-top']->get("barva_textu") . '; background-image: linear-gradient(to bottom right, ' . $banners['leaderboard-top']->get("barva_pozadi_1") . ',' . $banners['leaderboard-top']->get("barva_pozadi_2") . ');font-size: 40px;font-weight: 700;padding: 30px;">';
                            echo $banners['leaderboard-top']->get("text");
                            echo "</div>";
                        }
                        else{
                            if (isset($banners['leaderboard-top'])) {
                                echo '<a href="//' . $banners['leaderboard-top']->get('link') . '"><img style="width:100%" src="' . $banners['leaderboard-top']->get('image')->getUrl() . '" alt="' . $banners['leaderboard-top']->get('alternative_text') . '" /></a>';
                            }
                        }
                        ?>
                    </div>
                <? }

                    $fromDate = date('d.m.Y H:i',strtotime($startDate));
                    $toDate = date('d.m.Y H:i',strtotime($endDate));

                    ?>
                    <!-- Event cell -->
                    <div class="<?php print ($eObj->get('showAsPremium'))? 'premium-event ': ' '; ?> event-tab">
                        <div class="premium-img"></div>
                        <img class="event-tab__image" src="<?php print $imageURL;?>">
                        <div class="event-tab-center">
                            <a href="/eventdetail/<?php print $eObjID;?>"><h3 class="event-tab-center__title"><?php print $title;?></h3></a>
                            <p class="event-tab-center__location">
                                <i class="fa fa-map-marker" aria-hidden="true"></i><?php print ' '.$location.' ';?>
                                <span class="event-tab-center__time">
                                           <i class="fa fa-clock-o"></i><?php print ' '.$fromDate.' ';?> -
                                           <i class="fa fa-clock-o"></i><?php print ' '.$toDate;?>
                                       </span>
                        </p>
                        <p class="event-tab-center_p"><?php print $description;?></p>
                    </div><!-- end panel body -->
                    <div class="event-tab-right">
                        <a href="/eventdetail/<?php print $eObjID;?>"><button class="universal-button universal-button--red">Zobrazit detail</button></a>
                        <div class="clearfix"></div>
                        <button class="btn btn-primary" style="margin: 3px" data-eventId="<?=$eObjID?>" data-likes="<?=$likesCount?>" data-liked="<?=$liked?>" onclick="likeBtnClick(this)"><i class="fa <?= $liked?'fa-check':'fa-heart-o'?>"></i> <?=$likesCount?></button>
                    </div><!-- end Event cell -->
                </div>

            <?php } ?>
            </div>
            <button class="btn btn-info" onclick="showAll()">Zobrazit další</button>
            <script>
                function showAll(){
                    location.href = '/eventsList';
                }

                function likeBtnClick(e){
                    const event_id = e.dataset.eventid;
                    const like_count = parseInt(e.dataset.likes);
                    const liked = e.dataset.liked;

                    $.ajax({
                        url: "likeEvent",
                        type: "get",
                        data: {
                            event:event_id,
                            liked:liked
                        },
                        contentType:false,
                        success: function (data) {
                            if(data === "OK") {
                                if(liked === "true")
                                {
                                    e.dataset.likes = like_count - 1;
                                    e.dataset.liked = "false";
                                    e.innerHTML = "<i class=\"fa fa-heart-o\"></i> " + e.dataset.likes;
                                }
                                else
                                {
                                    e.dataset.likes = like_count + 1;
                                    e.dataset.liked = "true";
                                    e.innerHTML = "<i class=\"fa fa-check\"></i> " + e.dataset.likes;
                                }
                            }
                            else if (data==="NOTLOGGED")
                            {
                                $('#loginModal').modal('show');
                            }
                        }, error: function (e) {
                            alert("Něco se pokazilo, prosíme, zkuste akci opakovat! ");
                            console.log(e);
                        }
                    });
                }
            </script>
        </div>
</div>
<div class="subhead-part-3 content-width">
    <h2 class="subhead-part-3__title">Aplikace ke stažení</h2>
    <a href="#"><img src="./assets/img/google-play-coming-soon.png" alt="app-img-1" class="subhead-part-3__image"></a>
    <a href="#"><img src="./assets/img/appstore-coming-soon.png" alt="app-img-2" class="subhead-part-3__image"></a>
</div>