<?php
require_once './configs.php';

use Parse\ParseQuery;
use Parse\ParseUser;

$admin = ParseUser::logIn('admin', 'admin');


function SendEmailEventAccepted()
{
    // Select all events which are newly accepted
    $query = new ParseQuery('Events');
    $query->equalTo('isPending', false); // Event is accepted by Admin
    $query->notEqualTo('emailEventAccepted', true); // Email not sent yet
    $query->includeKey('owner'); // Include owner object
    $query->includeKey('owner.mailAddress'); // Include owner email
    $result = $query->find();

    for ($i = 0; $i < count($result); $i++) {
        $event = $result[$i];
        $owner = $event->get('owner');

        // Prepare email
        $to = $owner->get('mailAddress');
        $subject = "UDÁLOSTI | Událost akceptována administrátorem";
        $message = "Událost '" . $event->get('title') . "' byla akceptována administrátorem webu.";
        $headers = "From: " . ADMIN_EMAIL;
        // Send email
        $emailSent = mail($to, $subject, $message, $headers);
        if ($emailSent) {
            print('MAIL SENT SUCCESSFULY'."\n");
            // If email sent, mark event flag as true
            $event->set('emailEventAccepted', true);
            $event->save();
        }
    }
}

function SendEmailReminderToAttenders()
{
    $minStartDate = new DateTime();
    $minStartDate->setTimestamp(mktime(0, 0, (EVENT_EMAIL_REMINDER_DAYS_BEFORE * 86400), date("m"), date("d"), date("Y")));

    // Select all users which are registered attenders for upcomming events
    $query = new ParseQuery('Events');
    $query->equalTo('isPending', false); // Event is accepted by Admin
    $query->notEqualTo('emailEventReminder', true); // Email not sent yet
    $query->lessThanOrEqualTo('startDate', $minStartDate); // Event starts in EVENT_EMAIL_REMINDER_DAYS_BEFORE days
    // $query->exists('attenders');
    $query->includeKey('attenders'); // Include attenders objects
    $query->includeKey('attenders.mailAddress'); // Include attenders objects email
    // $query->greaterThan('attenders', null); // Email address

    $result = $query->find();

    for ($i = 0; $i < count($result); $i++) {
        $event = $result[$i];
        $queryAttenders = $event->getRelation('attenders')->getQuery();
        $attenders = $queryAttenders->find();

        if(count($attenders) < 1)
            continue;

        // Prepare email
        $to = '';
        for ($j = 0; $j < count($attenders); $j++) {
            $to .= $attenders[$j]->get('mailAddress') . ', ';
        }
        if (strlen($to) > 0) {
            $to = substr($to, 0, -2);
        }

        $subject = "UDÁLOSTI | Připomenutí události, které se chcete zúčastnit";
        $message = "Událost '" . $event->get('title') . "' začíná " . date_format($event->get('startDate'), "d.m.Y h:i\h"). ".";
        $headers = "From: " . ADMIN_EMAIL;

        // Send email
        $emailSent = mail($to, $subject, $message, $headers);

        if ($emailSent) {
            // If email sent, mark event flag as true
            $event->set('emailEventReminder', true);
            $event->save();
        }
    }
}

function SendEmailAskForReview()
{
    $maxEndDate = new DateTime();
    $maxEndDate->setTimestamp(mktime(0, 0, (EVENT_EMAIL_REVIEW_DAYS_AFTER * 86400), date("m"), date("d"), date("Y")));

    // Select all users which went to any events
    $query = new ParseQuery('Events');
    $query->equalTo('isPending', false); // Event is accepted by Admin
    $query->notEqualTo('emailEventAskForReview', true); // Email not sent yet
    $query->lessThanOrEqualTo('endDate', $maxEndDate); // Event ended EVENT_EMAIL_REVIEW_DAYS_AFTER days ago
    $query->includeKey('attenders'); // Include attenders objects
    $result = $query->find();

    for ($i = 0; $i < count($result); $i++) {
        $event = $result[$i];
        $attenders = $event->get('attenders');

        // Prepare email
        $to = '';
        for ($j = 0; $j < count($attenders); $j++) {
            $to .= $attenders[$j]->get('mailAddress') . ', ';
        }
        if (strlen($to) > 0) {
            $to = substr($to, 0, -2);
        }

        $subject = "UDÁLOSTI | Prosíme o ohodnocení události";
        $message = "Zúčastnil(a) jste se události '" . $event->get('title') . "'. Prosíme o její ohodnocení a případně komentář na tomto odkazu (" . URL_BASE . '/attendantreview/' . $event->get('objectId') . ").";
        $headers = "From: " . ADMIN_EMAIL;

        // Send email
        $emailSent = mail($to, $subject, $message, $headers);

        if ($emailSent) {
            // If email sent, mark event flag as true
            $event->set('emailEventAskForReview', true);
            $event->save();
        }
    }
}

function SendEmailWithReviewsLink()
{
    $maxEndDate = new DateTime();
    $maxEndDate->setTimestamp(mktime(0, 0, (EVENT_EMAIL_INFORM_OWNER_ABOUT_REVIEWS_DAYS_AFTER * 86400), date("m"), date("d"), date("Y")));

    // Select all events which already ended
    $query = new ParseQuery('Events');
    $query->equalTo('isPending', false); // Event is accepted by Admin
    $query->notEqualTo('emailEventReviewsLink', true); // Email not sent yet
    $query->lessThanOrEqualTo('endDate', $maxEndDate); // Event ended EVENT_EMAIL_INFORM_OWNER_ABOUT_REVIEWS_DAYS_AFTER days ago
    $query->includeKey('owner'); // Include owner object
    $result = $query->find();

    for ($i = 0; $i < count($result); $i++) {
        $event = $result[$i];
        $owner = $event->get('owner');

        // Prepare email
        $to = $owner->get('mailAddress');
        $subject = "UDÁLOSTI | Hodnocení Vaší události";
        $message = "Hodnocení účastníků Vaší události '" . $event->get('title') . "' si můžete projít na tomto odkazu ()" . URL_BASE . '/eventreviews/' . $event->get('objectId') . ").";
        $headers = "From: " . ADMIN_EMAIL;

        // Send email
        $emailSent = mail($to, $subject, $message, $headers);

        if ($emailSent) {
            // If email sent, mark event flag as true
            $event->set('emailEventReviewsLink', true);
            $event->save();
        }
    }
}

//SendEmailEventAccepted();
//SendEmailReminderToAttenders();
//SendEmailAskForReview();
//SendEmailWithReviewsLink();