<?php
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseRole;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseSessionStorage;

function saveNewUser()
{
    $firstname = $_POST['cFirstName'];
    $lastname = $_POST['cLastName'];
    $username = $_POST['cEmail'];
    $email = $_POST['cEmail'];
    $password = $_POST['cPassword'];
    $passwordcheck = $_POST['cPasswordCheck'];
    $country = $_POST['cCountry'];
    $zip = $_POST['cZip'];
    $city = $_POST['cCity'];
    $street = $_POST['cStreet'];
    $streetnumber = $_POST['cStreetNumber'];
    $userrole = $_POST['cUserRole'];
    $ICO='';
    $DIC='';
    $firmName = '';
    $marketingAccept = boolval($_POST['cMarketingAccept']);

    if($password!=$passwordcheck)
    {
        echo 'ERROR:PASSWORD';
        return;
    }

    if(!empty($_POST["cICO"]))
    {
        $ICO = $_POST["cICO"];
        $DIC = $_POST["cDIC"];
        $firmName = $_POST['cFirmName'];
    }

    $eUserNew = new ParseUser();

    // Set data
    $eUserNew->set("firstname", $firstname);
    $eUserNew->set("lastname", $lastname);
    $eUserNew->set('username', $username);
    $eUserNew->set("password", $password);
    $eUserNew->set("email", $email);
    $eUserNew->set("mailAddress", $email);
    $eUserNew->set("country", $country);
    $eUserNew->set("zip", $zip);
    $eUserNew->set("city", $city);
    $eUserNew->set("street", $street);
    $eUserNew->set("streetNumber", $streetnumber);
    $eUserNew->set("ico",$ICO);
    $eUserNew->set("dic",$DIC);
    $eUserNew->set("firmName",$firmName);
    $eUserNew->set("marketingAccepted",$marketingAccept);

    $eUserNew->set("userrole", $userrole);

    // Saving block
    try {
        $eUserNew->signUp();


        // Registration successfull
        echo 'OK';
        $eUserNew->logOut();
        try {
            // SEND EMAIL NOTIFICATION TO THE ADMIN
            $to = ADMIN_EMAIL;
            $subject = "UDÁLOSTI | Přidání nového uživatele!";
            $message = "Nový uživatel '" . $firstname . " " . $lastname . "' se zaregistroval";
            $headers = "From: $email";
            mail($to, $subject, $message, $headers);

            // error  on saving
        } catch (ParseException $ex) {
            echo 'ERROR:EMAIL';
        }
    } catch (ParseException $ex) {
        echo 'ERROR:REGISTRATION';
        echo "\n" . print_r($ex, true);
    }
}


// UPLOAD IMAGE ------------------------------------------
function uploadImage()
{
    // generate a unique random string
    $randomStr = generateRandomString();
    // upload image into the 'uploads' folder
    move_uploaded_file($_FILES["file"]["tmp_name"], PATH_IMAGE_USERS . $randomStr . ".jpg");

    // echo the link of the uploaded image
    echo PATH_IMAGE_USERS . $randomStr . ".jpg";
}

// GENERATE A RANDOM STRING ---------------------------------------
function generateRandomString($length = 15)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function saveUserImage($file_name)
{
    // Crop image to max dimension = 300px
    $maxDim = 600;

    list($width, $height, $type, $attr) = getimagesize($file_name);

    if ($width > $maxDim || $height > $maxDim) {
        $target_filename = $file_name;
        $ratio = $width / $height;
        if ($ratio > 1) {
            $new_width = $maxDim;
            $new_height = $maxDim / $ratio;
        } else {
            $new_width = $maxDim * $ratio;
            $new_height = $maxDim;
        }

        $src = imagecreatefromstring(file_get_contents($file_name));
        $dst = imagecreatetruecolor($new_width, $new_height);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        imagedestroy($src);
        imagepng($dst, $target_filename);
        imagedestroy($dst);

        uploadImage();

    } else {
        uploadImage();
    }
}

if (AJAXREQUEST) {
    // Resolve AJAX request
    if (isset($_FILES['file'])) {
        if ($_FILES['file']['tmp_name']) {
            if ($_FILES["file"]["error"] > 0) {
                echo "ERROR:PHOTO";
            } else {
                saveUserImage($_FILES['file']['tmp_name']);
            }
        }
    } elseif (isset($_POST['cFirstName'])
    ) {
        saveNewUser();
    } else {
        echo 'ERROR:VALUES';
    }
}
else {
    // Show page

// Send AJAX reqistration request
    // OK
    // <div class="text-center">
    // <div class="alert alert-success">Váš účet byl vytvořen - během chvíle Vám přijde ověřovací email!</div>
    // </div>

    // ERROR:REGISTRATION
    //   <div class="text-center">
    //   <div class="alert alert-danger">
    //          <em class="fa fa-exclamation"></em>Registrace se nezdařila!
    //        </div>
    //  </div>

    // ERROR:EMAIL
    //   <div class="text-center">
    //   <div class="alert alert-danger">
    //          <em class="fa fa-exclamation"></em>Nepodařilo se odeslat informační mail adminovi!
    //        </div>
    //  </div>

    // ERROR:VALUES
    //   <div class="text-center">
    //   <div class="alert alert-danger">
    //          <em class="fa fa-exclamation"></em>Musíte vyplnit všechna pole formuláře!
    //        </div>
    //  </div>

// SEND REGISTRATION EMAIL -------------------------------------------------------
    if (isset($_GET['cName'])
        && isset($_GET['cEmail'])
        && isset($_GET['cMessage'])
    ) {
        sendRegistrationEmail();
    }

    $firstName = '';
    $lastName = '';
    $email = '';
    $pwd = '';
    $pwdCheck = '';
    $marketingAccept = false;
    $userRole = 'organizer';
    if(isset($_POST['cFirstName']))
    {
        $firstName = $_POST['cFirstName'];
        $lastName = $_POST['cLastName'];
        $email = $_POST['cEmail'];
        $pwd = $_POST['cPassword'];
        $pwdCheck = $_POST['cPasswordCheck'];
        $marketingAccept = boolval($_POST['cMarketingAccept']);
        $userRole = $_POST['cUserRole'];
    }

    function sendRegistrationEmail()
    {
        $name = $_GET['cName'];
        $email = $_GET['cEmail'];
        $mess = $_GET['cMessage'];

        if ($name != ''
            && $email != ''
            && $mess != '') {

            // SEND EMAIL NOTIFICATION TO THE ADMIN (YOU)
            $to = ADMIN_EMAIL;
            $subject = "UDÁLOSTI | Zpráva z registračního formuláře od $name";
            $message = $mess;
            $headers = "From: $email";
            mail($to, $subject, $message, $headers);

            echo '
			<div class="text-center">
		  		<div class="alert alert-info">Děkujeme za zaslání zprávy, ozveme se Vám jak nejdříve to bude možné!</div>
			</div>
		';

            // You must fill all the fields!
        } else {
            echo '
		 <div class="text-center">
		 	<div class="alert alert-danger">
            	<em class="fa fa-exclamation"></em>Musíte vyplnit všechna pole formuláře<em class="fa fa-exclamation"></em>
            </div>
		</div>
		 ';

        }
    }
    ?>

    <!-- REGISTRATION MODAL -->
    <script>
        function registerUser() {
            var regForm = document.getElementById("formRegistrationNewUser");
            if (!regForm.checkValidity()) {
                regForm.reportValidity();
            }
            else {
                $.ajax({
                    url: "registrationnewsave",
                    type: 'POST',
                    data: $("#formRegistrationNewUser").serialize(),
                    success: function (data) {
                        data = data.trim();
                        if (data.includes('ERROR:')) {
                            if(data.includes('Account already exists'))
                            {
                                alert('Přihlašovací jméno nebo email už existuje. Změňte a zkuste znova.');
                            }
                            else if(data.includes('PASSWORD'))
                            {
                                alert('Password nebyl zkontrolovan');
                            }
                            else
                            {
                                alert('Došlo k chybě na serveru - zkuste akci opakovat později.');
                            }
                        }
                        else {
                            alert("Váš účet byl vytvořen - během chvíle Vám přijde ověřovací email!");
                            $('#registrationModal_buttonClose').trigger('click');
                        }
                    }, error: function (e) {
                        console.log(e);
                        alert("Něco se pokazilo, prosíme, zkuste akci opakovat!");
                    }
                });
            }
        }
    </script>
    <!-- REGISTRATION FORM -->
    <form id="formRegistrationNewUser" class="form-horizontal" action="?sendRegistrationEmail">
        <!-- YOUR FIRST NAME -->
        <div class="form-group">
            <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                <strong>VAŠE JMÉNO</strong>
                <br>
                <input type="text" class="form-control" name="cFirstName" placeholder="Vaše jméno" <?php echo !empty($firstName)?'value="'.$firstName.'"':''?> required>
            </div>
        </div>

        <!-- YOUR LAST NAME -->
        <div class="form-group">
            <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                <strong>VAŠE PŘÍJMENÍ</strong>
                <br>
                <input type="text" class="form-control" name="cLastName" placeholder="Vaše příjmení" value="<?=$lastName?>" required>
            </div>
        </div>

        <!-- YOUR LOGIN
        <div class="form-group">
            <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                <strong>VAŠE PŘIHLAŠOVACÍ JMÉNO</strong>
                <br>
                <input type="text" class="form-control" name="cLogin" placeholder="Vaše přihlašovací jméno" required>
            </div>
        </div>
        -->

        <!-- YOUR PASSWORD -->
        <div class="form-group">
            <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                <strong>VAŠE HESLO</strong>
                <br>
                <input type="password" class="form-control" name="cPassword" placeholder="Heslo" value="<?=$pwd?>" required>
            </div>
        </div>

        <!-- YOUR PASSWORD - SECOND TIME -->
        <div class="form-group">
            <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                <strong>ZNOVU VAŠE HESLO PRO KONTROLU</strong>
                <br>
                <input type="password" class="form-control" name="cPasswordCheck" placeholder="Znovu vaše heslo pro kontrolu" value="<?=$pwdCheck?>" required>
            </div>
        </div>

        <!-- YOUR EMAIL -->
        <div class="form-group">
            <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                <strong>VÁŠ EMAIL</strong>
                <br>
                <input type="text" class="form-control" name="cEmail" placeholder="Vaše emailová adresa" <?php echo !empty($email)?'value="'.$email.'"':''?> required>
            </div>
        </div>

        <!-- Fizická nebo právnická osoba -->
        <div class="form-group">
            <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                <strong>Typ osoby</strong>
                <select class="form-control" onchange="changeOrganizerType();">
                    <option>
                        Právnická osoba
                    </option>
                    <option>
                        Fyzická osoba
                    </option>
                </select>
                <script>
                    function changeOrganizerType(){
                        const firmNameDiv = document.getElementById('firmName');
                        const icoDiv = document.getElementById('ICO');
                        const dicDiv = document.getElementById('DIC');

                        const firmName = document.querySelector('input[name="cFirmName]');
                        const ico = document.querySelector('input[name="cICO]');
                        const dic = document.querySelector('input[name="cDIC]');

                        const display = dicDiv.style.display;

                        if(display==='block')
                        {
                            firmNameDiv.style.display = 'none';
                            firmName.removeAttribute("required");
                            icoDiv.style.display = 'none';
                            ico.removeAttribute("required");
                            dicDiv.style.display = 'none';
                            dic.removeAttribute("required");
                        }
                        else
                        {
                            firmNameDiv.style.display = 'block';
                            firmName.setAttribute("required", "");
                            icoDiv.style.display = 'block';
                            ico.setAttribute("required", "");
                            dicDiv.style.display = 'block';
                            dic.setAttribute("required", "");
                        }
                    }
                </script>
            </div>
        </div>

        <div class="form-group" id="firmName" style="display:block;">
            <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                <strong>Název firmy</strong>
                <br>
                <input type="text" class="form-control" name="cFirmName" placeholder="Název firmy" required>
            </div>
        </div>

        <!-- YOUR IČO -->
        <div class="form-group" id="ICO" style="display:block;">
            <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                <strong>Vaše IČO</strong>
                <br>
                <input type="text" class="form-control" name="cICO" placeholder="Vaše IČO" required>
            </div>
        </div>

        <!-- YOUR DIČ -->
        <div class="form-group" id="DIC" style="display:block;">
            <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                <strong>Vaše DIČ</strong>
                <br>
                <input type="text" class="form-control" name="cDIC" placeholder="Vaše DIČ" required>
            </div>
        </div>

        <!-- YOUR COUNTRY -->
        <div class="form-group">
            <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                <strong>VAŠE ZEMĚ</strong>
                <br>
                <select class="form-control" name="cCountry" required>
                    <option value=“AF“>Afghánistán</option>
                    <option value=“AL“>Albánie</option>
                    <option value=“DZ“>Alžírsko</option>
                    <option value=“AS“>Americká Samoa</option>
                    <option value=“VI“>Americké Panenské ostrovy</option>
                    <option value=“AD“>Andorra</option>
                    <option value=“AO“>Angola</option>
                    <option value=“AI“>Anguilla</option>
                    <option value=“AQ“>Antarktida</option>
                    <option value=“AG“>Antigua a Barbuda</option>
                    <option value=“AR“>Argentina</option>
                    <option value=“AM“>Arménie</option>
                    <option value=“AW“>Aruba</option>
                    <option value=“AU“>Austrálie</option>
                    <option value=“AZ“>Ázerbájdžán</option>
                    <option value=“BS“>Bahamy</option>
                    <option value=“BH“>Bahrajn</option>
                    <option value=“BD“>Bangladéš</option>
                    <option value=“BB“>Barbados</option>
                    <option value=“BE“>Belgie</option>
                    <option value=“BZ“>Belize</option>
                    <option value=“BJ“>Benin</option>
                    <option value=“BM“>Bermudy</option>
                    <option value=“BY“>Bělorusko</option>
                    <option value=“BT“>Bhútán</option>
                    <option value=“BO“>Bolívie</option>
                    <option value=“BA“>Bosna a Hercegovina</option>
                    <option value=“BW“>Botswana</option>
                    <option value=“BV“>Bouvetův ostrov</option>
                    <option value=“BR“>Brazílie</option>
                    <option value=“IO“>Britské Indickooceánské území</option>
                    <option value=“VG“>Britské Panenské ostrovy</option>
                    <option value=“BN“>Brunej</option>
                    <option value=“BG“>Bulharsko</option>
                    <option value=“BF“>Burkina Faso</option>
                    <option value=“BI“>Burundi</option>
                    <option value=“CK“>Cookovy ostrovy</option>
                    <option value=“TD“>Čad</option>
                    <option value=“CZ“ selected>Česká republika</option>
                    <option value=“CN“>Čína</option>
                    <option value=“DK“>Dánsko</option>
                    <option value=“CD“>Demokratická republika Kongo</option>
                    <option value=“DM“>Dominika</option>
                    <option value=“DO“>Dominikánská republika</option>
                    <option value=“DJ“>Džibutsko</option>
                    <option value=“EG“>Egypt</option>
                    <option value=“EC“>Ekvádor</option>
                    <option value=“ER“>Eritrea</option>
                    <option value=“EE“>Estonsko</option>
                    <option value=“ET“>Etiopie</option>
                    <option value=“FO“>Faerské ostrovy</option>
                    <option value=“FK“>Falklandy</option>
                    <option value=“FJ“>Fidži</option>
                    <option value=“PH“>Filipíny</option>
                    <option value=“FI“>Finsko</option>
                    <option value=“FR“>Francie</option>
                    <option value=“GF“>Francouzská Guyana</option>
                    <option value=“TF“>Francouzská jižní území</option>
                    <option value=“PF“>Francouzská Polynésie</option>
                    <option value=“GA“>Gabon</option>
                    <option value=“GM“>Gambie</option>
                    <option value=“GZ“>Gaza</option>
                    <option value=“GH“>Ghana</option>
                    <option value=“GI“>Gibraltar</option>
                    <option value=“GD“>Grenada</option>
                    <option value=“GL“>Grónsko</option>
                    <option value=“GE“>Gruzie</option>
                    <option value=“GP“>Guadeloupe</option>
                    <option value=“GU“>Guam</option>
                    <option value=“GT“>Guatemala</option>
                    <option value=“GG“>Guernsey</option>
                    <option value=“GN“>Guinea</option>
                    <option value=“GW“>Guinea-Bissau</option>
                    <option value=“GY“>Guyana</option>
                    <option value=“HT“>Haiti</option>
                    <option value=“HM“>Heardův ostrov a McDonaldovy ostrovy</option>
                    <option value=“HN“>Honduras</option>
                    <option value=“HK“>Hongkong</option>
                    <option value=“CL“>Chile</option>
                    <option value=“HR“>Chorvatsko</option>
                    <option value=“IN“>Indie</option>
                    <option value=“ID“>Indonésie</option>
                    <option value=“IQ“>Irák</option>
                    <option value=“IR“>Írán</option>
                    <option value=“IE“>Irsko</option>
                    <option value=“IS“>Island</option>
                    <option value=“IT“>Itálie</option>
                    <option value=“IL“>Izrael</option>
                    <option value=“JM“>Jamajka</option>
                    <option value=“JP“>Japonsko</option>
                    <option value=“YE“>Jemen</option>
                    <option value=“JE“>Jersey</option>
                    <option value=“ZA“>Jihoafrická republika</option>
                    <option value=“GS“>Jižní Georgie a Jižní Sandwichovy ostrovy</option>
                    <option value=“KR“>Jižní Korea</option>
                    <option value=“JO“>Jordánsko</option>
                    <option value=“KY“>Kajmanské ostrovy</option>
                    <option value=“KH“>Kambodža</option>
                    <option value=“CM“>Kamerun</option>
                    <option value=“CA“>Kanada</option>
                    <option value=“CV“>Kapverdy</option>
                    <option value=“QA“>Katar</option>
                    <option value=“KZ“>Kazachstán</option>
                    <option value=“KE“>Keňa</option>
                    <option value=“KI“>Kiribati</option>
                    <option value=“CC“>Kokosové ostrovy</option>
                    <option value=“CO“>Kolumbie</option>
                    <option value=“KM“>Komory</option>
                    <option value=“CG“>Kongo</option>
                    <option value=“CR“>Kostarika</option>
                    <option value=“CU“>Kuba</option>
                    <option value=“KW“>Kuvajt</option>
                    <option value=“CY“>Kypr</option>
                    <option value=“KG“>Kyrgyzstán</option>
                    <option value=“LA“>Laos</option>
                    <option value=“LS“>Lesotho</option>
                    <option value=“LB“>Libanon</option>
                    <option value=“LR“>Libérie</option>
                    <option value=“LY“>Libye</option>
                    <option value=“LI“>Lichtenštejnsko</option>
                    <option value=“LT“>Litva</option>
                    <option value=“LV“>Lotyšsko</option>
                    <option value=“LU“>Lucebursko</option>
                    <option value=“MO“>Macao</option>
                    <option value=“MG“>Madagaskar</option>
                    <option value=“HU“>Maďarsko</option>
                    <option value=“MK“>Makedonie</option>
                    <option value=“MY“>Malajsie</option>
                    <option value=“MW“>Malawi</option>
                    <option value=“MV“>Maledivy</option>
                    <option value=“ML“>Mali</option>
                    <option value=“MT“>Malta</option>
                    <option value=“MA“>Maroko</option>
                    <option value=“MH“>Marshallovy ostrovy</option>
                    <option value=“MQ“>Martinik</option>
                    <option value=“MU“>Mauricius</option>
                    <option value=“MR“>Mauritánie</option>
                    <option value=“YT“>Mayotte</option>
                    <option value=“MX“>Mexiko</option>
                    <option value=“FM“>Mikronésie</option>
                    <option value=“MD“>Moldavsko</option>
                    <option value=“MC“>Monako</option>
                    <option value=“MN“>Mongolsko</option>
                    <option value=“MS“>Montserrat</option>
                    <option value=“MZ“>Mosambik</option>
                    <option value=“MM“>Myanmar</option>
                    <option value=“NA“>Namibie</option>
                    <option value=“NR“>Nauru</option>
                    <option value=“NP“>Nepál</option>
                    <option value=“DE“>Německo</option>
                    <option value=“NE“>Niger</option>
                    <option value=“NG“>Nigérie</option>
                    <option value=“NI“>Nikaragua</option>
                    <option value=“NU“>Niue</option>
                    <option value=“AN“>Nizozemské Antily</option>
                    <option value=“NL“>Nizozemsko</option>
                    <option value=“NF“>Norfolk</option>
                    <option value=“NO“>Norsko</option>
                    <option value=“NC“>Nová Kaledonie</option>
                    <option value=“NZ“>Nový Zéland</option>
                    <option value=“OM“>Omán</option>
                    <option value=“IM“>Ostrov Man</option>
                    <option value=“PK“>Pákistán</option>
                    <option value=“PW“>Palau</option>
                    <option value=“PA“>Panama</option>
                    <option value=“PG“>Papua Nová Guinea</option>
                    <option value=“PY“>Paraguay</option>
                    <option value=“PE“>Peru</option>
                    <option value=“PN“>Pitcairn</option>
                    <option value=“CI“>Pobřeží slonoviny</option>
                    <option value=“PL“>Polsko</option>
                    <option value=“PR“>Portoriko</option>
                    <option value=“PT“>Portugalsko</option>
                    <option value=“AT“>Rakousko</option>
                    <option value=“RE“>Réunion</option>
                    <option value=“GQ“>Rovníková Guinea</option>
                    <option value=“RU“>Rusko</option>
                    <option value=“RO“>Rumunsko</option>
                    <option value=“RW“>Rwanda</option>
                    <option value=“GR“>Řecko</option>
                    <option value=“PM“>Saint Pierre a Miquelon</option>
                    <option value=“SV“>Salvador</option>
                    <option value=“WS“>Samoa</option>
                    <option value=“SM“>San Marino</option>
                    <option value=“SA“>Saúdská Arábie</option>
                    <option value=“SN“>Senegal</option>
                    <option value=“KP“>Severní Korea</option>
                    <option value=“MP“>Severní Mariany</option>
                    <option value=“SC“>Seychely</option>
                    <option value=“SL“>Sierra Leone</option>
                    <option value=“SG“>Singapur</option>
                    <option value=“SK“>Slovensko</option>
                    <option value=“SI“>Slovinsko</option>
                    <option value=“SO“>Somálsko</option>
                    <option value=“AE“>Spojené arabské emiráty</option>
                    <option value=“US“>Spojené státy</option>
                    <option value=“UM“>Spojené státy – menší odlehlé ostrovy</option>
                    <option value=“CS“>Srbsko</option>
                    <option value=“LK“>Srí Lanka</option>
                    <option value=“CF“>Středoafrická republika</option>
                    <option value=“SR“>Surinam</option>
                    <option value=“SD“>Súdán</option>
                    <option value=“SJ“>Svalbard</option>
                    <option value=“SH“>Svatá Helena</option>
                    <option value=“KN“>Svatý Kryštof a Nevis</option>
                    <option value=“LC“>Svatá Lucie</option>
                    <option value=“ST“>Svatý Tomáš</option>
                    <option value=“VC“>Svatý Vincenc a Grenadiny</option>
                    <option value=“SZ“>Svazijsko</option>
                    <option value=“SY“>Sýrie</option>
                    <option value=“SB“>Šalamounovy ostrovy</option>
                    <option value=“ES“>Španělsko</option>
                    <option value=“SE“>Švédsko</option>
                    <option value=“CH“>Švýcarsko</option>
                    <option value=“TJ“>Tádžikistán</option>
                    <option value=“TZ“>Tanzanie</option>
                    <option value=“TH“>Thajsko</option>
                    <option value=“TW“>Tchaj-wan</option>
                    <option value=“TG“>Togo</option>
                    <option value=“TK“>Tokelau</option>
                    <option value=“TO“>Tonga</option>
                    <option value=“TT“>Trinidad a Tobago</option>
                    <option value=“TN“>Tunisko</option>
                    <option value=“TR“>Turecko</option>
                    <option value=“TM“>Turkmenistán</option>
                    <option value=“TC“>Turks a Caicos</option>
                    <option value=“TV“>Tuvalu</option>
                    <option value=“UG“>Uganda</option>
                    <option value=“UA“>Ukrajina</option>
                    <option value=“UY“>Uruguay</option>
                    <option value=“UZ“>Uzbekistán</option>
                    <option value=“CX“>Vánoční ostrov</option>
                    <option value=“VA“>Vatikán</option>
                    <option value=“VU“>Vanuatu</option>
                    <option value=“VE“>Venezuela</option>
                    <option value=“GB“>Velká Británie</option>
                    <option value=“VN“>Vietnam</option>
                    <option value=“TP“>Východní Timor</option>
                    <option value=“WF“>Wallis a Futuna</option>
                    <option value=“ZM“>Zambie</option>
                    <option value=“EH“>Západní Sahara</option>
                    <option value=“ZW“>Zimbabwe</option>
                </select>
            </div>
        </div>

        <!-- YOUR ZIP -->
        <div class="form-group">
            <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                <strong>VAŠE PSČ</strong>
                <br>
                <input type="text" class="form-control" name="cZip" placeholder="Vaše PSČ" required>
            </div>
        </div>

        <!-- YOUR CITY -->
        <div class="form-group">
            <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                <strong>VAŠE MĚSTO</strong>
                <br>
                <input type="text" class="form-control" name="cCity" placeholder="Vaše město" required>
            </div>
        </div>

        <!-- YOUR STREET -->
        <div class="form-group">
            <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                <strong>VAŠE ULICE</strong>
                <br>
                <input type="text" class="form-control" name="cStreet" placeholder="Vaše ulice" required>
            </div>
        </div>

        <!-- YOUR STREET NUMBER -->
        <div class="form-group">
            <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                <strong>VAŠE ČÍSLO POPISNÉ</strong>
                <br>
                <input type="number" class="form-control" name="cStreetNumber" placeholder="Vaše číslo popisné" required>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                <input type="checkbox" id="marketing_checkbox" name="cMarketingAccept" required <?php echo !empty($marketingAccept)?'checked':''?>>
                <input type="text" name="cUserRole" value="<?=$userRole?>" hidden required>
                <label for="marketing_checkbox">Souhlasím s <a href="/assets/files/Souhlas_organizer.pdf" target="_blank">podmínkami</a></label>
            </div>
        </div>

        <!-- REGISTER BUTTON -->
        <div class="form-group">
            <div class="text-center">
                <button type="button" class="btn btn-primary" onclick="registerUser()">ODESLAT</button>
            </div>
        </div>

    </form>
    <!-- END REGISTER FORM -->
    <?php
}
?>
