<?php
// SEND CONTACT EMAIL -------------------------------------------------------
if( isset($_POST['cName'])
		&& isset($_POST['cEmail'])
		&& isset($_POST['cMessage'])
		) { 
	sendContactEmail();
}
	
function sendContactEmail() {
    $name = $_POST['cName'];
	$email = $_POST['cEmail'];
	$mess = $_POST['cMessage'];
	
	if (  $name != '' 
			&& $email != ''  
			&& $mess != '') {
	  

		$to = "info@baavis.com";
		$subject = "UDÁLOSTI | Zpráva z kontaktního formuláře od $name";


        $message = "Od: $name  |  Email odesílatele: $email  |  Text zprávy: $mess";



		mail($to,$subject,$message);
		
		echo '
			<div class="text-center">
		  		<div class="alert alert-info">Děkujeme za zaslání zprávy, ozveme se Vám jak nejdříve to bude možné!</div>
			</div>
		';
	

  // You must fill all the fields!	
	} else {
		  echo '
		 <div class="text-center">
		 	<div class="alert alert-danger">
            	<em class="fa fa-exclamation"></em>Musíte vyplnit všechna pole formuláře<em class="fa fa-exclamation"></em>
            </div>
		</div>
		 ';
		  
	}
}
?>

<!-- CONTACT US MODAL -->
<div id="contactModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
    	<div class="modal-dialog">
        	<div class="modal-content">

            <div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="contactModalLabel">KONTAKTUJTE NÁS</h4>
                <P>Bude nám potěšením si přečíst Vaše názory i dotazy ohledně našich služeb a těchto webových stránek. Neváhejte nás kontaktovat.</p>
            </div>

            <div class="modal-body">

              <!-- CONTACT FORM -->
              <form class="form-horizontal" method="post" action="?sendContactEmail">

                <!-- YOUR NAME -->
                <div class="form-group">
                  <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                    <strong>VAŠE JMÉNO</strong>
                    <br>
                    <input type="text" class="form-control" name="cName" placeholder="Vyplňte Vaše celé jméno">
                  </div>
                </div>             
                                
                <!-- YOUR EMAIL -->
                <div class="form-group">
                  <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                    <strong>VÁŠ EMAIL</strong>
                    <br>
                    <input type="text" class="form-control" name="cEmail" placeholder="Vyplňte Váš email">
                  </div>
                </div>   
                     
                <!-- MESSAGE -->
                <div class="form-group">
                  <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
                    <strong>ZPRÁVA</strong>
                    <br>
                    <textarea class="form-control" rows="3" name="cMessage" placeholder="Vyplňte Vaši zprávu"></textarea>
                  </div>   
                  </div>


                <!-- SEND MESSAGE BUTTON -->
                <div class="form-group">
                    <div class="text-center">
                      <button type="submit" class="btn btn-primary">ODESLAT ZPRÁVU</button>
                    </div>
                </div>

              </form><!-- END SEND MESSAGE FORM -->
				

			       </div><!-- end modal body -->


             <!-- CLOSE BUTTON -->
             <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>
            </div>

        </div></div></div><!-- END CONTACT US MODAL -->