<?php
use Parse\ParseException;
use Parse\ParseFile;
use Parse\ParseGeoPoint;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseUser;
require_once './modal-premiumspotcalendar.php';

// SAVE EVENT ------------------------------------------------
if (isset($_POST['title'])
    && isset($_POST['description'])
    && isset($_POST['location'])
    && isset($_POST['startDate'])
    && isset($_POST['endDate'])
    && isset($_POST['cost'])
    && isset($_POST['website'])
    && isset($_POST['category'])
    && isset($_POST['fileURL'])
    && isset($_POST['latitude'])
    && isset($_POST['longitude'])
    && (isset($_POST['cStreet']) || isset($_POST['cCity']))
) {
    saveEvent();
}

function saveEvent()
{
    $fileURL = $_POST['fileURL'];
    $title = $_POST['title'];
    $description = $_POST['description'];
    $location = $_POST['location'];
    $startDate = $_POST['startDate'];
    $endDate = $_POST['endDate'];
    $cost = $_POST['cost'];
    $website = $_POST['website'];
    $category = $_POST['category'];
    $latitude = $_POST['latitude'];
    $longitude = $_POST['longitude'];
    $premiumSpotLevel = intval($_POST['premiumspotlevel']);
    $premiumSpotStart = $_POST['premiumspotstart'];
    $premiumSpotLevelCategory = intval($_POST['premiumspotlevelcategory']);
    $premiumSpotStartCategory = $_POST['premiumspotstartcategory'];
    $street = $_POST['cStreet'];
    $city = $_POST['cCity'];
    $houseNumber = $_POST['cNumber'];
    $contactPhone = $_POST['contact-phone'];
    $contactWeb = $_POST['contact-web'];
    $contactEmail = $_POST['contact-email'];

    if ($title != ''
        && $description != ''
        && $location != ''
        && $startDate != ''
        && $endDate != ''
        && $cost != ''
        && $website != ''
        && $category != ''
        && $latitude != ''
        && $longitude != ''
    ) {

        $eObj = new ParseObject("Events");

        // Set data
        $eObj->set("title", $title);
        $eObj->set("description", $description);
        $categoryObject = new ParseObject('EventCategories', $category);
        $categoryRelations = $eObj->getRelation('categories');
        $categoryRelations->add($categoryObject);

        $ownerObject = ParseUser::getCurrentUser();
        $eObj->set('owner', $ownerObject);

        $eObj->set("location", $location);
        $geoPoint = new ParseGeoPoint($latitude, $longitude);
        $eObj->set("locationGps", $geoPoint);

        $eObj->set('city',$city);
        $eObj->set('street',$street);
        $eObj->set('houseNumber',$houseNumber);

        // Set startDate
        $sDate = new DateTime($startDate);
        $eObj->set('startDate', $sDate);

        // Set endDate
        $eDate = new DateTime($endDate);
        $eObj->set('endDate', $eDate);

        // Set cost
        $eObj->set('cost', $cost);

        // Set website
        $eObj->set('website', $website);
        $eObj->set('phone',$contactPhone);
        $eObj->set('contact_website',$contactWeb);
        $eObj->set('email',$contactEmail);
        // Set premium spot
        if($premiumSpotLevel && $premiumSpotStart) {
            $premiumDatesArray = explode(',',$premiumSpotStart);
            $eObj->add('unpaidPremiumCommon',$premiumDatesArray);
            $eObj->set('premiumSpot', $premiumSpotLevel);
        }

        if($premiumSpotLevelCategory && $premiumSpotStartCategory) {
            $premiumDatesCategoryArray = explode(',',$premiumSpotStartCategory);
            $eObj->add('unpaidPremiumCategory',$premiumDatesCategoryArray);
            $eObj->set('categoryPremiumSpot', $premiumSpotLevelCategory);
        }

        // Make an array of keywords
        $keywordsString = strtolower($title) . ' ' . strtolower($description) . ' ' . strtolower($location);
        $keywArray = explode(" ", $keywordsString);
        $eObj->setArray("keywords", $keywArray);

        if (EVENT_MUST_BE_ACCEPTED_BY_ADMIN) {
            $eObj->set("isPending", true);
        } else {
            $eObj->set("isPending", false);
        }

        // SAVE A FILE (an image)
        if ($fileURL != null) {
            $localFilePath = $fileURL;
        } else {
            $localFilePath = "images/logo.png";
        }
        $file = ParseFile::createFromFile($localFilePath, "image.jpg");
        $file->save();
        $eObj->set("image", $file);

        // Saving block
        try {
            $eObj->save();
            if (EVENT_MUST_BE_ACCEPTED_BY_ADMIN) {
                echo '
			<div class="text-center">
                  <div class="alert alert-success">Vaše událost byla zapsána a schvalovací proces proběhne do následujích 48 hodin!';
                  if($premiumSpotLevel)
                  echo 'Na prémiovém postu se zobrazí po zaplacení faktury, kterou dostanete emailem.';
                echo '</div>
			</div>
            ';
            } else {
                echo '
                <div class="text-center">
                      <div class="alert alert-success">Vaše událost byla zapsána.';
                if($premiumSpotLevel)
                    echo 'Na prémiovém postu se zobrazí po zaplacení faktury, kterou dostanete emailem.';
                echo'</div>
                </div>
                ';
            }
            $currentUser = ParseUser::getCurrentUser();
            if ($currentUser) {
                // SEND EMAIL NOTIFICATION TO THE ADMIN (YOU)
                $to = ADMIN_EMAIL;
                $subject = "UDÁLOSTI | Přidání nové události!";
                $message = $currentUser->get('firstname') . ' ' . $currentUser->get('lastname') . ' přidal novou událost: \'' . $title . '\'' . "\n\n.";
                $headers = "From: " . $currentUser->get('email');
                mail($to, $subject, $message, $headers);
            }

            // error  on saving
        } catch (ParseException $ex) {
            echo '
      <div class="text-center">
        <div class="alert alert-danger">
          <em class="fa fa-exclamation"></em> ' . $ex->getMessage() . '
        </div>
      </div>
		';
        }

// You must fill all the fields to sign up!
    } else {
        echo '
		 <div class="text-center">
		 	<div class="alert alert-danger">
            	<em class="fa fa-exclamation"></em>Musíte vyplnit všechna pole formuláře<em class="fa fa-exclamation"></em>
            </div>
		</div>
		 ';
    }

}
?>
<div class="section-content">
    <!-- SELECT IMAGE SECTION-->
    <div class="col-lg-4 col-sm-4 col-md-4">

        <div class="panel panel-primary">
            <div class="panel-body">
                <div class="fileupload fileupload-new pull-left" data-provides="fileupload">
                    <div class="fileupload-new thumbnail">

                        <!-- image -->
                        <img id="image-img" class="img-responsive center-cropped-avatar" src="images/add_image.jpg" height="64" width="64" />

                    </div>
                    <span class="btn btn-file btn-info">
                        <span class="fileupload-new">Vybrat obrázek</span>
                        <span class="fileupload-exists">Změnit</span>

                        <!-- ImageData input -->
                        <input id="imageData" name="file" type="file" accept="image/*" />


                    </span>
                    <!-- <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a> -->
                </div>
            </div>

            <div class="panel-footer">
                <h5>Nahrát obázek</h5>
            </div>

        </div>

    </div>
    <!-- END SELECT IMAGE SECTION -->
</div>
    <!-- AUTOMATICALLY UPLOAD SELECTED IMAGE -->
    <script>
        document.getElementById("imageData").onchange = function () {
            var reader = new FileReader();
            reader.onload = function (data) {

                document.getElementById("image-img").src = data.target.result;
                console.log(data.target.result);
                document.getElementById("image-img").onload = function () {

                    // Upload the selected image automatically into the 'uploads' folder
                    var filename = "avatar.jpg";
                    var data = new FormData();
                    data.append('file', document.getElementById('imageData').files[0]);

                    var websitePath = "<?php echo $_GLOBALS['WEBSITE_PATH'] ?>";

                    $.ajax({
                        url: "/uploadeventimage",
                        type: 'POST',
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            data = data.trim();
                            document.getElementById("fileURLTxt").value = websitePath + data;
                        }, error: function (e) {
                            console.log(e);
                            alert("Něco se pokazilo, prosíme, zkuste akci opakovat! " + e);
                        }
                    });
                };
            };
            if (document.getElementById('imageData').files[0]) {
                reader.readAsDataURL(document.getElementById('imageData').files[0]);
            }
        };

    </script>

        <!-- SUBMIT FORM -->
        <form class="form-horizontal" action="/eventnew" method="post" id="eventNewForm">

            <!-- Hidden fileURL input -->
            <input id="fileURLTxt" style="opacity:0;" size="50" type="text" name="fileURL" value="">



            <!-- TITLE -->
            <div class="form-group">
                <div class="col-sm-12 col-lg-12 col-md-12">
                    <strong>NÁZEV UDÁLOSTI</strong>
                    <br>
                    <input type="text" class="form-control" name="title" placeholder="např.: Pink Floyd koncert v Praze" required>
                </div>
            </div>


            <!-- DESCRIPTION -->
            <div class="form-group">
                <div class="col-sm-12 col-lg-12 col-md-12">
                    <strong>POPIS UDÁLOSTI</strong>
                    <br>
                    <textarea class="form-control" rows="3" name="description" placeholder="Vložte popis události" required></textarea>
                </div>
            </div>

            <!-- CATEGORY -->
            <div class="form-group">
                <div class="col-sm-6 col-lg-6 col-md-6">
                <strong>KATEGORIE</strong>
                <br>
                <select class="form-control" name="category" id="categorySelect">
<?php
$query = new ParseQuery('EventCategories');
$query->ascending('name');
$result = $query->find();
for ($i = 0; $i < count($result); $i++) {
    echo '<option value="' . $result[$i]->getObjectId() . '">' . $result[$i]->get('name') . '</option>';
}
?>
                </select>
              </div>

            <!-- LOCATION -->
                <div class="col-sm-6 col-lg-6 col-md-6">
                    <strong>MÍSTO UDÁLOSTI</strong>
                    <br>
                    <input type="text" id="google_autocomplete" class="form-control" name="location" placeholder="Zadejte adresu..." required>
                    <input type="hidden" name="latitude" value=""/>
                    <input type="hidden" name="longitude" value=""/>
                    <input type="hidden" name="cCity" id="locality_new">
                    <input type="hidden" name="cStreet" id="route_new">
                    <input type="hidden" name="cNumber" id="street_number_new">
                    <script>
                        document.getElementById('eventNewForm').addEventListener('keydown', function (e) {
                            if (e.keyCode === 13) {
                                e.preventDefault();
                            }
                        });
                    var autocomplete2;
                    var componentForm2 = {
                        street_number: 'short_name',
                        route: 'long_name',
                        locality: 'long_name',
                    };

                        function initEventAutocomplete() {
                            // Create the autocomplete object, restricting the search to geographical
                            // location types.
                            autocomplete2 = new google.maps.places.Autocomplete(
                                /** @type {!HTMLInputElement} */(document.getElementById('google_autocomplete')),
                                {
                                    types: ['geocode'],
                                    componentRestrictions: {country: "cz"}
                                });

                            // When the user selects an address from the dropdown, populate the address
                            // fields in the form.
                            autocomplete2.addListener('place_changed', fillInAddressEvent);
                        }

                        function fillInAddressEvent() {
                            // Get the place details from the autocomplete object.
                            var place = autocomplete2.getPlace();

                            for (var component in componentForm2) {
                                document.getElementById(component).value = '';
                            }
                            // Get each component of the address from the place details
                            // and fill the corresponding field on the form.
                            for (var i = 0; i < place.address_components.length; i++) {
                                var addressType = place.address_components[i].types[0];
                                if (componentForm2[addressType]) {
                                    var val = place.address_components[i][componentForm2[addressType]];
                                    document.getElementById(addressType+'_new').value = val;
                                }
                            }

                            $('input[name=latitude]').val(place.geometry.location.lat());
                            $('input[name=longitude]').val(place.geometry.location.lng());
                        }
                    </script>

                </div>
            </div>


            <!-- START DATE -->
            <div class="form-group">
                <div class="col-sm-6 col-lg-6 col-md-6">
                    <strong>ZAČÁTEK</strong>
                    <br>
                    <input type="datetime-local" class="form-control" name="startDate" placeholder="např.: 2018-05-16T12:00" required>
                </div>

            <!-- END DATE -->
            <div class="col-sm-6 col-lg-6 col-md-6">
                    <strong>KONEC</strong>
                    <br>
                    <input type="datetime-local" class="form-control" name="endDate" placeholder="např.: 2018-05-16T17:00" required>
                </div>
            </div>

            <!-- COST -->
            <div class="form-group">
                <div class="col-sm-6 col-lg-6 col-md-6">
                    <strong>CENA</strong>
                    <br>
                    <input type="text" class="form-control" name="cost" placeholder="např.: 150" required>
                </div>

            <!-- WEBSITE -->
                <div class="col-sm-6 col-lg-6 col-md-6">
                    <strong>WEBOVÁ STRÁNKA PRO ZAKOUPENÍ VSTUPENKY</strong>
                    <br>
                    <input type="text" class="form-control" name="website" placeholder="http://" required>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-4 col-lg-4 col-md-4">
                    <strong>WEB</strong>
                    <br>
                    <input type="text" class="form-control" name="contact-web" value="" placeholder="Web">
                </div>
                <div class="col-sm-4 col-lg-4 col-md-4">
                    <strong>TELEFON</strong>
                    <br>
                    <input type="text" class="form-control" name="contact-phone" value="" placeholder="Telefon">
                </div>
                <div class="col-sm-4 col-lg-4 col-md-4">
                    <strong>E-MAIL</strong>
                    <br>
                    <input type="text" class="form-control" name="contact-email" value="" placeholder="Email">
                </div>
            </div>

                <!-- PREMIUM SPOT -->


            <div class="form-group premiumspotcalendar-button">
                <!-- <div > -->
                <div class="col-sm-12 col-lg-12 col-md-12">
                    <span id="eventnew-premiumspotposition-text"></span>
                    <span id="eventnew-premiumspotstartdate-text"></span>
                    <span id="eventnew-premiumspotenddate-text"></span>
                    <input type="hidden" id="eventnew-premiumspotposition" name="premiumspotlevel" value=""/>
                    <input type="hidden" id="eventnew-premiumspotstartdate" name="premiumspotstart" value=""/>
                    <input type="hidden" id="eventnew-premiumspotenddate" name="premiumspotend" value=""/>
                    <br>

                    <span id="eventnew-premiumspotposition-text_category"></span>
                    <span id="eventnew-premiumspotstartdate-text_category"></span>
                    <span id="eventnew-premiumspotenddate-text_category"></span>
                    <input type="hidden" id="eventnew-premiumspotposition_category" name="premiumspotlevelcategory" value=""/>
                    <input type="hidden" id="eventnew-premiumspotstartdate_category" name="premiumspotstartcategory" value=""/>
                    <input type="hidden" id="eventnew-premiumspotenddate_category" name="premiumspotendcategory" value=""/>

                    <a data-toggle="modal" href="#premiumSpotCalendarModal" class="btn btn-info btn-block">
                        <strong>PRÉMIOVÉ UMÍSTĚNÍ UDÁLOSTI</strong>
                    </a>
                </div>
                <!-- </div> -->
            </div>

            <!-- SUBMIT BUTTON -->
            <div class="form-group">
                <div class="text-center">
                    <p>*Nezapomeňte nahrát obrázek před odesláním události!</p>
                    <!-- <button type="submit" class="btn btn-primary" onclick="showLoadingModal()">ODESLAT UDÁLOST KE SCHVÁLENÍ</button> -->
                    <button type="submit" class="btn btn-primary">Uložit</button>
                </div>
            </div>

        </form>
        <!-- END FORM -->

<script>

    $("#categorySelect").on("change",function(){
        var select = $('#categorySelect');
        $('#catPremium2').attr('data-label',select[0].options[select[0].selectedIndex].text);
        $('#catPremium2').value = select[0].value;
        $('#catPremium2Label').text(select[0].options[select[0].selectedIndex].text);

        $('#catPremium3').attr('data-label',"Všechny+" + select[0].options[select[0].selectedIndex].text);
        $('#catPremium3').value = "Všechny+" + select[0].value;
        $('#catPremium3Label').text("Všechny+" + select[0].options[select[0].selectedIndex].text);
        setPremiumSpot($("#premiumSpot")[0].value, ($("#premiumSpot_category")[0].value));
    });

    $( document ).ready(function() {
        var select = $('#categorySelect');
        $('#catPremium2').attr('data-label',select[0].options[select[0].selectedIndex].text);
        $('#catPremium2').value = select[0].value;
        $('#catPremium2Label').text(select[0].options[select[0].selectedIndex].text);

        $('#catPremium3').attr('data-label',"Všechny+" + select[0].options[select[0].selectedIndex].text);
        $('#catPremium3').value = "Všechny+" + select[0].value;
        $('#catPremium3Label').text("Všechny+" + select[0].options[select[0].selectedIndex].text);

        initEventAutocomplete();
    });
</script>