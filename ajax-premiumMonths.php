<?php
use Parse\ParseQuery;

$premiumSpot = $_GET['spot'];
$category = $_GET['category'];
$categorySpot = $_GET['categorySpot'];
$result = array();
$result["common"] = array();
$result["category"] = array();
$queryPremiumSpots = new ParseQuery("Events");
$currDate = new DateTime();
try {
        if($category[0]==="Všechny")
            $category[0]="";
        $queryPremiumSpots->equalTo("premiumSpotPayed", true);
        $queryPremiumSpots->equalTo('categoryPremiumSpot',intval($categorySpot));
        if($category[0] !== "" || count($category)>1)
        {
            $catFilter = new ParseQuery("EventCategories");
            $catFilter->equalTo("name",count($category)>1?$category[1]:$category[0]);
            $categoryArr = $catFilter->find();
            $queryPremiumSpots->equalTo("categories",$categoryArr[0]);
        }
        $commonQuery = new ParseQuery("Events");
        $commonQuery->equalTo("premiumSpotPayed", true);
        $commonQuery->equalTo('premiumSpot',intval($premiumSpot));


    $commonEventsAr = $commonQuery->find();

    if($category[0]!=="" || count($category)>1)
    {
        $premiumEventsAr = $queryPremiumSpots->find();
        for($i=0;$i<count($premiumEventsAr);++$i) {
            $premiumDatesArray = $premiumEventsAr[$i]->get('paidPremiumCategory');
            if(isset($premiumDatesArray)) {
                $result["category"] = array_merge($result["category"], $premiumDatesArray);
            }
    }
    }

    for($i=0;$i<count($commonEventsAr);++$i) {
            $premiumDatesArray = $commonEventsAr[$i]->get('paidPremiumCommon');
            if(isset($premiumDatesArray)) {
                $result["common"] = array_merge($result["common"], $premiumDatesArray);
            }
    }

    echo json_encode($result);
}
catch (Exception $e) {
    echo 'ERROR:SERVERFAILURE'.$e->getMessage();
}
?>