<?php
/* Variables */
$searchLocation = $_POST['searchLocation'];
if(empty($searchLocation) || is_null($searchLocation))
    $searchLocation = $_GET['searchLocation'];
$category = "";
$currDate = new DateTime();
$eventsCount= 0;
$categoryArr = null;
// QUERY EVENTS -----------------------------------
try {
    if (isset($_GET["category"]))
        $category = $_GET["category"];

    if (isset($_GET["eventsCount"]))
        $eventsCount = $_GET["eventsCount"];
    $lng = null;
    $lat = null;
    // Find premium
    $queryGetEvents = new ParseQuery("Events");
    $queryGetEvents->greaterThanOrEqualTo("endDate", $currDate);

    $queryGetEvents->ascending("premiumSpot");
    $queryGetEvents->ascending("startDate");

    //Set search by location
    if (!is_null($searchLocation) && !empty($searchLocation)) {
        $url = str_replace('<ADDRESS>', urlencode($searchLocation), GEOCODING_API_URL);
        $response = file_get_contents($url);
        $json = json_decode($response, true);

        $lat = $json['results'][0]['geometry']['location']['lat'];
        $lng = $json['results'][0]['geometry']['location']['lng'];

        if ($lat && $lng) {
            try {
                $searchGeoPoint = new ParseGeoPoint($lat, $lng);
                $queryGetEvents->withinKilometers('locationGps', $searchGeoPoint, 0);
            } catch (Exception $e) {
            }
        }
    }
    //Set category filter
    if (!empty($category)) {
        $catFilter = new ParseQuery("EventCategories");
        $catFilter->equalTo("name", $category);
        $categoryArr = $catFilter->find();
    }


    if ($categoryArr) {
        $queryGetEvents->equalTo("categories", $categoryArr[0]);
    }
    $evEventsArray = $queryGetEvents->find();
    $evPremiumArray = [];
    $evNonPremiumArray = [];

    //Group premium and not premium events
    for($i = 0; $i<count($evEventsArray);$i++) {
        $eObj = $evEventsArray[$i];
        $isPremiumPaid = $eObj->get('premiumSpotPayed');
        $isPremium = false;
        if (!$isPremiumPaid) {
            $evNonPremiumArray[] = $eObj;
            continue;
        }
        $premiumMonthsArr = $eObj->get('paidPremium');
        for ($j = 0; $j < count($premiumMonthsArr); $j++) {
            $premiumMonth = date("M-yyyy", strtotime($premiumMonthsArr[$j]));
            $currentMonth = date("M-yyyy", strtotime($currDate));

            if ($premiumMonth == $currentMonth) {
                $eObj->set('showAsPremium', true);
                $evPremiumArray[] = $eObj;
                $isPremium = true;
                break;
            }
        }
        if(!$isPremium)
            $evNonPremiumArray[] = $eObj;
    }

    $evArray = array_merge($evPremiumArray, $evNonPremiumArray);
}
catch(Exception $e)
{}
