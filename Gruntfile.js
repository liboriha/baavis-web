module.exports = function (grunt) {

	require('jit-grunt')(grunt);

	grunt.initConfig({

		pkg: grunt.file.readJSON('package-lock.json'),

        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'assets/css/main.css': 'assets/scss/main.scss'
                }
            }
        },


		autoprefixer: {
            options: {
                    map: {
                        prev: 'assets/css/',
                        inline: false
                	},
            	browsers: ['last 3 version', 'ie 9']
            },
                style: {
                src: 'assets/css/main.css',
            	dest: 'assets/css/main.css'
            }
        },
		
		pixrem: {
			options: {
				rootvalue: '10px',
			},
			style: {
				src: 'assets/css/main.css',
				dest: 'assets/css/main.css'
			}
		},



		cssmin: {
  			target: {
    			files: [{
      				expand: true,
      				cwd: 'assets/css/',
      				src: ['*.assets/css', '!*.min.css'],
      				dest: 'assets/css/',
      				ext: '.min.css'
    			}]
  			}
		},


        cssnano: {
            options: {
                sourcemap: false
            },
            dist: {
                files: {
                    'assets/css/main.css': 'assets/css/main.css'
                }
            }
        },

		watch: {
			sass: {
				files: ['assets/scss/**/*.*'],
				tasks: ['doCSS'],
				options: {
				    livereload: {
				    	host: 'localhost',
        				port: 9001,
				    },
				},
			},

			html: {
				files: ['../*.html'],
				options: {
					livereload: {
				    	host: 'localhost',
        				port: 9001,
				    },
				},
			},
			images: {
				files: ['../img/sprites/*.*'],
				tasks: ['sprite', 'buildcss:style']
			},
		},

		hashres: {
			options: {
				encoding: 'utf8',
				fileNameFormat: '${name}.${ext}?${hash}',
				renameFiles: false
			},
			dist: {
				src: [
					'../images/*.{png,jpg,gif}',
					'../images/*/*.{png,jpg,gif}',
				],
				dest: [
					'../css/main.css',
					'*.php',
				],
			}
		}
	});

    grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');


	grunt.registerTask('doCSS',  ['sass', 'pixrem']);
	grunt.registerTask('doJS',  ['concat']);
	grunt.registerTask('doBASIC', ['doCSS', 'doJS', 'sprite', 'cssnano']);
	

	grunt.registerTask('default', ['concat', 'sass', 'pixrem']);
	grunt.registerTask('production', ['concat', 'sass', 'hashres']);

}