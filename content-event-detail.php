
<?php
use Parse\ParseObject;
use Parse\ParseUser;
use Parse\ParseQuery;

/* Variables */
$eObjID = $params[0];

$eObj = new ParseObject("Events", $eObjID);
$eObj->fetch();

$currentUser = ParseUser::getCurrentUser();



function TranslateMonth($monthNumber)
{
    $months = ['Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosinec'];
    return $months[$monthNumber];
}

// Get parse Object by its objectID
$eObj = new ParseObject("Events", $eObjID);
$eObj->fetch();

// Get title
$title = $eObj->get('title');

// Get image
$file = $eObj->get('image');
$imageURL = $file->getURL();

// Get cost
$cost = $eObj->get('cost');

// Get start date
$sDate = $eObj->get('startDate');
$startDate = date_format($sDate, "d ") . TranslateMonth(intval(date_format($sDate, "m"))) . date_format($sDate, " Y | H:i A");
$startDateString = date_format($sDate, "dmYHi");
$startDateForCalendar = date_format($sDate, "Ymd\THis\Z");

// Get end date
$eDate = $eObj->get('endDate');
$endDate = date_format($eDate, "d ") . TranslateMonth(intval(date_format($eDate, "m"))) . date_format($eDate, " Y | H:i A");
$endDateString = date_format($eDate, "dmYHi");
$endDateForCalendar = date_format($eDate, "Ymd\THis\Z");

// Get month, day and year of the Start Date
$month = date_format($sDate, "m");
$day = date_format($sDate, "d");
$year = date_format($sDate, "Y");

// Get website
$website = $eObj->get('website');

// Get description
$description = $eObj->get('description');

//get likes
$likesCount = $eObj->get("likes");
$contactPhone = $eObj->get("phone");
$contactEmail = $eObj->get("email");
$contactWeb = $eObj->get("contact_website");
// Get location
$location = $eObj->get('location');
$locationNoSpaces = preg_replace('/\s+/', '+', $location);

$liked = false;

if(isset($currentUser)) {
    $query = new ParseQuery("UserEventLikes");
    $query->equalTo("user_id", $currentUser->getObjectId());
    $query->equalTo("event_id", $eObj->getObjectId());

    $result = $query->find();
    if (count($result) > 0) {
        $liked = true;
    }
}
?>
<div class="section-content">
    <!-- EVENT IMAGE -->
    <img class="content-event-img" src="<?php print $imageURL;?>" alt="<?php print $title;?>">
    <!-- DESCRIPTION -->
    <article class="subhead-description">
        <button class="btn btn-primary" style="margin: 3px" data-eventId="<?=$eObjID?>" data-likes="<?=$likesCount?>" data-liked="<?=$liked?>" onclick="likeBtnClick(this)"><i class="fa <?= $liked?'fa-check':'fa-heart-o'?>"></i> <?=$likesCount?></button>
        <h3 class="subhead-header">POPIS:</h3>
        <p class="subhead-p">
            <?php
            $descArray = preg_split('/\n+/', $description);
            for ($i = 0; $i < count($descArray); $i++) {
                echo $descArray[$i] . '<br>';
            }
            ?>
        </p>
    </article>
    <article class="subhead-details">
        <h3 class="subhead-header">DETAILY:</h3>
        <p class="subhead-p">Začátek: <span class="subhead-p__date"><?= $startDate;?></span></p>
        <p class="subhead-p">Konec: <span class="subhead-p__date"><?= $endDate;?></span></p>
        <?php
        if(!empty($website)) {
            ?>
            <p class="subhead-p">Cena: <span class="subhead-p__price"> <?= $cost?>,- Kč</span></p>
            <?php
        }
        else {
            ?>
            <p class="subhead-p">Cena: <span class="subhead-p__price"> <?= $cost?>,- Kč</span></p>
            <?php
        }

        ?>

    </article>
    <?php
    if (isset($contactPhone) || isset($contactWeb) || isset($contactEmail))
    {
    ?>
    <article class="subhead-details">
        <h3 class="subhead-header">KONTAKTNÍ ÚDAJE:</h3>
        <?php if (isset($contactPhone)) echo '<p class="subhead-p">Telefon: <span class="subhead-p__price">'.$contactPhone.'</span></p>'?>
        <?php if (isset($contactWeb)) echo '<p class="subhead-p">Web: <span class="subhead-p__price">'.$contactWeb.'</span></p>'?>
        <?php if (isset($contactEmail)) echo '<p class="subhead-p">Email: <span class="subhead-p__price">'.$contactEmail.'</span></p>'?>
    </article>
    <?php }?>
    <article class="subhead-place">
        <h3 class="subhead-header">MÍSTO:</h3>
        <p class="subhead-p">Adresa: <span class="subhead-p__address"> <?= $location;?></span></p>
        <div class="thumbnail">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3157.3500761935384!2d144.9398501505624!3d-37.68797673452914!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65abc3263bf4b%3A0x9431840304afb2ae!2s<?php print $locationNoSpaces;?>!5e0!3m2!1sen!2sit!4v1490197029927" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </article>

    <div class="fb-comments" data-href="https://events.viptrust.com/eventdetail/<?php print $eObjID?>" data-numposts="5"></div>
</div>
<!-- DATE AND BUTTONS COLUMN -->
<div class="section-sidebar">

    <h2 class="event-month event-header"><?= TranslateMonth(intval($month));?></h2>
    <h2 class="event-day event-header"><?= $day;?></h2>
    <h2 class="event-year event-header"><?= $year;?></h2>
    <p class="event-location">
        <i class="fa fa-map-marker" aria-hidden="true"></i>
        <?= $location?>
    </p>
    <p class="event-time">
        <?= $startDate ?>
    </p>
    <p class="event-price">
        <i class="fa fa-ticket" aria-hidden="true"></i>
        <?= $cost?>, -Kč
    </p>
    <!-- I WILL ATTEND -->
    <script>
        function likeBtnClick(e){
            const event_id = e.dataset.eventid;
            const like_count = parseInt(e.dataset.likes);
            const liked = e.dataset.liked;

            $.ajax({
                url: "likeEvent",
                type: "get",
                data: {
                    event:event_id,
                    liked:liked
                },
                contentType:false,
                success: function (data) {
                    if(data === "OK") {
                        if(liked === "true")
                        {
                            e.dataset.likes = like_count - 1;
                            e.dataset.liked = "false";
                            e.innerHTML = "<i class=\"fa fa-heart-o\"></i> " + e.dataset.likes;
                        }
                        else
                        {
                            e.dataset.likes = like_count + 1;
                            e.dataset.liked = "true";
                            e.innerHTML = "<i class=\"fa fa-check\"></i> " + e.dataset.likes;
                        }
                    }
                    else if (data==="NOTLOGGED")
                    {
                        $('#loginModal').modal('show');
                    }
                }, error: function (e) {
                    alert("Něco se pokazilo, prosíme, zkuste akci opakovat! ");
                    console.log(e);
                }
            });
        }

        function IWillAttend(eventId) {
            // Attend
            $.ajax({
                url: "/attendevent/" + eventId,
                type: 'GET',
                data: {},
                success: function (data) {
                    data = data.trim();
                    if(data.substring(0,6) == 'ERROR:') {
                        switch(data.substring(6)) {
                            case 'NOTEVENTID':
                            case 'EVENTNOTEXISTS':
                            case 'EVENTEXPIRED':{
                                alert('Bohužel událost s tímto ID neexistuje nebo již proběhla.');
                                break;
                            }
                            case 'USERISNOTLOGGED':{
                                alert('Pro potvrzení účasti je třeba se přihlásit. Pokud ještě nemáte účet, je třeba se nejprve zaregistrovat.');
                                break;
                            }
                            default: {
                                alert('Došlo k chybě na serveru - zkuste akci opakovat později.');
                                break;
                            }
                        }
                    }
                    else {
                        alert('Vaše účast na akci byla zaznamenána.');
                    }
                },
                error: function (e) {
                    alert("Něco se pokazilo, prosíme, zkuste akci opakovat později.");
                }
            });
        }
    </script>
<!--    <button class="universal-button universal-button--red" onclick="IWillAttend('<?php /*print $eObjID;*/?>')">
        ZÚČASTNÍM SE
    </button>
  ADD THIS EVENT TO GOOGLE CALENDAR
    <button class="universal-button universal-button--blue" href="https://calendar.google.com/calendar/render?action=TEMPLATE&text=<?php /*print $title;*/?>&dates=<?php /*print $startDateForCalendar;*/?>/<?php /*print $endDateForCalendar;*/?>&location=<?php /*print $location;*/?>&sprop=name:Name&sprop=website:<?php /*print $website;*/?>&details=<?php /*print $description;*/?>&sf=true&output=xml#eventpage_6">
            Přidat do<i class="fa fa-calendar"></i>
    </button>-->

    <!-- SHARE BUTTONS -->
    <ul class="side-bar-social-list clearfix">
        <li class="side-bar-social-list__item"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php print URL_BASE;?>/eventdetail?eObjID=<?php print $eObjID;?>" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
        <li class="side-bar-social-list__item"><a href="https://twitter.com/share?url=<?php print URL_BASE;?>/eventdetail?eObjID=<?php print $eObjID;?>" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
        <li class="side-bar-social-list__item"><a href="#" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        <li class="side-bar-social-list__item"><a href="mailto:?Subject=<?php print $title;?>&amp;Body=Tahle%20událost%20by%20se%20ti%20mohla%20líbit!: <?php print URL_BASE . 'eventdetail?eObjID=' . $eObjID;?>" target="_blank"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></li>
    </ul>
</div><!-- END DATE & BUTTONS COLUMN -->
