<?php
require 'autoload.php';

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
use Parse\ParseSessionStorage;
session_start();

// System constants
// IMPORTANT: REPLACE THE EMAIL ADDRESS BELOW WITH YOUR OWN ONE, SO YOU'LL RECEIVE EMAILS WHEN A NEW EVENT WILL BE SUBMITTED 
define('ADMIN_EMAIL', 'arseniy.o94@gmail.com');

$urlBase = null;
$urlPath = null;

if(isset($_SERVER['REQUEST_SCHEME']))
	$urlBase = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
else 
	$urlBase = 'http://localhost:8080';

if(isset($_GET['path']))
	$urlPath = $_GET['path'];

define('URL_BASE', $urlBase);
define('URL_PATH', $urlPath);

define('PATH_WEB_ROOT', $_SERVER['DOCUMENT_ROOT']);
define('PATH_IMAGE_EVENTS', 'uploads/');
define('PATH_IMAGE_BANNERS', 'uploads/banners/');
define('PATH_IMAGE_USERS', 'uploads/users/');

define('DEFAULT_USER_IMAGE', 'images/user.png');

// REPLACE THIS STRING WITH YOUR OWN App Id FROM back4app
$back4app_apiID = null;
// REPLACE THIS STRING WITH YOUR OWN REST API Key FROM back4app
$back4app_apiKey = null;
// REPLACE THIS STRING WITH YOUR OWN Master Key FROM back4app
$back4app_apiMater = null;
// REPLACE THIS STRING WITH YOUR OWN API Key FROM Google
$geocoding_apiKey = null;

$httpHost = null;
if(isset($_SERVER['HTTP_HOST']))
	$httpHost = $_SERVER['HTTP_HOST'];

switch($httpHost) {
	case 'localhost:8080': {
		$back4app_apiID = 'gGW4mHIZCjy595itVUbgHVO8r0TaaKighUlfG2as';
		$back4app_apiKey = 'wfDjpALjYPk9IJbF8GnyPfmJYrZgyF0K7Mbrqw9s';
		$back4app_apiMater = 'YdOtAgpGEvSWrUa3iQzCKUo2E9dRzkgx6ttuT0YB';
		$geocoding_apiKey = 'AIzaSyCq9aD62MPc1DU3s-2H9zOa2_ek3LoYb-U';
		break;
	}

	case 'event.viptrust.com':
	default:{
		$back4app_apiID = 'LkvzhqbmQZFPbymDmNEA2h0N4yNuaOPUVLmRNSdL';
		$back4app_apiKey = '9IApcdAKY1f8kTVxzgYzFoesSXTB8g7RQqKbKlgP';
		$back4app_apiMater = 'rS08EkHZb2ZdVjtwG72RQhGV48AQ3AVyVWccINpt';
		$geocoding_apiKey = 'AIzaSyCq9aD62MPc1DU3s-2H9zOa2_ek3LoYb-U';
		break;
	}
}

define('BACK4APP_API_URL', 'https://parseapi.back4app.com');
define('BACK4APP_API_ID', $back4app_apiID);
define('BACK4APP_API_KEY', $back4app_apiKey);
define('BACK4APP_API_MASTER', $back4app_apiMater);
define('GEOCODING_API_KEY', $geocoding_apiKey);
define('GEOCODING_API_URL', 'https://maps.googleapis.com/maps/api/geocode/json?key='.GEOCODING_API_KEY.'&address=<ADDRESS>');

define('EVENT_EMAIL_REMINDER_DAYS_BEFORE', 1); // How many days before event should attenders receive an email with reminder
define('EVENT_EMAIL_REVIEW_DAYS_AFTER', 1); // How many days after event should attenders receive an email with asking for review
define('EVENT_EMAIL_INFORM_OWNER_ABOUT_REVIEWS_DAYS_AFTER', 2); // How many days after event should owner receive an email with link to reviews

define('EVENT_MUST_BE_ACCEPTED_BY_ADMIN', false); // Every event must be accepted by Admin - includes sending mails


// Set API
ParseClient::initialize(BACK4APP_API_ID,BACK4APP_API_KEY, BACK4APP_API_MASTER);
ParseClient::setServerURL(BACK4APP_API_URL,'/');
// Set session storage
ParseClient::setStorage( new ParseSessionStorage() );
?>