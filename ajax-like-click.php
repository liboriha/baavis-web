<?php
$objId = $_GET['event'];
$is_liked = $_GET['liked']==='true'?true:false;
if(isset($objId)) {
    try {
        $eventFilter = new \Parse\ParseQuery("Events");

        $eventFilter->equalTo('objectId', $objId);

        $eventAr = $eventFilter->find();
        $event = $eventAr[0];

        $currentUser = \Parse\ParseUser::getCurrentUser();
        if(isset($currentUser) && isset($event)) {
                $likes = $event->get('likes');

                if ($is_liked) {
                    $likes--;
                    //remove user like
                    $query = new \Parse\ParseQuery("UserEventLikes");
                    $query->equalTo("user_id", $currentUser->getObjectId());
                    $query->equalTo("event_id", $event->getObjectId());

                    $result = $query->find();

                    if (count($result) > 0) {
                        $result[0]->destroy();
                    }
                } else {
                    //save user like
                    $eObj = new \Parse\ParseObject("UserEventLikes");
                    $eObj->set("user_id", $currentUser->getObjectId());
                    $eObj->set("event_id", $event->getObjectId());
                    $eObj->save();
                    $likes++;
                }

                $event->set("likes", $likes);
                $event->save();
                echo "OK";
        }
        else
        {
            echo "NOTLOGGED";
        }
    } catch (Exception $ex) {
        echo $ex;
    }
}
