<?php
require_once './configs.php';

use Parse\ParseException;
use Parse\ParseUser;
use Parse\ParseQuery;

// Router
$action = null;
$params = array();
if (URL_PATH) {
    $tmp = explode('/', URL_PATH);
    $action = array_shift($tmp);
    $params = $tmp;
}

$additional_header = false;
$showCategory = false;
$mainPage = false;
$content_section_class = 'section-event';
$htmlHeadBase = URL_BASE . '/'; //HTML head - base tag content
$htmlHeadTitle = null; // HTML head - title tag content
$htmlHeadMetaDescription = null; // HTML head - meta_name_description tag content

//Get events categories
$categories = new ParseQuery("EventCategories");
$categories_array = $categories->find();
$cat_name_array = [];
for($ii = 0;$ii<count($categories_array);++$ii)
{
    $cat_name_array[$ii] = $categories_array[$ii]->get("name");
}

// Get logged user
$loggedUser = null;
try {
    $currentUser = ParseUser::getCurrentUser();
    if ($currentUser) {
        $loggedUser = $currentUser;
    }

} catch (ParseException $e) {
}

ob_start();
switch ($action) {
    case 'ajaxspotsave':{
        define('AJAXREQUEST', true);
        if ($loggedUser && $loggedUser->get('userrole') == 'admin') {
            require_once './ajax-save-spot-price.php';
        }
        else {
            require_once './content-error-usernotlogged.php';
        }
        break; 
    }
    case 'ajaxeventpremiumaccept':{
        define('AJAXREQUEST', true);
        if ($loggedUser && $loggedUser->get('userrole') == 'admin') {
            require_once './ajax-accept-premium.php';
        }
        else {
            require_once './content-error-usernotlogged.php';
        }
        break; 
    }
    case 'attendevent':{
            define('AJAXREQUEST', true);
            if ($loggedUser) {
                require_once './content-attend.php';
            } else {
                die('ERROR:USERISNOTLOGGED');
            }
            break;
        }
    case 'attendantreview':{
            define('AJAXREQUEST', false);
            if ($loggedUser) {
                require_once './content-event-review.php';
            } else {
                require_once './content-error-usernotlogged.php';
            }
            $htmlHeadTitle = 'Události - Hodnocení události';
            $htmlHeadMetaDescription = 'seznam událostí web';
            break;
        }
    case 'attendantreviewsave':{
            define('AJAXREQUEST', false);
            if ($loggedUser) {
                require_once './content-event-review.php';
            } else {
                require_once './content-error-usernotlogged.php';
            }
            $htmlHeadTitle = 'Události - Hodnocení události';
            $htmlHeadMetaDescription = 'seznam událostí web';
            break;
        }
    case 'registrationnewphotosave':{

    }
    case 'registrationnewsave':{
            define('AJAXREQUEST', true);
            require_once './registration_form.php';
            break;
        }
    case 'eventdetail':
        {
            define('AJAXREQUEST', false);
            require_once './content-event-detail.php';
            $content_section_class = 'section-event-detail';
            $htmlHeadTitle = $eObj->get('title');
            $htmlHeadMetaDescription = $eObj->get('description');
            break;
        }
    case 'showMoreEvents':{
        define('AJAXREQUEST',true);
        require_once './ajax-show-more.php';
        break;
    }
    case 'eventfilter':{
        define('AJAXREQUEST',true);
        require_once './content-event-list.php';
        break;
    }
    case 'eventsList':
        {
            define('AJAXREQUEST',false);
            $htmlHeadTitle = 'Události';
            $htmlHeadMetaDescription = 'seznam událostí web';
            $eCount = 0;
            $showCategory = true;
            require_once './content-event-list.php';
            break;
        }
    case 'eventadministration':{
            define('AJAXREQUEST', false);
            if ($loggedUser) {
                if ($loggedUser->get('userrole') == 'organizer' || $loggedUser->get('userrole')=='admin') {
                    require_once './content-event-administration.php';
                } else {
                    require_once './content-error-usernotorganizer.php';
                }
            } else {
                require_once './content-error-usernotlogged.php';
            }
            $htmlHeadTitle = 'Události - Administrace událostí';
            $htmlHeadMetaDescription = 'seznam událostí web';
            break;
        }
    case 'eventreviews':{
            define('AJAXREQUEST', false);
            if ($loggedUser) {
                if ($loggedUser->get('userrole') == 'organizer') {
                    require_once './content-event-administration-reviews.php';
                } else {
                    require_once './content-error-usernotorganizer.php';
                }
            } else {
                require_once './content-error-usernotlogged.php';
            }
            $htmlHeadTitle = 'Události - Administrace událostí';
            $htmlHeadMetaDescription = 'seznam událostí web';
            break;
        }
    case 'eventdelete':{
            define('AJAXREQUEST', false);
            if ($loggedUser) {
                if ($loggedUser->get('userrole') == 'organizer' ||$loggedUser->get('userrole')=='admin') {
                    require_once './content-event-administration.php';
                } else {
                    require_once './content-error-usernotorganizer.php';
                }
            } else {
                require_once './content-error-usernotlogged.php';
            }
            $htmlHeadTitle = 'Události - Administrace událostí';
            $htmlHeadMetaDescription = 'seznam událostí web';
            break;
        }
    case 'getSpotPrice':{
        define('AJAXREQUEST', true);
        require_once './ajax-spotPrice.php';
        break;
    }
    case 'geocoding':{
            define('AJAXREQUEST', true);
            if ($loggedUser) {
                if ($loggedUser->get('userrole') == 'organizer') {
                    require_once './ajax-geocoding.php';
                } else {
                    die('ERROR:USERISNOTORGANIZER');
                }
            } else {
                die('ERROR:USERISNOTLOGGED');
            }
            break;
        }
    case 'uploadeventimage':{
            define('AJAXREQUEST', true);
            if ($loggedUser) {
                require_once './ajax-upload-image.php';
            } else {
                die('ERROR:USERISNOTLOGGED');
            }
            break;
        }
    case 'getPremiumDates':
        {
            define('AJAXREQUEST', true);
            require_once './ajax-premiumMonths.php';
            break;
        }
    case 'editevent':
        {
            define('AJAXREQUEST', false);
            //require_once './content-event-edit.php';
            if ($loggedUser) {
                if ($loggedUser->get('userrole') == 'organizer' || $loggedUser->get('userrole')=='admin') {
                    require_once './content-event-edit.php';
                } else {
                    require_once './content-error-usernotorganizer.php';
                }
            } else {
                require_once './content-error-usernotlogged.php';
            }
            $htmlHeadTitle = 'Editace události';
            $htmlHeadMetaDescription = 'seznam událostí web';
            break;
        }
    case 'eventnew':{
            define('AJAXREQUEST', false);
            if ($loggedUser) {
                if ($loggedUser->get('userrole') == 'organizer' || $loggedUser->get('userrole') == 'admin') {
                    require_once './content-event-new.php';
                } else {
                    require_once './content-error-usernotorganizer.php';
                }
            } else {
                require_once './content-error-usernotlogged.php';
            }
            $htmlHeadTitle = 'Události - Vytvoření nové události';
            $htmlHeadMetaDescription = 'seznam událostí web';
            break;
        }
    case 'registrationform':{
        define('AJAXREQUEST', false);
        require_once './registration_form.php';
        $htmlHeadTitle = 'Registrace organizátora';
        break;
    }
    case 'login':{
            define('AJAXREQUEST', true);
            require_once './modal-login.php';
            break;
        }
    case 'logout':{
            define('AJAXREQUEST', false);
            $htmlHeadTitle = 'Události - odhlášení';
            $htmlHeadMetaDescription = 'seznam událostí web';
            $additional_header = true;
            $content_section_class = 'section-hp';
            try {
                $currentUser = ParseUser::getCurrentUser();
                if ($currentUser) {
                    $currentUser->logOut();
                }
            } catch (ParseException $e) {
                echo $e->getMessage();
            }
            header("LOCATION: http://baavis.com");
            break;
        }
    case 'likeEvent':{
        define('AJAXREQUEST', true);
        require_once './ajax-like-click.php';
        break;
    }
    case 'admin' :{
        define('ADMINPANEL', true);
        define('AJAXREQUEST', false);
        if ($loggedUser && $loggedUser->get('userrole') == 'admin') {
            require_once './administration-panel.php';
        }
        else {
            require_once './content-error-usernotlogged.php';
        }
        $htmlHeadTitle = 'Administrace';
        $htmlHeadMetaDescription = 'Settings';
        $additional_header = false;
        $showCategory=false;
        $mainPage = false;
        break;
    }
    case 'deleteProfile':{
        define('AJAXREQUEST', true);
        if ($loggedUser && $loggedUser->get('userrole') == 'admin') {
            require_once './ajax-delete-profile.php';
        }
        break;
    }
    case 'generateuserinfo':
        {
            define("AJAXREQUEST",true);
            if ($loggedUser) {
                require_once './ajax-user-info.php';
            } else {
                require_once './content-error-usernotlogged.php';
            }
            break;
        }
    case 'cronDeleteEvents':
        {
            define("AJAXREQUEST",true);
            require_once './cron-delete-old-events.php';
            break;
        }
    case 'profile':{
        define("AJAXREQUEST", false);
        if ($loggedUser) {
            require_once './user-profile.php';
        } else {
            require_once './content-error-usernotlogged.php';
        }
        $htmlHeadMetaDescription = 'user profile';
        $htmlHeadTitle = 'My profile';
        break;
    }
    default:{
            define('AJAXREQUEST', false);
            $htmlHeadTitle = 'Události';
            $htmlHeadMetaDescription = 'seznam událostí web';
            $eCount = 0;
            $showCategory = true;
            if ($loggedUser || !empty($_POST['searchLocation'])) {
                require_once './content-event-list.php';
            } else {
                $additional_header = true;
                $content_section_class = 'section-hp';
                $mainPage = true;
                $eCount = 3;
                require_once './main_page.php';
            }

            break;
        }
}
define('HTML_HEAD_BASE', $htmlHeadBase);
define('HTML_HEAD_TITLE', $htmlHeadTitle);
define('HTML_HEAD_META_DESCRIPTION', $htmlHeadMetaDescription);
define('CONTAINER_CONTENT', ob_get_clean());


if (AJAXREQUEST !== true) {

// If not AJAX request print whole page

    require_once "./header.php";
    ?>

<!-- CONTAINER-BEGIN -->
    <main class="main-content" role="main" id="mainContent">
        <section class=<?php print $content_section_class ?>>
            <?php

    if(ADMINPANEL!==true)
    {

    ?>

            <header class="section-header">
                <div class="section-header-inner content-width">
                    <!-- There is a hiding title on tha main page. Bad, but it works. -->
                    <h1 class="section-header__title" <?php if($mainPage){echo 'style="font-size: 0em;"';}?>><?= $htmlHeadTitle?></h1>
                    <?php if($additional_header == true)
                        {?>
                            <a href="#" class="header-button"><button class="universal-button universal-button--red">Pojďte se bavit..</button></a>
                            <p class="section-header__p"><b style="color: white;">BAAVIS.com</b> je ojedinělí a výjimeční způsob, jak propagovat Vámi pořádané události, akce ve Vašem širokém okolí.</p>
                            <p class="section-header__p">
                                <b style="color: white;">Rychle, jednoduše a snadno</b>. Pořádáte sportovní akci, divadlo, kino, výstavu, auto-moto akci, koncert, akci v restauraci, zábavu atd. A chcete  <b style="color: white;">zvýšit návštěvnost</b>.  <b style="color: white;">Propagujte ji</b> zdarma na  <b style="color: white;">BAAVIS.com</b>.
                                Nabízíme Vám i  <b style="color: white;">exklusivní zvýhodněné prémiové umístění na BAAVIS.com</b>. pro vysokou sledovanost. Propagace na  <b style="color: white;">BAAVIS.com</b> je účinnější propagace než klasické bilboardy, plakáty atd.
                            </p>
                            <p class="section-header__p">Chcete se bavit, jít za zábavou atd. na  <b style="color: white;">BAAVIS.com</b> najdete vždy akci, událost podle Vašich představ.</p>

                        <?php } ?>
                </div>
            </header>
            <?php
            if($showCategory!=false)
            {?>
            <div class="menu-categories">
                <div class="menu-categories-inner content-width">
                    <input hidden id="eCount" value="<?=$eCount?>">
                    <ul class="menu-categories-list clearfix">
                        <li class="menu-categories-list__item active">Všechny</li>
                        <?php
                        foreach ($cat_name_array as $cat) {
                            ?>
                            <li class="menu-categories-list__item"><?= $cat ?></li>
                        <?php
                        }
                        ?>
                    </ul>
                    <script>
                        $('.menu-categories-list').on('click', 'li', function() {
                            var value = this.innerHTML;
                            var searchLocation = getQueryVariable('searchLocation');
                            var eCount = $("#eCount").val();
                            if(searchLocation === '')
                                searchLocation = $('input[name=searchLocation]').val();
                            if (value=='Všechny')
                                value = '';
                            $('.menu-categories-list li.active').removeClass('active');
                            var loadIcon = $("#loader");
                            $('.section-content').empty();
                            $('.section-content').append(loadIcon);
                            checkSkyscraperAvailable();
                            loadIcon.modal('show');
                            $(this).addClass('active');
                            $.ajax({
                                url: 'eventfilter',
                                type :'GET',
                                data : {
                                    'category' : value,
                                    'searchLocation': searchLocation,
                                    'eventsCount': eCount
                                },
                                success: function (result) {
                                    $('.section-content').append(result);
                                    $('html, body').animate({
                                        scrollTop: $('.section-content').offset().top
                                    }, 2000);

                                },
                                error: function(err){
                                    console.log(err);
                                },
                                complete: function(){
                                    loadIcon.modal('hide');
                                    checkSkyscraperAvailable();
                                }
                            });

                        });
                    </script>
                </div>
            </div>
                <?php }?>
            <!-- BANNERS -->
            <div class="banner-skyscraper-left" id="left_skyscraper">
                <?php
                $statusLeft = intval($banners['skyscraper-left']->get("status"));
                $statusRight = intval($banners['skyscraper-right']->get("status"));
                $statusRightBott = intval($banners['skyscraper-right-bott']->get("status"));
                $statusLeftBott = intval($banners['skyscraper-left-bott']->get("status"));

                if($statusLeft==1){
                    echo '<div class="banner-text" style="color:' . $banners['skyscraper-left']->get("barva_textu") . '; background-image: linear-gradient(to bottom right, ' . $banners['skyscraper-left']->get("barva_pozadi_1") . ',' . $banners['skyscraper-left']->get("barva_pozadi_2") . ');font-weight: 700;padding: 30px;height:' . $banners['skyscraper-left']->get("height") . '">';
                    echo $banners['skyscraper-left']->get("text");
                    echo "</div>";
                }
                else{
                    if (isset($banners['skyscraper-left'])) {
                        echo '<a href="//' . $banners['skyscraper-left']->get('link') . '"><img style="width:100%" src="' . $banners['skyscraper-left']->get('image')->getUrl() . '" alt="' . $banners['skyscraper-left']->get('alternative_text') . '" /></a>';
                    }
                }
                ?>
            </div>



        <div class="banner-skyscraper-left-bott" id="leftbott_skyscraper">
            <?php


            if($statusLeftBott==1){
                echo '<div class="banner-text" style="color:' . $banners['skyscraper-left-bott']->get("barva_textu") . '; background-image: linear-gradient(to bottom right, ' . $banners['skyscraper-left-bott']->get("barva_pozadi_1") . ',' . $banners['skyscraper-left-bott']->get("barva_pozadi_2") . ');font-weight: 700;padding: 30px;height:' . $banners['skyscraper-left-bott']->get("height") . '">';
                echo $banners['skyscraper-left-bott']->get("text");
                echo "</div>";
            }
            else{
                if (isset($banners['skyscraper-left-bott'])) {
                    echo '<a href="//' . $banners['skyscraper-left-bott']->get('link') . '"><img style="width:100%" src="' . $banners['skyscraper-left-bott']->get('image')->getUrl() . '" alt="' . $banners['skyscraper-left-bott']->get('alternative_text') . '" /></a>';
                }
            }
            ?>
        </div>




            <div class="banner-skyscraper-right" id="right_skyscraper">
                <?php
                if($statusRight==1){
                    echo '<div class="banner-text"  style="color:' . $banners['skyscraper-right']->get("barva_textu") . '; background-image: linear-gradient(to bottom right, ' . $banners['skyscraper-right']->get("barva_pozadi_1") . ',' . $banners['skyscraper-right']->get("barva_pozadi_2") . ');font-weight: 700;padding: 30px;height:' . $banners['skyscraper-right']->get("height") . '">';
                    echo $banners['skyscraper-right']->get("text");
                    echo "</div>";

                }else{
                    if(isset($banners['skyscraper-right'])) {
                        echo '<a href="//'.$banners['skyscraper-right']->get('link').'"><img style="width:100%" src="'.$banners['skyscraper-right']->get('image')->getUrl().'" alt="'.$banners['skyscraper-right']->get('alternative_text').'" /></a>';
                    }
                }
                ?>
            </div>



        <div class="banner-skyscraper-right-bott" id="rightbott_skyscraper">
            <?php


            if($statusRightBott==1){
                echo '<div class="banner-text" style="color:' . $banners['skyscraper-right-bott']->get("barva_textu") . '; background-image: linear-gradient(to bottom right, ' . $banners['skyscraper-right-bott']->get("barva_pozadi_1") . ',' . $banners['skyscraper-right-bott']->get("barva_pozadi_2") . ');font-weight: 700;padding: 30px;height:' . $banners['skyscraper-right-bott']->get("height") . '">';
                echo $banners['skyscraper-right-bott']->get("text");
                echo "</div>";
            }
            else{
                if (isset($banners['skyscraper-right-bott'])) {
                    echo '<a href="//' . $banners['skyscraper-right-bott']->get('link') . '"><img style="width:100%" src="' . $banners['skyscraper-right-bott']->get('image')->getUrl() . '" alt="' . $banners['skyscraper-right-bott']->get('alternative_text') . '" /></a>';
                }
            }
            ?>
        </div>


        <script>


            var banner_rightheight = $('.banner-skyscraper-right').height();
            $('.banner-skyscraper-right-bott').css('margin-top', banner_rightheight + 50 + "px");


            var banner_leftheight = $('.banner-skyscraper-left').height();
            $('.banner-skyscraper-left-bott').css('margin-top', banner_leftheight + 50 + "px");


            var banner_left = undefined;
            var banner_right = undefined;
            var banner_leftbott = undefined;
            var banner_rightbott = undefined;
            var footer = undefined;
            var footer_top = 0;

            $( document ).ready(function() {
                banner_left =  $('.banner-skyscraper-left');
                banner_right =  $('.banner-skyscraper-right');
                banner_leftbott=  $('.banner-skyscraper-left-bott');
                banner_rightbott =  $('.banner-skyscraper-right-bott');
                footer = document.querySelector("footer");
                footer_top = getTop(footer);
            });

            $(document).scroll(function() {
                var scrollVal = $(document).scrollTop();
                var main_section = $('.section-content');
                banner_left.css('top', scrollVal);
                banner_right.css('top', scrollVal);
                banner_leftbott.css('top', scrollVal);
                banner_rightbott.css('top', scrollVal);

                if (scrollVal < main_section.offset().top) {
                    banner_left.css('top', main_section.offset().top);
                    banner_right.css('top', main_section.offset().top);
                    banner_leftbott.css('top', main_section.offset().top);
                    banner_rightbott.css('top', main_section.offset().top);
                }

                if (banner_left.offset().top + banner_left.outerHeight() > footer_top) {
                    banner_left.css('top', footer_top - banner_left.outerHeight());
                    banner_right.css('top', footer_top - banner_right.outerHeight());
                    banner_leftbott.css('top', footer_top - banner_leftbott.outerHeight());
                    banner_rightbott.css('top', footer_top - banner_rightbott.outerHeight());
                }
            });

            function getTop(element) {
                var top = 0;
                do {
                    top += element.offsetTop  || 0;
                    element = element.offsetParent;
                } while(element);

                return top;
            }
        </script>
            <?
    }
    ?>
            <div class="section-container content-width" id="contentSection">
                <div id="loader" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
                    <div id="loaderSpinner"></div>
                </div>

                <?php print CONTAINER_CONTENT;?>
            </div>
        </section>
    </main>
<!-- CONTAINER-END -->
<?php
require_once "./footer.php";
} else {
    // If AJAX request print only result for AJAX
    print CONTAINER_CONTENT;
}
?>
