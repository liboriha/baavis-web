<?php
use Parse\ParseUser;

$user = ParseUser::getCurrentUser();

$firstName = $user->get("firstname");
$lastName = $user->get("lastname");
$email = $user->get("email");
$country = $user->get("country");
$zip = $user->get("zip");
$city = $user->get("city");
$street = $user->get("street");
$streetnumber = $user->get("streetnumber");
$userrole = $user->get("userrole");
$ICO=$user->get("ico");
$DIC=$user->get("dic");
$firmName = $user->get("firmname");

if(isset($_POST['deleteProfile']))
{
    $user->set('status',2);
    $user->save();
}

?>
<form id="userInfoForm" class="form-horizontal" action="">
    <!-- YOUR FIRST AND LAST NAME -->
    <div class="form-group">
        <div class="col-lg-6 col-sm-6 col-md-6">
            <strong>VAŠE JMÉNO</strong>
            <br>
            <input type="text" class="form-control" name="cFirstName" placeholder="Vaše jméno" <?php echo !empty($firstName)?'value="'.$firstName.'"':''?> readonly>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6">
            <strong>VAŠE PŘÍJMENÍ</strong>
            <br>
            <input type="text" class="form-control" name="cLastName" placeholder="Vaše příjmení" value="<?=$lastName?>" readonly>
        </div>
    </div>

    <!-- YOUR EMAIL AND COUNTRY -->
    <div class="form-group">
        <div class="col-lg-6 col-sm-6 col-md-6">
            <strong>VÁŠ EMAIL</strong>
            <br>
            <input type="text" class="form-control" name="cEmail" placeholder="Vaše emailová adresa" <?php echo !empty($email)?'value="'.$email.'"':''?> readonly>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6">
            <strong>VAŠE ZEMĚ</strong>
            <br>
            <input class="form-control" name="cCountry" value="<?=$country?>" readonly>
        </div>
    </div>

    <!-- YOUR FIRM NAME -->
    <div class="form-group" style="display:block;">
        <div class="col-lg-12 col-sm-12 col-md-12">
            <strong>Název firmy</strong>
            <br>
            <input type="text" class="form-control" name="cFirmName" placeholder="Název firmy" value="<?=$firmName?>" readonly>
        </div>
    </div>

    <div class="form-group">
        <div class="col-lg-6 col-sm-6 col-md-6">
            <strong>Vaše DIČ</strong>
            <br>
            <input type="text" class="form-control" name="cDIC" placeholder="Vaše DIČ" value="<?=$DIC?>" readonly>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6">
            <strong>Vaše IČO</strong>
            <br>
            <input type="text" class="form-control" name="cICO" placeholder="Vaše IČO" value="<?=$ICO?>" readonly>
        </div>
    </div>

    <!-- YOUR ZIP AND CITY -->
    <div class="form-group">
        <div class="col-lg-6 col-sm-6 col-md-6">
            <strong>VAŠE PSČ</strong>
            <br>
            <input type="text" class="form-control" name="cZip" placeholder="Vaše PSČ" value="<?=$zip?>" readonly>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6">
            <strong>VAŠE MĚSTO</strong>
            <br>
            <input type="text" class="form-control" name="cCity" placeholder="Vaše město" value="<?=$city?>" readonly>
        </div>
    </div>

    <!-- YOUR STREET AND STREET NUMBER -->
    <div class="form-group">
        <div class="col-lg-6 col-sm-6 col-md-6">
            <strong>VAŠE ULICE</strong>
            <br>
            <input type="text" class="form-control" name="cStreet" placeholder="Vaše ulice" value="<?=$street?>" readonly>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6">
            <strong>VAŠE ČÍSLO POPISNÉ</strong>
            <br>
            <input type="number" class="form-control" name="cStreetNumber" placeholder="Vaše číslo popisné" value="<?=$streetnumber?>" readonly>
        </div>
    </div>
</form>

<!-- REGISTER BUTTON -->
<div class="form-group">
    <div class="col-lg-12 col-sm-12 col-md-12">
        <button type="button" class="btn btn-primary" onclick="sendDeleteProfileRequest();">Zrušit účet a zapomenout osobní údaje</button>
        <a class="btn btn-success" href="/generateuserinfo" target="_blank">Vyžádání uložených osobních údajů</a>
        <script>
            function sendDeleteProfileRequest(){
                var http = new XMLHttpRequest();
                var url = '/profile';
                var params = 'deleteProfile=true';
                http.open('POST', url, true);

                //Send the proper header information along with the request
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

                http.onreadystatechange = function() {//Call a function when the state changes.
                    if(http.readyState == 4 && http.status == 200) {
                        alert("Žádost byla zaslána administrátorovi");
                    }
                }
                http.send(params);
            }
        </script>
    </div>
</div>

<script>

</script>