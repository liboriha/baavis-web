<?php
use Parse\ParseException;
use Parse\ParseQuery;

try{
    $spotNumber = intval($_GET['spot']);
    $spotCategoryNumber = intval($_GET['categorySpot']);
    $category = $_GET['category'];
    $price = -1;

    $spotQuery = new ParseQuery("PremiumSpots");
    $spotQuery->equalTo("spotNumber",$spotNumber);

    $spotInfo = $spotQuery->find();


    if(isset($spotCategoryNumber) && (isset($category)))
    {
        $spotCategoryQuery = new ParseQuery("PremiumSpots");
        $spotCategoryQuery->equalTo("spotNumber",$spotCategoryNumber);

        $spotCategoryInfo = $spotCategoryQuery->find();
    }
    if($spotInfo) {

        $pricesArrayFormated = [];
            $pricesArrayFormated["Všechny"] = $spotInfo[0]->get("PriceForCommon");
            if(isset($category))
            {
                $pricesArrayFormated["Sport"] = $spotCategoryInfo[0]->get("sportPrice");  
                $pricesArrayFormated["Kino"] = $spotCategoryInfo[0]->get("kinoPrice");  
                $pricesArrayFormated["Divadlo"] = $spotCategoryInfo[0]->get("theatrePrice");  
                $pricesArrayFormated["Výstava"] = $spotCategoryInfo[0]->get("exhibitionPrice");
                $pricesArrayFormated["Zážitky"] = $spotCategoryInfo[0]->get("experiencePrice");
                $pricesArrayFormated["Koncert"] = $spotCategoryInfo[0]->get("concertPrice");
                $pricesArrayFormated["Auto-Moto"] = $spotCategoryInfo[0]->get("autoPrice");
                $pricesArrayFormated["Zábava"] = $spotCategoryInfo[0]->get("funPrice");
                $pricesArrayFormated["Gastro"] = $spotCategoryInfo[0]->get("gastroPrice");
            }

        if (is_array($category)) {
            $price = $pricesArrayFormated[$category[0]] + "+" +  $pricesArrayFormated[$category[1]];
        }
        else {
            $price = $pricesArrayFormated[$category];
        }
    }
    echo $price;
}
catch (ParseException $exception){

}