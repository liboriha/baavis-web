<?php


try {
    //Find all old events
    $oldEventsFilter = new \Parse\ParseQuery("Events");
    $currentDate = new DateTime();
    $oldEventsFilter->lessThanOrEqualTo("endDate",$currentDate);
    $oldEventsAr = $oldEventsFilter->find();

    //Delete all old events
    foreach ($oldEventsAr as $item) {
        $item->destroy();
    }
}
catch (Exception $ex){
    //TODO: Need to log Exception
}

