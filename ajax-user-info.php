<?php
use Parse\ParseUser;

function generateUserInfoCsv(){
    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename="report.csv";');
    $user = ParseUser::getCurrentUser();
    $userInfo = [];
    $userInfo['headers'] = ['Jmeno', 'Příjmení', 'Email', 'Stat', 'Město', 'Zip', 'Ulice', 'Č.P', 'Accept for marketing'];
    $userInfo['values'] = [$user->get("firstname"), $user->get("lastname"),  $user->get("email"),  $user->get("country"),
        $user->get("city"), $user->get("zip"), $user->get("street"), $user->get("streetnumber"), $user->get("marketingAccepted")];

    $userrole = $user->get("userrole");
    if($userrole==='organizer') {
        array_push($userInfo['headers'],'IČO');
        array_push($userInfo['headers'],'DIČ');
        array_push($userInfo['headers'],'Název firmy');

        array_push($userInfo['values'], $user->get("ico"));
        array_push($userInfo['values'], $user->get("dic"));
        array_push($userInfo['values'], $user->get("firmname"));
    }

    # Start the ouput
    $output = fopen("php://output", "w");

    # Then loop through the rows
    foreach ($userInfo as $row) {
        # Add the rows to the body
        fputcsv($output, $row,';',' '); // here you can change delimiter/enclosure
    }
    # Close the stream off
    fclose($output);
}

generateUserInfoCsv();