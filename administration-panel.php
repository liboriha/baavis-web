<?php
use Parse\ParseFile;
$premiumPrice = new \Parse\ParseQuery("PremiumSpots");
$priceArr = $premiumPrice->find();

$pricesArrayFormated = [];

for($i=0;$i<6;$i++)
{
    $pricesArrayFormated[$i]["common"] = $priceArr[$i]->get("PriceForCommon");
    $pricesArrayFormated[$i]["Sport"] = $priceArr[$i]->get("sportPrice");
    $pricesArrayFormated[$i]["Kino"] = $priceArr[$i]->get("kinoPrice");
    $pricesArrayFormated[$i]["Divadlo"] = $priceArr[$i]->get("theatrePrice");
    $pricesArrayFormated[$i]["Výstava"] = $priceArr[$i]->get("exhibitionPrice");

    $pricesArrayFormated[$i]["Zážitky"] = $priceArr[$i]->get("experiencePrice");
    $pricesArrayFormated[$i]["Koncert"] = $priceArr[$i]->get("concertPrice");
    $pricesArrayFormated[$i]["Auto-Moto"] = $priceArr[$i]->get("autoPrice");
    $pricesArrayFormated[$i]["Zábava"] = $priceArr[$i]->get("funPrice");
    $pricesArrayFormated[$i]["Gastro"] = $priceArr[$i]->get("gastroPrice");
}

//Get events categories
$categories = new \Parse\ParseQuery("EventCategories");
$categories_array = $categories->find();
$cat_name_array = [];
for($ii = 0;$ii<count($categories_array);++$ii)
{
    $cat_name_array[$ii] = $categories_array[$ii]->get("name");
}

$topBannerQuery = new \Parse\ParseQuery("Banners");
$rightBannerQuery = new \Parse\ParseQuery("Banners");
$leftBannerQuery = new \Parse\ParseQuery("Banners");
$rightBottBannerQuery = new \Parse\ParseQuery("Banners");
$leftBottBannerQuery = new \Parse\ParseQuery("Banners");

$topBannerQuery->equalTo("type","leaderboard-top");
$rightBannerQuery->equalTo("type","skyscraper-right");
$leftBannerQuery->equalTo("type","skyscraper-left");
$rightBottBannerQuery->equalTo("type","skyscraper-right-bott");
$leftBottBannerQuery->equalTo("type","skyscraper-left-bott");

$topBanner = $topBannerQuery->find()[0];
$rightBanner = $rightBannerQuery->find()[0];
$leftBanner = $leftBannerQuery->find()[0];
$rightBottBanner = $rightBottBannerQuery->find()[0];
$leftBottBanner = $leftBottBannerQuery->find()[0];


//Save banners
if(isset($_POST['fileURLTop']) || isset($_POST['fileURLRight']) || isset($_POST['fileURLLeft']) || isset($_POST['fileURLLeftBott']) || isset($_POST['fileURLLeftBott']))
{

    $bannerRightHeight = $_POST['HeightRight'];
    $bannerLeftHeight = $_POST['HeightLeft'];
    $bannerRightBottHeight = $_POST['HeightRightBott'];
    $bannerLeftBottHeight = $_POST['HeightLeftBott'];


    $bannerTopFileUrl = $_POST['fileURLTop'];
    $bannerRightFileUrl = $_POST['fileURLRight'];
    $bannerLeftFileUrl = $_POST['fileURLLeft'];
    $bannerRightBottFileUrl = $_POST['fileURLRightBott'];
    $bannerLeftBottFileUrl = $_POST['fileURLLeftBott'];

    $bannerRightUrl = $_POST['UrlRight'];
    $bannerLeftUrl = $_POST['UrlLeft'];
    $bannerTopUrl = $_POST['UrlTop'];
    $bannerRightBottUrl = $_POST['UrlRightBott'];
    $bannerLeftBottUrl = $_POST['UrlLeftBott'];

    $bannerRightHtml = $_POST['htmlRight'];
    $bannerLeftHtml = $_POST['htmlLeft'];
    $bannerTopHtml = $_POST['htmlTop'];
    $bannerRightBottHtml = $_POST['htmlRightBott'];
    $bannerLeftBottHtml = $_POST['htmlLeftBott'];

    $bannerRightStatus = $_POST['rightStatus'];
    $bannerLeftStatus = $_POST['leftStatus'];
    $bannerTopStatus = $_POST['topStatus'];
    $bannerRightBottStatus = $_POST['rightBottStatus'];
    $bannerLeftBottStatus = $_POST['leftBottStatus'];


    $bannerRightBottText = $_POST['textRightBott'];
    $bannerRightBottBarva_1 = $_POST['barva_pozadi_1RightBott'];
    $bannerRightBottBarva_2 = $_POST['barva_pozadi_2RightBott'];
    $bannerRightBottBarvaTextu = $_POST['barva_textuRightBott'];


    $bannerLeftBottText = $_POST['textLeftBott'];
    $bannerLeftBottBarva_1 = $_POST['barva_pozadi_1LeftBott'];
    $bannerLeftBottBarva_2 = $_POST['barva_pozadi_2LeftBott'];
    $bannerLeftBottBarvaTextu = $_POST['barva_textuLeftBott'];


    $bannerRightText = $_POST['textRight'];
    $bannerRightBarva_1 = $_POST['barva_pozadi_1Right'];
    $bannerRightBarva_2 = $_POST['barva_pozadi_2Right'];
    $bannerRightBarvaTextu = $_POST['barva_textuRight'];


    $bannerLeftText = $_POST['textLeft'];
    $bannerLeftBarva_1 = $_POST['barva_pozadi_1Left'];
    $bannerLeftBarva_2 = $_POST['barva_pozadi_2Left'];
    $bannerLeftBarvaTextu = $_POST['barva_textuLeft'];


    $bannerTopText = $_POST['textTop'];
    $bannerTopBarva_1 = $_POST['barva_pozadi_1Top'];
    $bannerTopBarva_2 = $_POST['barva_pozadi_2Top'];
    $bannerTopBarvaTextu = $_POST['barva_textuTop'];


    $localFilePathTop = '';
    $localFilePathRight = '';
    $localFilePathLeft = '';
    $localFilePathRightBott = '';
    $localFilePathLeftBott = '';

    // SAVE A FILE (an image)
    if ($bannerTopFileUrl != null) {
        $localFilePathTop = $bannerTopFileUrl;
        $fileTop = ParseFile::createFromFile($localFilePathTop, "banner-horizontal.jpeg");
        $fileTop->save();

        $topBanner->set("image",$fileTop);

        $topBanner->save();
    }

    if(isset($bannerTopBarva_1))
    {
        $topBanner->set("barva_pozadi_1",$bannerTopBarva_1);
        $topBanner->save();
    }

    if(isset($bannerTopBarva_2))
    {
        $topBanner->set("barva_pozadi_2",$bannerTopBarva_2);
        $topBanner->save();
    }

    if(isset($bannerTopBarvaTextu))
    {
        $topBanner->set("barva_textu",$bannerTopBarvaTextu);
        $topBanner->save();
    }

    if(isset($bannerTopText))
    {
        $topBanner->set("text",$bannerTopText);
        $topBanner->save();
    }





    if(isset($bannerRightHeight))
    {
        $rightBanner->set("height",$bannerRightHeight);
        $rightBanner->save();
    }

    if(isset($bannerLeftHeight))
    {
        $leftBanner->set("height",$bannerLeftHeight);
        $leftBanner->save();
    }

    if(isset($bannerRightBottHeight))
    {
        $rightBottBanner->set("height",$bannerRightBottHeight);
        $rightBottBanner->save();
    }

    if(isset($bannerLeftBottHeight))
    {
        $leftBottBanner->set("height",$bannerLeftBottHeight);
        $leftBottBanner->save();
    }






    if(isset($bannerRightBarva_1))
    {
        $rightBanner->set("barva_pozadi_1",$bannerRightBarva_1);
        $rightBanner->save();
    }

    if(isset($bannerRightBarva_2))
    {
        $rightBanner->set("barva_pozadi_2",$bannerRightBarva_2);
        $rightBanner->save();
    }

    if(isset($bannerRightBarvaTextu))
    {
        $rightBanner->set("barva_textu",$bannerRightBarvaTextu);
        $rightBanner->save();
    }

    if(isset($bannerRightText))
    {
        $rightBanner->set("text",$bannerRightText);
        $rightBanner->save();
    }







    if(isset($bannerLeftBarva_1))
    {
        $leftBanner->set("barva_pozadi_1",$bannerLeftBarva_1);
        $leftBanner->save();
    }

    if(isset($bannerLeftBarva_2))
    {
        $leftBanner->set("barva_pozadi_2",$bannerLeftBarva_2);
        $leftBanner->save();
    }

    if(isset($bannerLeftBarvaTextu))
    {
        $leftBanner->set("barva_textu",$bannerLeftBarvaTextu);
        $leftBanner->save();
    }

    if(isset($bannerLeftText))
    {
        $leftBanner->set("text",$bannerLeftText);
        $leftBanner->save();
    }





    if(isset($bannerRightBottBarva_1))
    {
        $rightBottBanner->set("barva_pozadi_1",$bannerRightBottBarva_1);
        $rightBottBanner->save();
    }

    if(isset($bannerRightBottBarva_2))
    {
        $rightBottBanner->set("barva_pozadi_2",$bannerRightBottBarva_2);
        $rightBottBanner->save();
    }

    if(isset($bannerRightBottBarvaTextu))
    {
        $rightBottBanner->set("barva_textu",$bannerRightBottBarvaTextu);
        $rightBottBanner->save();
    }

    if(isset($bannerRightBottText))
    {
        $rightBottBanner->set("text",$bannerRightBottText);
        $rightBottBanner->save();
    }







    if(isset($bannerLeftBottBarva_1))
    {
        $leftBottBanner->set("barva_pozadi_1",$bannerLeftBottBarva_1);
        $leftBottBanner->save();
    }

    if(isset($bannerLeftBottBarva_2))
    {
        $leftBottBanner->set("barva_pozadi_2",$bannerLeftBottBarva_2);
        $leftBottBanner->save();
    }

    if(isset($bannerLeftBottBarvaTextu))
    {
        $leftBottBanner->set("barva_textu",$bannerLeftBottBarvaTextu);
        $leftBottBanner->save();
    }

    if(isset($bannerLeftBottText))
    {
        $leftBottBanner->set("text",$bannerLeftBottText);
        $leftBottBanner->save();
    }







    if(isset($bannerRightHtml))
    {
        $rightBanner->set("htmlCode",$bannerRightHtml);
        $rightBanner->save();
    }

    if(isset($bannerLeftHtml))
    {
        $leftBanner->set("htmlCode",$bannerLeftHtml);
        $leftBanner->save();
    }

    if(isset($bannerTopHtml))
    {
        $topBanner->set("htmlCode",$bannerTopHtml);
        $topBanner->save();
    }

    if(isset($bannerRightBottHtml))
    {
        $rightBottBanner->set("htmlCode",$bannerRightBottHtml);
        $rightBottBanner->save();
    }

    if(isset($bannerLeftBottHtml))
    {
        $leftBottBanner->set("htmlCode",$bannerLeftBottHtml);
        $leftBottBanner->save();
    }





    if(isset($bannerRightStatus))
    {
        $rightBanner->set("status",intval($bannerRightStatus));
        $rightBanner->save();
    }

    if(isset($bannerLeftStatus))
    {
        $leftBanner->set("status",intval($bannerLeftStatus));
        $leftBanner->save();
    }

    if(isset($bannerTopStatus))
    {
        $topBanner->set("status",intval($bannerTopStatus));
        $topBanner->save();
    }
    if(isset($bannerRightBottStatus))
    {
        $rightBottBanner->set("status",intval($bannerRightBottStatus));
        $rightBottBanner->save();
    }

    if(isset($bannerLeftBottStatus))
    {
        $leftBottBanner->set("status",intval($bannerLeftBottStatus));
        $leftBottBanner->save();
    }





    if(isset($bannerTopUrl))
    {
        $topBanner->set("link",$bannerTopUrl);
        $topBanner->save();
    }

    if(isset($bannerRightUrl))
    {
        $rightBanner->set("link",$bannerRightUrl);
        $rightBanner->save();
    }


    if ($bannerRightFileUrl != null) {
        $localFilePathRight = $bannerRightFileUrl;
        $fileRight = ParseFile::createFromFile($localFilePathRight, "banner.jpeg");
        $fileRight->save();

        $rightBanner->set("image",$fileRight);
        $rightBanner->save();
    }

    if(isset($bannerLeftUrl))
    {
        $leftBanner->set("link",$bannerLeftUrl);
        $leftBanner->save();
    }


    if ($bannerLeftFileUrl != null) {
        $localFilePathLeft = $bannerLeftFileUrl;
        $fileLeft = ParseFile::createFromFile($localFilePathLeft, "banner.jpeg");
        $fileLeft->save();

        $leftBanner->set("image",$fileLeft);
        $leftBanner->save();
    }


    if(isset($bannerRightBottUrl))
    {
        $rightBottBanner->set("link",$bannerRightBottUrl);
        $rightBottBanner->save();
    }


    if ($bannerRightBottFileUrl != null) {
        $localFilePathRightBott = $bannerRightBottFileUrl;
        $fileRightBott = ParseFile::createFromFile($localFilePathRightBott, "banner.jpeg");
        $fileRightBott->save();

        $rightBottBanner->set("image",$fileRightBott);
        $rightBottBanner->save();
    }

    if(isset($bannerLeftBottUrl))
    {
        $leftBottBanner->set("link",$bannerLeftBottUrl);
        $leftBottBanner->save();
    }


    if ($bannerLeftBottFileUrl != null) {
        $localFilePathLeftBott = $bannerLeftBottFileUrl;
        $fileLeftBott = ParseFile::createFromFile($localFilePathLeftBott, "banner.jpeg");
        $fileLeftBott->save();

        $leftBottBanner->set("image",$fileLeftBott);
        $leftBottBanner->save();
    }



}

//Get users for delete
$getUsersQuery = new \Parse\ParseQuery("_User");
$getUsersQuery->equalTo("status",2);
$usersList = $getUsersQuery->find();

//Get premium events
$getPremiumQuery = new \Parse\ParseQuery("Events");
$getPremiumQuery->notEqualTo("premiumSpotPayed", true);
$getPremiumQuery->notEqualTo("premiumSpot", null);

$premiumUnpaid = $getPremiumQuery->find();

?>
<div class="panel panel-default">
    <div class="panel-heading" data-acc-link="banner" data-toggle="collapse" data-target="#banner" aria-expanded="false" aria-controls="banner">
        Banners
    </div>
    <div class="panel-body collapse" id="banner" data-acc-content="banner">
        <div class="row">
            <div class="col-lg-6">
                <div class="fileupload fileupload-new" data-provides="fileupload" >
                    Left banner (160x600)
                    <div class="col-md-12">
                        <input type="number" placeholder="Výška" name="HeightLeft" form="Banners" value="<?=$leftBanner->get('height') ?>">
                    </div>
                    <div class="fileupload-new thumbnail" style="height: 610px;">
                        <!-- image -->
                        <img id="image-img-left" class="img-responsive center-cropped-avatar" src="<?=$leftBanner->get('image')->getURL()?>"/>
                    </div>
                    <div class="fileupload-preview fileupload-exists thumbnail" style="height: 120px;"></div>
                    <span class="btn btn-file btn-info">
                        <span class="fileupload-new">Vybrat obrázek</span>
                        <!-- ImageData input -->
                        <input id="imageDataLeft" name="file" type="file" accept="image/*" />
                    </span>
                    <br>
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <input type="radio" name="leftStatus" value="1" form="Banners" <?= $leftBanner->get('status')=='1'?'checked':'' ?>>
                            <p>TEXTOVĚ:</p>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <input type="radio" form="Banners" name="leftStatus" value="2" <?= $leftBanner->get('status')=='2'?'checked':'' ?>>
                            <p>URL:</p>
                            <input id="bannerLinkLeft" name="UrlLeft" form="Banners" type="text" value="<?=$leftBanner->get('link') ?>"/>
                        </div>
                    </div>


                    <div class="row">


                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Text:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="textLeft" form="Banners" type="text" value="<?=$leftBanner->get('text') ?>"/>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Barva textu:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="barva_textuLeft" form="Banners" type="text" value="<?=$leftBanner->get('barva_textu') ?>"/>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Barva horní části banneru:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="barva_pozadi_1Left" form="Banners" type="text" value="<?=$leftBanner->get('barva_pozadi_1') ?>"/>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Barva dolní části banneru:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="barva_pozadi_2Left" form="Banners" type="text" value="<?=$leftBanner->get('barva_pozadi_2') ?>"/>
                        </div>

                    </div>


                </div>
            </div>
            <div class="col-lg-6">
                <div class="fileupload fileupload-new" data-provides="fileupload" >
                    Right banner (160x600)
                    <div class="col-md-12">
                        <input type="number" placeholder="Výška" name="HeightRight" form="Banners" value="<?=$rightBanner->get('height') ?>">
                    </div>
                    <div class="fileupload-new thumbnail" style="height: 610px;">
                        <!-- image -->
                        <img id="image-img-right" class="img-responsive center-cropped-avatar" src="<?=$rightBanner->get('image')->getURL()?>" height="600" width="160" />
                    </div>
                    <div class="fileupload-preview fileupload-exists thumbnail" style="height: 120px;"></div>
                    <span class="btn btn-file btn-info">
                        <span class="fileupload-new">Vybrat obrázek</span>
                        <!-- ImageData input -->
                        <input id="imageDataRight" name="file" type="file" accept="image/*" />
                    </span>
                    <br>
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <input type="radio" name="rightStatus" value="1" form="Banners" <?= $rightBanner->get('status')=='1'?'checked':'' ?>>
                            <p>TEXTOVĚ:</p>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <input type="radio" form="Banners" name="rightStatus" value="2" <?= $rightBanner->get('status')=='2'?'checked':'' ?>>
                            <p>URL:</p>
                            <input id="bannerLinkRight" name="UrlRight" form="Banners" type="text" value="<?=$rightBanner->get('link') ?>"/>
                        </div>
                    </div>

                    <div class="row">


                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Text:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="textRight" form="Banners" type="text" value="<?=$rightBanner->get('text') ?>"/>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Barva textu:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="barva_textuRight" form="Banners" type="text" value="<?=$rightBanner->get('barva_textu') ?>"/>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Barva horní části banneru:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="barva_pozadi_1Right" form="Banners" type="text" value="<?=$rightBanner->get('barva_pozadi_1') ?>"/>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Barva dolní části banneru:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="barva_pozadi_2Right" form="Banners" type="text" value="<?=$rightBanner->get('barva_pozadi_2') ?>"/>
                        </div>

                    </div>



                </div>
            </div>
        </div>


        <br><br><br><br><br><br>
<!--test začátek-->
        <div class="row">
            <div class="col-lg-6">
                <div class="fileupload fileupload-new" data-provides="fileupload" >
                    Left banner bottom
                    <div class="col-md-12">
                        <input type="number" placeholder="Výška" name="HeightLeftBott" form="Banners" value="<?=$leftBottBanner->get('height') ?>">
                    </div>
                    <div class="fileupload-new thumbnail" style="height: 610px;">
                        <!-- image -->
                        <img id="image-img-left" class="img-responsive center-cropped-avatar" src="<?=$leftBottBanner->get('image')->getURL()?>"/>
                    </div>
                    <div class="fileupload-preview fileupload-exists thumbnail" style="height: 120px;"></div>
                    <span class="btn btn-file btn-info">
                        <span class="fileupload-new">Vybrat obrázek</span>
                        <!-- ImageData input -->
                        <input id="imageDataLeft" name="file" type="file" accept="image/*" />
                    </span>
                    <br>
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <input type="radio" name="leftBottStatus" value="1" form="Banners" <?= $leftBottBanner->get('status')=='1'?'checked':'' ?>>
                            <p>TEXTOVĚ:</p>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <input type="radio" form="Banners" name="leftBottStatus" value="2" <?= $leftBottBanner->get('status')=='2'?'checked':'' ?>>
                            <p>URL:</p>
                            <input id="bannerLinkLeft" name="UrlLeftBott" form="Banners" type="text" value="<?=$leftBottBanner->get('link') ?>"/>
                        </div>
                    </div>


                    <div class="row">


                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Text:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="textLeftBott" form="Banners" type="text" value="<?=$leftBottBanner->get('text') ?>"/>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Barva textu:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="barva_textuLeftBott" form="Banners" type="text" value="<?=$leftBottBanner->get('barva_textu') ?>"/>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Barva horní části banneru:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="barva_pozadi_1LeftBott" form="Banners" type="text" value="<?=$leftBottBanner->get('barva_pozadi_1') ?>"/>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Barva dolní části banneru:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="barva_pozadi_2LeftBott" form="Banners" type="text" value="<?=$leftBottBanner->get('barva_pozadi_2') ?>"/>
                        </div>

                    </div>


                </div>
            </div>
            <div class="col-lg-6">
                <div class="fileupload fileupload-new" data-provides="fileupload" >
                    Right banner bottom
                    <div class="col-md-12">
                        <input type="number" placeholder="Výška" name="HeightRightBott" form="Banners" value="<?=$rightBottBanner->get('height') ?>">
                    </div>
                    <div class="fileupload-new thumbnail" style="height: 610px;">
                        <!-- image -->
                        <img id="image-img-right" class="img-responsive center-cropped-avatar" src="<?=$rightBottBanner->get('image')->getURL()?>" height="600" width="160" />
                    </div>
                    <div class="fileupload-preview fileupload-exists thumbnail" style="height: 120px;"></div>
                    <span class="btn btn-file btn-info">
                        <span class="fileupload-new">Vybrat obrázek</span>
                        <!-- ImageData input -->
                        <input id="imageDataRight" name="file" type="file" accept="image/*" />
                    </span>
                    <br>
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <input type="radio" name="rightBottStatus" value="1" form="Banners" <?= $rightBottBanner->get('status')=='1'?'checked':'' ?>>
                            <p>TEXTOVĚ:</p>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <input type="radio" form="Banners" name="rightBottStatus" value="2" <?= $rightBottBanner->get('status')=='2'?'checked':'' ?>>
                            <p>URL:</p>
                            <input id="bannerLinkRight" name="UrlRightBott" form="Banners" type="text" value="<?=$rightBottBanner->get('link') ?>"/>
                        </div>
                    </div>

                    <div class="row">


                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Text:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="textRightBott" form="Banners" type="text" value="<?=$rightBottBanner->get('text') ?>"/>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Barva textu:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="barva_textuRightBott" form="Banners" type="text" value="<?=$rightBottBanner->get('barva_textu') ?>"/>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Barva horní části banneru:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="barva_pozadi_1RightBott" form="Banners" type="text" value="<?=$rightBottBanner->get('barva_pozadi_1') ?>"/>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Barva dolní části banneru:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="barva_pozadi_2RightBott" form="Banners" type="text" value="<?=$rightBottBanner->get('barva_pozadi_2') ?>"/>
                        </div>

                    </div>



                </div>
            </div>
        </div>

<!--        test konec-->

        <br><br><br><br><br><br>
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="fileupload fileupload-new" data-provides="fileupload">
                    Event banner (728x90)
                    <div class="fileupload-new thumbnail" style="height: 120px;">
                        <!-- image -->
                        <img id="image-img-top" class="img-responsive center-cropped-avatar" src="<?= $topBanner->get('image')->getURL()?>" height="160" width="400" />
                    </div>
                    <div class="fileupload-preview fileupload-exists thumbnail" style="height: 120px;"></div>
                    <span class="btn btn-file btn-info">
                        <span class="fileupload-new">Vybrat obrázek</span>
                        <!-- ImageData input -->
                        <input id="imageDataTop" name="file" type="file" accept="image/*" />
                    </span>
                    <br>
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <input type="radio" name="topStatus" value="1" form="Banners" <?= $topBanner->get('status')=='1'?'checked':'' ?>>
                            <p>TEXTOVĚ:</p>

                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <input type="radio" form="Banners" name="topStatus" value="2" <?= $topBanner->get('status')=='2'?'checked':'' ?>>
                            <p>URL:</p>
                            <input id="bannerLinkTop" name="UrlTop" form="Banners" type="text" value="<?= $topBanner->get('link') ?>"/>
                        </div>
                    </div>

                    <div class="row">


                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Text:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="textTop" form="Banners" type="text" value="<?=$topBanner->get('text') ?>"/>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Barva textu:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="barva_textuTop" form="Banners" type="text" value="<?=$topBanner->get('barva_textu') ?>"/>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Barva horní části banneru:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="barva_pozadi_1Top" form="Banners" type="text" value="<?=$topBanner->get('barva_pozadi_1') ?>"/>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6">
                            <p>Barva dolní části banneru:</p>
                            <input id="bannerLinkLeft" style="width: 100%" name="barva_pozadi_2Top" form="Banners" type="text" value="<?=$topBanner->get('barva_pozadi_2') ?>"/>
                        </div>

                    </div>
                    <!-- <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">Remove</a> -->
                    <!-- SUBMIT FORM -->
                </div>
            </div>
        </div>
            <form class="form-horizontal" method="post" id="Banners">
                <!-- Hidden fileURL input -->
                <input id="fileURLTxtTop" style="opacity:0;" size="50" type="text" name="fileURLTop" value="">
                <input id="fileURLTxtRight" style="opacity:0;" size="50" type="text" name="fileURLRight" value="">
                <input id="fileURLTxtLeft" style="opacity:0;" size="50" type="text" name="fileURLLeft" value="">
                <input id="fileURLTxtRightBott" style="opacity:0;" size="50" type="text" name="fileURLRightBott" value="">
                <input id="fileURLTxtLeftBott" style="opacity:0;" size="50" type="text" name="fileURLLeftBott" value="">
                <br>
                <button type="submit" class="btn btn-success">Save</button>
            </form>
    </div>
    <script>
        document.getElementById("imageDataLeft").onchange = function () {
            var reader = new FileReader();
            reader.onload = function (data) {

                document.getElementById("image-img-left").src = data.target.result;
                console.log(data.target.result);
                document.getElementById("image-img-left").onload = function () {

                    // Upload the selected image automatically into the 'uploads' folder
                    var filename = "avatar.jpg";
                    var data = new FormData();
                    data.append('file', document.getElementById('imageDataLeft').files[0]);

                    var websitePath = "<?php echo $_GLOBALS['WEBSITE_PATH'] ?>";

                    $.ajax({
                        url: "/uploadeventimage",
                        type: 'POST',
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            data = data.trim();
                            document.getElementById("fileURLTxtLeft").value = websitePath + data;
                        }, error: function (e) {
                            console.log(e);
                            alert("Něco se pokazilo, prosíme, zkuste akci opakovat! " + e);
                        }
                    });
                };
            };
            if (document.getElementById('imageDataLeft').files[0]) {
                reader.readAsDataURL(document.getElementById('imageDataLeft').files[0]);
            }
        };

        document.getElementById("imageDataRight").onchange = function () {
            var reader = new FileReader();
            reader.onload = function (data) {

                document.getElementById("image-img-right").src = data.target.result;
                console.log(data.target.result);
                document.getElementById("image-img-right").onload = function () {

                    // Upload the selected image automatically into the 'uploads' folder
                    var filename = "avatar.jpg";
                    var data = new FormData();
                    data.append('file', document.getElementById('imageDataRight').files[0]);

                    var websitePath = "<?php echo $_GLOBALS['WEBSITE_PATH'] ?>";

                    $.ajax({
                        url: "/uploadeventimage",
                        type: 'POST',
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            data = data.trim();
                            document.getElementById("fileURLTxtRight").value = websitePath + data;
                        }, error: function (e) {
                            console.log(e);
                            alert("Něco se pokazilo, prosíme, zkuste akci opakovat! " + e);
                        }
                    });
                };
            };
            if (document.getElementById('imageDataRight').files[0]) {
                reader.readAsDataURL(document.getElementById('imageDataRight').files[0]);
            }
        };

        document.getElementById("imageDataTop").onchange = function () {
            var reader = new FileReader();
            reader.onload = function (data) {

                document.getElementById("image-img-top").src = data.target.result;
                console.log(data.target.result);
                document.getElementById("image-img-top").onload = function () {

                    // Upload the selected image automatically into the 'uploads' folder
                    var filename = "avatar.jpg";
                    var data = new FormData();
                    data.append('file', document.getElementById('imageDataTop').files[0]);

                    var websitePath = "<?php echo $_GLOBALS['WEBSITE_PATH'] ?>";

                    $.ajax({
                        url: "/uploadeventimage",
                        type: 'POST',
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            data = data.trim();
                            document.getElementById("fileURLTxtTop").value = websitePath + data;
                        }, error: function (e) {
                            console.log(e);
                            alert("Něco se pokazilo, prosíme, zkuste akci opakovat! " + e);
                        }
                    });
                };
            };
            if (document.getElementById('imageDataTop').files[0]) {
                reader.readAsDataURL(document.getElementById('imageDataTop').files[0]);
            }
        };

    </script>
</div>
<div class="panel panel-default">
    <div class="panel-heading" data-acc-link="users" data-toggle="collapse" data-target="#users" aria-expanded="false" aria-controls="users">
        Users requests
    </div>
    <div class="panel-body collapse" id="users" data-acc-content="users">
        <table class="table" id="usersTable">
            <thead>
                <tr>
                    <th scope="col">UserName</th>
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i=0;
                foreach($usersList as $user)
                {
                    if($user->get("status")==2)
                    {
                        echo '<tr>';
                        echo '<td>'.$user->get("username").'</td>';
                        echo '<td>Delete requested</td>';
                        echo '<td><btn onclick="deleteUser(\''.$user->getObjectId().'\');">Delete</btn></td>';
                        echo '</tr>';
                        $i++;
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading" data-acc-link="spotPrice" data-toggle="collapse" data-target="#spotPrice" aria-expanded="false" aria-controls="spotPrice">
        Premium spot price
    </div>
    <div class="panel-body collapse" id="spotPrice" data-acc-content="spotPrice">
            <form class="form-horizontal" name="premiumSpotSettings" action="/premiumSpotPrice" method="post" id="premiumSpotsPrice">
                <!-- TITLE -->
                <div class="form-group">
                <div class="col-sm-12 col-lg-12 col-md-12">
                    <div class="col-sm-4 col-lg-4 col-md-4">
                            <select class="form-control" id="catSelect">
                                <option value="common">Všechny</option>
                                <?php
                                foreach ($cat_name_array as $cat) {
                                ?>
                                    <option value="<?=$cat?>"><?= $cat ?></option>
                                <?php
                                }
                                ?>
                            </select>
                    </div>
                    </div>
                    <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 5px">
                        <div class="col-sm-6 col-lg-6 col-md-6">
                            <strong>Spot 1</strong>
                            <input type="number" class="form-control" data-spot="0" id="Spot1" name="Spot1" onblur="valueChanged(0);" value="<?=$priceArr[0]->get("PriceForCommon")?>">
                        </div>
                        <div class="col-sm-6 col-lg-6 col-md-6">
                            <strong>Spot 2</strong>
                            <input type="number" class="form-control" data-spot="1" id="Spot2" name="Spot2" onblur="valueChanged(1);" value="<?=$priceArr[1]->get("PriceForCommon")?>">
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 5px">
                        <div class="col-sm-6 col-lg-6 col-md-6">
                            <strong>Spot 3</strong>
                            <input type="number" class="form-control" data-spot="2" id="Spot3" name="Spot3" onblur="valueChanged(2);" value="<?=$priceArr[2]->get("PriceForCommon")?>">
                        </div>
                        <div class="col-sm-6 col-lg-6 col-md-6">
                            <strong>Spot 4</strong>
                            <input type="number" class="form-control" data-spot="3" id="Spot4" name="Spot4" onblur="valueChanged(3);" value="<?=$priceArr[3]->get("PriceForCommon")?>">
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 5px">
                    <div class="col-sm-6 col-lg-6 col-md-6">
                            <strong>Spot 5</strong>
                            <input type="number" class="form-control" data-spot="4" id="Spot5" name="Spot5" onblur="valueChanged(4);" value="<?=$priceArr[4]->get("PriceForCommon")?>">
                        </div>
                        <div class="col-sm-6 col-lg-6 col-md-6">
                            <strong>Spot 6</strong>
                            <input type="number" class="form-control" data-spot="5" id="Spot6" name="Spot6" onblur="valueChanged(5);" value="<?=$priceArr[5]->get("PriceForCommon")?>">
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-12 col-md-12" style="margin-bottom: 5px">
                        <button type="button" class="btn btn-success" onclick="save_price();">Uložit</button>
                    </div>
                </div>
            </form>
            <script>
                var prices = <?php echo json_encode($pricesArrayFormated); ?>;
                const spot1 = document.getElementById("Spot1");
                const spot2 = document.getElementById("Spot2");
                const spot3 = document.getElementById("Spot3");
                const spot4 = document.getElementById("Spot4");
                const spot5 = document.getElementById("Spot5");
                const spot6 = document.getElementById("Spot6");


                const category_select = document.getElementById("catSelect");
                $('#catSelect').on('change', function() {
                    spot1.value = prices[0][this.value];
                    spot2.value = prices[1][this.value];
                    spot3.value = prices[2][this.value];
                    spot4.value = prices[3][this.value];
                    spot5.value = prices[4][this.value];
                    spot6.value = prices[5][this.value];

                });

                function valueChanged(spot){
                    spotN = spot + 1;
                    const spot_input = document.getElementById("Spot"+spotN);
                    prices[spot][category_select.options[category_select.selectedIndex].value] = spot_input.value;
                }

                function save_price(){
                    $.ajax({
                        type: 'POST',
                        url: '/ajaxspotsave',
                        data: {prices},
                        dataType: 'json'
                    });

                }
            </script>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading" data-acc-link="spotStatus" data-toggle="collapse" data-target="#spotStatus" aria-expanded="false" aria-controls="spotStatus">
        Premium spot status
    </div>
    <div class="panel-body collapse" id="spotStatus" data-acc-content="spotStatus">
    <table class="table" id="usersTable">
            <thead>
                <tr>
                    <th scope="col">Událost</th>
                    <th scope="col">Premium spot</th>
                    <th scope="col">Datum kategorie</th>
                    <th scope="col">Datum</th>
                    <th scope="col">Cena</th>
                    <th scope="col">Oveření</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach($premiumUnpaid as $event)
                {
                    $categoryRelations = $event->getRelation('categories');
                    $categoryRelationsQuery = $categoryRelations->getQuery();

                    $currentCategories = $categoryRelationsQuery->find();

                    $cost = 0;
                        echo '<tr>';
                        echo '<td><a href="eventdetail/'.$event->getObjectId().'">'.$event->get("title").'</a></td>';
                        echo '<td>'.$event->get("premiumSpot").'</td>';
                        if(!empty($event->get("unpaidPremiumCategory")))
                        {
                            $cost+=(count($event->get("unpaidPremiumCategory"))-1)*intval($pricesArrayFormated[$event->get("premiumSpot")][$currentCategories[0]->get("name")]);
                            echo '<td>'.implode("; ",$event->get("unpaidPremiumCategory")).'</td>';
                        }
                        else echo '<td></td>';
                        if(!empty($event->get("unpaidPremiumCommon")))
                        {
                            echo '<td>'.implode("; ",$event->get("unpaidPremiumCommon")).'</td>';
                            $cost+=(count($event->get("unpaidPremiumCommon"))-1)*intval($pricesArrayFormated[$event->get("premiumSpot")]["common"]);
                        }
                        else echo '<td></td>';
                        echo '<td>'.$cost.'</td>';
                        echo '<td><btn class="btn btn-success" onclick="acceptEvent(\''.$event->getObjectId().'\')">Accept</btn></td>';
                        echo '</tr>';
                }
                ?>
            </tbody>
        </table>
    </div>
</div>

<script>
    function deleteUser(user_id){
        const users_table = document.getElementById("usersTable");
        $.ajax({
                        url: 'deleteProfile',
                        type: 'GET',
                        data: {
                            userId:user_id
                        },
                        contentType: false,
                        success: function (data) {
                            if(data === "OK")
                            {
                                location.reload();
                            }
                            else
                               console.log(data);
                        }, error: function (e) {
                            console.log(e);
                        }
                    });
    }

    function acceptEvent(event_id){

        $.ajax({
            url: '/ajaxeventpremiumaccept',
            type: 'GET',
            data: {
                eventId:event_id
            },
            contentType: false,
            success: function (data) {

                if(data = true)
                {

                    location.reload();
                }
                else
                    alert("error");
                    console.log(data);
            },
            error: function (e) {

                console.log(e);
            }
        });
    }
</script>