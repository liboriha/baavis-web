<?php
use Parse\ParseException;
use Parse\ParseQuery;
?>

<!-- BEGIN:PREMIUM SPOT CALENDAR MODAL -->
<script>
    var monthNames = ['Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosinec'];
    var today = new Date();
    var premiumSpotCalendar_spot = 1;
    var premiumSpotCalendar_spotCategory = 1;
    var premiumSpotCalendar_year = today.getFullYear();
    var premiumSpotCalendar_month = today.getMonth();
    var premiumSpotStartDate = null;
    var premiumSpotEndDate = null;
    var spotPrice = 0;

    function getSpotPrice(spot, categorySpot, category){
        $.ajax({
            url:"/getSpotPrice",
            type: 'GET',
            data:{
                'spot':spot,
                'categorySpot': categorySpot,
                'category':category
            },
            contentType:false,
            success:function(price)
            {

                spotPrice = price;
                $("#spotPriceParagraph")[0].textContent = "Cena za den: " + price + " Kč";


                $('#datepicker').on("changeDate", function () {
                    var the_dates = $('#datepicker').datepicker('getDates');

                    //this does not work

                    $("#spotPriceParagraph2")[0].textContent = "Cena za vybrané období: " + price * the_dates.length + " Kč";


                });


                $('#datepicker_category').on("changeDate", function () {
                    var the_dates2 = $('#datepicker_category').datepicker('getDates');

                    //this does not work

                    $("#spotPriceParagraph2")[0].textContent = "Cena za vybrané období: " + price * the_dates2.length + " Kč";


                });

            }
            });
    }
    function setPremiumSpot(spot, categorySpot) {
        showLoadingModal();
        var category = document.querySelector('input[name = "category"]:checked').getAttribute('data-label').split('+');
        getSpotPrice(spot, categorySpot, category);
        
        $.ajax({
            url: "/getPremiumDates",
            type: 'GET',
            data: {
                'spot': spot,
                'categorySpot': categorySpot,
                'category': category
            },
            contentType: false,
            success: function (data) {
                var datesToDisable = [];
                var datesToDisable = JSON.parse(data);

                var months = ['Led', 'Úno', 'Bře', 'Dub', 'Kvě', 'Čer',
                    'Čnc', 'Srp', 'Zář', 'Říj', 'Lis', 'Pro'];




                $('#datepicker').datepicker({
                    format: "yyyy-mm-dd",

                    language: "cs",
                    toggleActive: true,
                    startDate: new Date(),
                    multidate:true,
                    endDate: '+24m'

                }).on("show", function (event) {



                    var currentYear = (new Date()).getFullYear();
                    var currentMonth = (new Date()).getMonth();
                    var year = $("th.datepicker-switch").eq(1).text();  // there are 3 matches
                    $(".month").each(function (index, element) {
                        var el = $(element);

                        var hideMonth = $.grep(datesToDisable['common'], function (n, i) {
                            return n.substr(0, 4) == year && months[parseInt(n.substr(5, 1)) - 1] == el.text();
                        });

                        if (hideMonth.length)
                            el.addClass('disabled');
                        else if (year > currentYear || (index >= currentMonth))
                            el.removeClass('disabled');

                    });
                });

                $('#datepicker_category').datepicker({
                    format: "yyyy-mm-dd",
                    // startView: 1,
                    // minViewMode: 1,
                    // maxViewMode: 2,
                    language: "cs",
                    toggleActive: true,
                    startDate: new Date(),
                    multidate:true,
                    endDate: '+24m'
                }).on("show", function (event) {
                    var currentYear = (new Date()).getFullYear();
                    var currentMonth = (new Date()).getMonth();
                    var year = $("th.datepicker-switch").eq(1).text();  // there are 3 matches
                    $(".month").each(function (index, element) {
                        var el = $(element);

                        var hideMonth = $.grep(datesToDisable['category'], function (n, i) {
                            return n.substr(0, 4) == year && months[parseInt(n.substr(5, 2)) - 1] == el.text();
                        });

                        if (hideMonth.length)
                            el.addClass('disabled');
                        else if (year > currentYear || (index >= currentMonth))
                            el.removeClass('disabled');
                    });
                });
            }, error: function (e) {
                console.log(e);
            },
            complete: function () {
                hideLoadingModal();
            }
        });
        premiumSpotCalendar_spot = spot;
        premiumSpotCalendar_spotCategory = categorySpot;
        premiumSpotStartDate = null;
        premiumSpotEndDate = null;
    }

    function setPremiumSpotForEvent () {
        if(document.getElementById("startDate").value) {
            const dates = document.getElementById("startDate").value;
            const dates_array = dates.split(',');
            var premium_dates = "";


            const totalDatesArray = dates_array.length;
            var counter = 1;
            dates_array.forEach(function (item, i, dates_array) {
                // const date = new Date(item);
                // const end_date = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                // const end_date_string = end_date.getFullYear() + '-' + (("0" + (end_date.getMonth() + 1)).slice(-2)) + '-' + end_date.getDate();
                // premium_dates = premium_dates + end_date_string + ",";

                if( counter < totalDatesArray ) {
                    premium_dates = premium_dates + item + ",";
                } else {
                    premium_dates = premium_dates + item;
                }

                counter++;
            });

            const isForCategory = document.querySelector('input[name = "category"]:checked').value;

            $('#eventnew-premiumspotposition').val(premiumSpotCalendar_spot);
            $('#eventnew-isPremiumSpotForCategroy').val(isForCategory);
            $('#eventnew-premiumspotstartdate').val(premium_dates);
            $('#eventnew-premiumspotposition-text').html('Prémiový spot: ' + premiumSpotCalendar_spot + ';').css('display', 'inline-block');
            $('#eventnew-premiumspotstartdate-text').html(' Datum: ' + premium_dates);
        }
        else {
            $('#eventnew-premiumspotposition').val(null);
            $('#eventnew-premiumspotstartdate').val(null);
            $('#eventnew-premiumspotenddate').val(null);
            $('#eventnew-premiumspotposition-text').html(null);
            $('#eventnew-premiumspotstartdate-text').html(null);
            $('#eventnew-premiumspotenddate-text').html(null);
        }

        if(document.getElementById("startDate_category").value) {

            const dates = document.getElementById("startDate_category").value;
            const dates_array = dates.split(',');
            var premium_dates = "";

            dates_array.forEach(function (item, i, dates_array) {
                const date = new Date(item);
                const end_date = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                const end_date_string = end_date.getFullYear() + '-' + (("0" + (end_date.getMonth() + 1)).slice(-2)) + '-' + end_date.getDate();
                premium_dates = premium_dates + end_date_string + ",";
            });

            const isForCategory = document.querySelector('input[name = "category"]:checked').value;

            $('#eventnew-premiumspotposition_category').val(premiumSpotCalendar_spotCategory);
            $('#eventnew-premiumspotstartdate_category').val(premium_dates);
            $('#eventnew-premiumspotposition-text_category').html('Prémiový spot (kategorie): ' + premiumSpotCalendar_spotCategory + ';').css('display', 'inline-block');
            $('#eventnew-premiumspotstartdate-text_category').html(' Datum: ' + premium_dates);
            }
            else {
            $('#eventnew-premiumspotposition_category').val(null);
            $('#eventnew-premiumspotstartdate_category').val(null);
            $('#eventnew-premiumspotenddate_category').val(null);
            $('#eventnew-premiumspotposition-text_category').html(null);
            $('#eventnew-premiumspotstartdate-text_category').html(null);
            $('#eventnew-premiumspotenddate-text_category').html(null);
            }
        $('#premiumSpotCalendarModal-button-close').trigger('click');
    }
</script>
<div id="premiumSpotCalendarModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="premumSpotCalendarModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button id="premiumSpotCalendarModal_buttonClose" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="premiumSpotCalendarModalLabel">Prémiové umístění</h4>
                <p>Zvolte období pro prémiové umístění.</p>
                <p id="spotPriceParagraph">Cena za den: 0 Kč</p>
                <p id="spotPriceParagraph2">Cena za vybrané období: 0 Kč</p>
            </div>

            <div class="modal-body">
                <!-- BEGIN:PREMIUM SPOT CALENDAR FORM -->
                <!-- <form id="formPremiumSpotCalendar" class="form-horizontal"> -->
                <div class="row">
                    <input type="radio" id="catPremium" name="category" data-label="Všechny" data-number=0 value="0" checked>
                    <label id="catPremiumLabel" for="catPremium">Všechny</label>
                    <input type="radio" id="catPremium2" name="category" data-label="" data-number=1 value="1">
                    <label id="catPremium2Label" for="catPremium2"></label>
                 <!--   <input type="radio" id="catPremium3" name="category" data-label="" data-number=2 value="1">
                    <label id="catPremium3Label" for="catPremium3"></label>-->
                </div>
                <div class="row" style="margin: 10px">
                    <select class="form-control" id="premiumSpot" name="premiumspot" onchange="setPremiumSpot(this.value, $('#premiumSpot_category')[0].value)">
                        <option value="1">premium spot 1</option>
                        <option value="2">premium spot 2</option>
                        <option value="3">premium spot 3</option>
                        <option value="4">premium spot 4</option>
                        <option value="5">premium spot 5</option>
                        <option value="6">premium spot 6</option>
                    </select>
                </div>
                <div class="row" style="display:none" id="category_label">
                    Kategorie spot:
                </div>
                <div class="row" style="margin: 10px">
                    <select class="form-control" style="display:none" id="premiumSpot_category" name="premiumSpot_category" onchange="setPremiumSpot($('#premiumSpot')[0].value, this.value)">
                        <option value="1">premium spot 1</option>
                        <option value="2">premium spot 2</option>
                        <option value="3">premium spot 3</option>
                        <option value="4">premium spot 4</option>
                        <option value="5">premium spot 5</option>
                        <option value="6">premium spot 6</option>
                    </select>
                </div>
                <div class="input-group date" id="datepicker">
                    <input type="text" class="form-control" id="startDate">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
                <br>
                <div class="input-group date" id="datepicker_category" style="display:none">
                    <input type="text" class="form-control" id="startDate_category">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>



                <!-- PREMIUM SPOT CALENDAR SELECT BUTTON -->
                <div class="form-group" style="margin-top:5px">
                    <div class="text-center">
                        <a onclick="setPremiumSpotForEvent()" class="btn btn-primary">NASTAVIT</a>
                    </div>
                </div>
                <!-- END:PREMIUM SPOT CALENDAR SELECT BUTTON -->

                <!-- </form> -->
                <!-- END:PREMIUM SPOT CALENDAR FORM -->
            </div>
            <!-- BEGIN:CLOSE BUTTON -->
            <div class="modal-footer">
                <a id="premiumSpotCalendarModal-button-close" class="btn btn-default" data-dismiss="modal">Zavřít</a>
            </div>
            <!-- END:CLOSE BUTTON -->

        </div>
    </div>
</div>
<!-- END:PREMIUM SPOT CALENDAR MODAL -->

<script>
    $(document).ready(function() {
        setPremiumSpot(1,1);
    });

    $(document).on("change","input[name=category]",function(){
        setPremiumSpot($("#premiumSpot")[0].value, ($("#premiumSpot_category")[0].value));
        const number = this.dataset['number'];
        document.getElementById("startDate").value = '';
        document.getElementById("startDate_category").value = '';
        if(number == 0)
        {
            $("#datepicker").css('display','inline-table');        
            $("#datepicker_category").css('display','none');
            $("#premiumSpot").css('display','inline-table');        
            $("#premiumSpot_category").css('display','none');
            $("#category_label").css('display','none');
            
        }
        else if (number==1)
        {
            $("#datepicker").css('display','none');
            $("#datepicker_category").css('display','inline-table');
            $("#premiumSpot").css('display','none');
            $("#premiumSpot_category").css('display','inline-table');
            $("#category_label").css('display','inline-table');
        }
        else if (number==2)
        {
            $("#datepicker").css('display','inline-table');
            $("#datepicker_category").css('display','inline-table');
            $("#premiumSpot").css('display','inline-table');
            $("#premiumSpot_category").css('display','inline-table');
            $("#category_label").css('display','inline-table');
        }
    });

    $("#premiumSpotCalendarModal").on("hidden.bs.modal", function () {
        hideLoadingModal();
    });
</script>
