<?php
$address = $params[0];

try {
    if (!empty($address)) {
        $url = str_replace('<ADDRESS>', urlencode($address), GEOCODING_API_URL);
        $response = file_get_contents($url);
        $json = json_decode($response, true);
        $lat = $json['results'][0]['geometry']['location']['lat'];
        $lng = $json['results'][0]['geometry']['location']['lng'];
 
        echo 'OK:'.$lat.':'.$lng;

    } else {
        echo 'ERROR:EMPTYADDRESS';
    }
} catch (Exception $e) {
    echo 'ERROR:SERVERFAILURE';
}