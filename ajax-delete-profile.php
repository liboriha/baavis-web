<?php 

$userId = $_GET['userId'];

if (isset($userId))
{
    $userQuery = new \Parse\ParseQuery("_User");

    $userQuery->equalTo("objectId",$userId);

    $userAr = $userQuery->find();

    if(count($userAr)>0)
    {
        $user = $userAr[0];

        $user->destroy(true);
        echo "OK";
    }
    else 
        echo "ERROR";
}