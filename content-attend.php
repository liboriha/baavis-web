<?php
use Parse\ParseObject;
use Parse\ParseUser;

if (AJAXREQUEST) {
    // Resolve AJAX request
    // Check if event id is set
    if (!isset($params[0]) || empty($params[0])) {
        die('ERROR:NOTEVENTID');
    }

    // Check if event exists and not expired
    try {
        $event = new ParseObject('Events', $params[0]);
        $event->fetch();
        if($event->get('endDate')->getTimestamp() <= time()) {
          die('ERROR:EVENTEXPIRED');
        }

    } catch (ParseException $e) {
        die('ERROR:EVENTNOTEXISTS');
    }

    // Get current user
    $currentUser = ParseUser::getCurrentUser();
    if (!$currentUser) {
        die('ERROR:USERISNOTLOGGED');
    }

    // Attend user to this event
    try {
        $eventUserRelation = $event->getRelation('attenders');
        $eventUserRelation->add($currentUser);
        $event->save();
        die('OK');
    } catch (ParseException $e) {
        die('ERROR:ADDATTENDER');
    }
}
