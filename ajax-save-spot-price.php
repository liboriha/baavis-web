<?php
use Parse\ParseException;
use Parse\ParseQuery;

try{
    $premiumPrice = new ParseQuery("PremiumSpots");
    $priceArr = $premiumPrice->find();
    for($i = 0;$i<6;$i++){
        $priceArr[$i]->set("PriceForCommon",intval($_POST["prices"][$i]["common"]));
        $priceArr[$i]->set("sportPrice",intval($_POST["prices"][$i]["Sport"]));
        $priceArr[$i]->set("kinoPrice",intval($_POST["prices"][$i]["Kino"]));
        $priceArr[$i]->set("theatrePrice",intval($_POST["prices"][$i]["Divadlo"]));
        $priceArr[$i]->set("exhibitionPrice",intval($_POST["prices"][$i]["Výstava"]));

        $priceArr[$i]->set("experiencePrice",intval($_POST["prices"][$i]["Zážitky"]));
        $priceArr[$i]->set("concertPrice",intval($_POST["prices"][$i]["Koncert"]));
        $priceArr[$i]->set("autoPrice",intval($_POST["prices"][$i]["Auto-Moto"]));
        $priceArr[$i]->set("funPrice",intval($_POST["prices"][$i]["Zábava"]));
        $priceArr[$i]->set("gastroPrice",intval($_POST["prices"][$i]["Gastro"]));


        $priceArr[$i]->save();
    }
}
catch (ParseException $exception){

}