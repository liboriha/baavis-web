<?php
use Parse\ParseException;
use Parse\ParseQuery;
use Parse\ParseUser;

/* Variables */
$ownerObject = ParseUser::getCurrentUser();
try {
    if ($action == 'eventdelete') {
        if (empty($params[0])) {
            echo '<script>alert("Událost nebyla nalezena.");</script>';
        } else {
            $query = new ParseQuery('Events');
            $query->equalTo('objectId', $params[0]);
            $query->equalTo('owner', $ownerObject);
            $result = $query->find();
            if (count($result) != 1) {
                echo '<script>alert("Událost nebyla nalezena.");</script>';
            } else {
                $deleteEvent = $result[0];

                $deleteEvent->destroy();
                echo '
                    <div class="text-center">
                        <div class="alert alert-success">Vaše událost byla smazána.</div>
                    </div>';

                // Get attenders to send them email about cancelation os this event
                $attendersRelation = $deleteEvent->getRelation('attenders');
                $query = $attendersRelation->getQuery();
                $result = $query->find();
                for ($i = 0; $i < count($result); $i++) {
                    $to = $result[$i]->get('email');
                    $subject = "UDÁLOSTI | Zrušení události!";
                    $message = "Událost '" . $deleteEvent->get('title') . "' byla zrušena.";
                    $headers = "From: " . ADMIN_EMAIL;
                    mail($to, $subject, $message, $headers);
                }
            }
        }
    }
} catch (ParseException $ex) {
    echo '<script>alert("Nastala chyba při vykonávání operace. Opakujte, prosím, akci později.");</script>';
}
?>
<div class="section-content">
<?php
// QUERY EVENTS -----------------------------------
try {

    $query = new ParseQuery('Events');
    if($ownerObject->getUsername()!=='admin') {
        $query->equalTo('owner', $ownerObject);
    }
    $query->ascending('startDate');

    $query->limit(20);

    // Find objects
    $evArray = $query->find();

    for ($i = 0; $i < count($evArray); $i++) {
        // Get Parse Object
        $eObj = $evArray[$i];
        $eObjID = $eObj->getObjectId();

        // Get image
        $file = $eObj->get('image');
        $imageURL = $file->getURL();

        // Get title
        $title = $eObj->get('title');
        $title = substr($title, 0, 25);

        // Get location
        $location = $eObj->get('location');

        // Get cost
        $cost = $eObj->get('cost');

        // Get start date
        $sDate = $eObj->get('startDate');
        $startDate = date_format($sDate, "M d Y | @H:i A");

        // Get end date
        $eDate = $eObj->get('endDate');
        $endDate = date_format($eDate, "d M Y | @H:i A");

        // Get description
        $description = $eObj->get('description');
        $description = substr($description, 0, 80);
        ?>

							<!-- Event cell -->
							<div class="event-tab">

								<div class="panel panel-default">
			       					<a href="eventdetail/<?php print $eObjID;?>">
			       						<img class="img-responsive center-cropped" src="<?php print $imageURL;?>">
			       					</a>

									<div class="panel-body">
										<h4><a href="/eventdetail/<?php print $eObjID;?>" style="text-transform: uppercase"><?php print $title;?>...</a></h4>
										<h5><i class="fa fa-map-marker"></i> <?php print $location;?></h5>
										<h6><i class="fa fa-hourglass-start"></i> <?php print $startDate;?><br>
										<i class="fa fa-hourglass-end"></i> <?php print $endDate;?></h6>
										<h5><i class="fa fa-ticket"></i> <?php print $cost;?></h5>
										<p class="eventlist-event-detail"><?php print $description;?>...</p>

                                        <p><a href="/editevent/<?php print $eObjID;?>" class="btn btn-primary btn-block">Upravit událost</a> </p>
                                        <p><a href="/eventdetail/<?php print $eObjID;?>" class="btn btn-primary btn-block">Zobrazit detaily</a> </p>
                                        <p><a href="/eventreviews/<?php print $eObjID;?>" class="btn btn-info btn-block">Zobrazit hodnocení</a> </p>
                                        <script>
                                        function deleteEvent(eventTitle, eventId) {
                                            var confirmation = confirm("Opravdu chcete smazat událost '" + eventTitle + "'?");
                                            if(confirmation == true) {
                                                window.location.href = "/eventdelete/" + eventId;
                                            }
                                        }
                                        </script>
                                        <p style="text-align: center;">
                                        <button type="button" class="btn btn-danger" onclick="deleteEvent('<?php print $title;?>', '<?php print $eObjID;?>')">Smazat událost</button>
                                        </p>
					        		</div><!-- end panel body -->
					      		</div>
							</div><!-- end Event cell -->
	<?php

    } // end FOR loop

    // error in query
} catch (ParseException $e) {echo $e->getMessage();}
?>
</div>
