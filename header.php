<?php
use Parse\ParseException;
use Parse\ParseQuery;
use Parse\ParseUser;
?>

    <html>

    <head>
        <base href="<?php print HTML_HEAD_BASE;?>">
        <meta charset="utf-8">
        <title><?php print HTML_HEAD_TITLE;?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php print HTML_HEAD_META_DESCRIPTION;?>">
        <meta name="author" content="viptrust.com">

        <!-- Favicon -->
        <link rel="shortcut icon" type="image/png" href="images/favicon.png" />

        <!-- Google Web Font -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,700,300italic' rel='stylesheet' type='text/css'>
        <link href="https://fonts.googleapis.com/css?family=Lato:400i,700i" rel="stylesheet">
        <!-- Bootstrap 3 Plan styles -->
        <link href="assets/css/bootstrap.main.css" rel="stylesheet">
        <!-- Plan UI -->
        <link href="assets/css/main.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/vendor/font-awesome.min.css">
        <!-- Vide JS -->
        <link href="assets/css/video-js.min.css" rel="stylesheet">
        <link href="assets/css/datepicker.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

        <![endif]-->
        <style>
            body {
                /* padding-top: 100px;  */
            }

            hr {
                margin: 80px 0;
            }

            /* Center and crop an image */

            .center-cropped-img {
                width: 100px;
                height: 100px;
                background-position: center center;
                background-repeat: no-repeat;
                object-fit: cover;
            }

            .center {
                position: absolute;
                left: 0;
                right: 0;
                margin: auto;
            }

            .user-loggeg-photo {
                width: 22px;
            }
        </style>
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({
                google_ad_client: "ca-pub-3256400094065711",
                enable_page_level_ads: true
            });

        </script>
        <script type="text/javascript">
            var clicked = false;

            function menuhover()
            {
                if(clicked)
                {

                    $(".navbar-toggle").removeClass("active-menu");



                }
                else
                {

                    $(".navbar-toggle").addClass("active-menu");

                }
                clicked = !clicked;

            }
        </script>
    </head>

<body>
<!--===================================
  JAVASCRIPT
=====================================-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-124469190-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-124469190-1');
</script>

<!-- jQuery -->
<script src="assets/js/jquery.js"></script>
<!-- Main Scripts-->
<script src="assets/js/bootstrap.min.js"></script>
<!-- Video -->
<script src="assets/js/video.js"></script>
<script src="assets/js/responsive.video.js"></script>
<!-- Plan UI elements -->
<script src="assets/js/plan.ui.js"></script>
<!-- PlaceHolder fallback -->
<script src="assets/js/jquery.placeholder.js"></script>
<!-- Demo -->
<script src="assets/js/application.js"></script>
<script src="assets/js/utills.js"></script>

<script type="text/javascript" src="assets/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="assets/js/locales/bootstrap-datepicker.cs.js"></script>

<div id="fb-root"></div>
<!-- FaceBook -->
<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/cs_CZ/sdk.js#xfbml=1&version=v2.12';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<script>
    window.onresize = checkSkyscraperAvailable;
</script>
<?php

//Search for categories
$categoriesQuery = new ParseQuery("Events");
$categoriesQuery->select("Categories");
$resultArray = $categoriesQuery->find();


//Search for banners
$banners = array();
try {
    $query = new ParseQuery("Banners");
    $query->equalTo("show", true);

    // Find objects
    $bannerArray = $query->find();
    for ($i = 0; $i < count($bannerArray); $i++) {
        // Get Parse Object
        $bannerObject = $bannerArray[$i];
        $banners[$bannerObject->get('type')] = $bannerObject;
    }
    // error in query
} catch (ParseException $e) {echo $e->getMessage();}

// Check if user is logged
$firstname = '';
$lastname = '';
$photo = '';
$organizer = false;
$userLogged = false;

try
{
    $currentUser = ParseUser::getCurrentUser();
    $userId = "empty";
    if ($currentUser && $currentUser->getObjectId()) {
        $firstname = $currentUser->get('firstname');
        $lastname = $currentUser->get('lastname');
        $photo = $currentUser->get('photo');
        if($currentUser->get('userrole') == 'organizer' || $currentUser->get('userrole') == 'admin')
            $organizer = true;
        $userLogged = true;
        $userId = $currentUser->getObjectId();
    }
} catch (ParseException $e) {
    echo $e->getMessage();
}

?>
<div class="menu">
    <div class="menu-inner content-width">
        <a href="index.php"><img src="./assets/img/logo.png" alt="menu-logo" class="menu-logo"></a>
        <div class="navbar-header" onclick="menuhover()">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex3-collapse">

            </button>
        </div>
        <nav class="menu-nav open-hover" role="navigation">

            <div class="collapse navbar-collapse navbar-ex3-collapse col-lg-12">

                <ul class="menu-list clearfix">
                    <li class="menu-list__item"><a data-toggle="modal" href="#contactModal" target="_self">Kontaktujte nás</a></li>

                    <script>
                        function userLoggedIn(firstname, lastname, photo, userrole) {
                            $("#loggedUser #loggedUserPhoto").attr('alt', firstname+' '+lastname);
                            $("#loggedUser #loggedUserPhoto").attr('src', photo);
                            $("#loggedUser #loggedUserName").html(firstname+' '+lastname);
                            $("#loggedUser").show();
                            $("#buttonLogout").show();
                            if(userrole == 'organizer') {
                                $("#buttonAddEvent").show();
                                $("#buttonEventAdministration").show();
                            }
                            //$("#buttonRegister").hide();
                            $("#buttonLogin").hide();
                        }

                        function userLoggedOut() {
                            $("#loggedUser").hide();
                            $("#buttonLogout").hide();
                            $("#buttonAddEvent").hide();
                            $("#buttonEventAdministration").hide();
                            $("#buttonRegister").show();
                            $("#buttonLogin").show();
                            $("#loggedUser #loggedUserPhoto").attr('alt', '');
                            $("#loggedUser #loggedUserPhoto").attr('src', );
                            $("#loggedUser #loggedUserName").html('');
                        }

                        function sendSearchRequest() {
                            window.history.pushState("", "", '');
                            $.ajax({
                                url: "index.php",
                                type: 'POST',
                                data: $("#searchForm").serialize(),
                                success: function (data) {

                                }, error: function (e) {

                                }
                            });
                        }
                    </script>


                    <li class="menu-list__item" id="buttonAddEvent" style="<?php print ($organizer)?'':'display:none'?>">
                        <a href="/eventnew">Přidat událost</a>
                    </li>
                    <li class="menu-list__item" id="loggedUser" style="<?php print ($userLogged)?'':'display:none'?>">
                        <a href="/profile">
                            <span id="loggedUserName">Profil</span>
                        </a>
                    </li>

                    <li class="menu-list__item" id="buttonLogout" style="<?php print ($userLogged)?'':'display:none'?>">
                        <a href="/logout">Odhlásit</a>
                        <div class="name">
                            <?php echo $firstname . " " . $lastname; ?>
                        </div>
                    </li>


                    <li class="menu-list__item" id="buttonLogin" style="<?php print ($userLogged)?'display:none':''?>">
                        <a data-toggle="modal" href="#loginModal">Přihlásit se</a>
                    </li>
                    <li class="menu-list__item" id="buttonRegister" style="<?php print ($userLogged)?'display:none':''?>">
                        <a data-toggle="modal" href="#registrationModal">Registrace</a>
                    </li>
                    <li class="menu-list__search-bar" id="searchBar">
                        <form style="display: block" role="search" action="index.php" id="searchForm" method="post">
                            <input type="text" id="autocomplete" name="searchLocation" class="input-search" placeholder="Zadejte adresu..." onfocus="geolocate()" onblur="searchFocusLost()">
                            <input type="hidden" name="cCity" id="locality">
                            <input type="hidden" name="cStreet" id="route">
                            <input type="hidden" name="cNumber" id="street_number">
                            <button onclick="sendSearchRequest()" class="input-search__button"><i class="fa fa-search"></i></button>
                            <script>

                                function searchFocusLost(){
                                    $('#searchForm').css('display','none');
                                    $('#addressLabel').css('display','block');
                                    $('#addressLbl').text('<?=$searchLocation?>');
                                    $("#searchBar").css('width','auto');
                                }

                                function changeAddress(){
                                    $('#searchForm').css('display','block');
                                    $('#addressLabel').css('display','none');
                                    $('#searchBar').css('width','300px');
                                    $('#autocomplete').focus();
                                }

                                document.getElementById('searchForm').addEventListener('keydown', function (e) {
                                    if (e.keyCode === 13) {
                                        e.preventDefault();
                                    }
                                });
                                // This example displays an address form, using the autocomplete feature
                                // of the Google Places API to help users fill in the information.

                                // This example requires the Places library. Include the libraries=places
                                // parameter when you first load the API. For example:
                                // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

                                var placeSearch, autocomplete;
                                var componentForm = {
                                    street_number: 'short_name',
                                    route: 'long_name',
                                    locality: 'long_name',
                                };

                                function initAutocomplete() {
                                    // Create the autocomplete object, restricting the search to geographical
                                    // location types.
                                    autocomplete = new google.maps.places.Autocomplete(
                                        /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                                        {
                                            types: ['geocode'],
                                            componentRestrictions: {country: "cz"}
                                        });

                                    // When the user selects an address from the dropdown, populate the address
                                    // fields in the form.
                                    autocomplete.addListener('place_changed', fillInAddress);
                                }

                                function fillInAddress() {
                                    // Get the place details from the autocomplete object.
                                    var place = autocomplete.getPlace();

                                    for (var component in componentForm) {
                                        document.getElementById(component).value = '';
                                    }
                                    // Get each component of the address from the place details
                                    // and fill the corresponding field on the form.
                                    for (var i = 0; i < place.address_components.length; i++) {
                                        var addressType = place.address_components[i].types[0];
                                        if (componentForm[addressType]) {
                                            var val = place.address_components[i][componentForm[addressType]];
                                            document.getElementById(addressType).value = val;
                                        }
                                    }
                                    $('#searchForm').submit();
                                }

                                // Bias the autocomplete object to the user's geographical location,
                                // as supplied by the browser's 'navigator.geolocation' object.
                                function geolocate() {
                                    if (navigator.geolocation) {
                                        navigator.geolocation.getCurrentPosition(function(position) {
                                            var geolocation = {
                                                lat: position.coords.latitude,
                                                lng: position.coords.longitude
                                            };
                                            var circle = new google.maps.Circle({
                                                center: geolocation,
                                                radius: position.coords.accuracy
                                            });
                                            autocomplete.setBounds(circle.getBounds());
                                        });
                                    }
                                }


                            </script>
                            <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA622CyBXd7CioMptGFQBghZeONMdzz96k&libraries=places&callback=initAutocomplete"></script>
                        </form>
                        <fieldset class="address" style="display: none" id="addressLabel">
                            <legend class="address-border">Hledáme</legend>
                                    <label class="address" id="addressLbl"></label>
                                    <a href="#" style="color: white; text-decoration: underline; margin-left:10px" onclick="changeAddress()">Změnit</a>
                        </fieldset>
                    </li>

                </ul>
            </div>
        </nav>
    </div>
</div>
<!-- <nav class="navbar navbar-default navbar-fixed-top open-hover" role="navigation">

        <!-- Brand and toggle get grouped for better mobile display
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex3-collapse">
            <span class="sr-only">Navigace</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"> UDÁLOSTI</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling
        <div class="collapse navbar-collapse navbar-ex3-collapse col-sm-6">
          <ul class="nav navbar-nav">
            <li>
              <a href="/">
                <i class="fa fa-bars"></i> Hlavní strana</a>
            </li>
            <li id="buttonAddEvent" style="<?php /*print ($userLogged && $organizer)?'':'display:none'*/?>">
              <a href="/eventnew">
                <i class="fa fa-plus-circle"></i> Přidat událost</a>
            </li>

            <li>
              <a data-toggle="modal" href="#contactModal">
                <i class="fa fa-paper-plane"></i> Kontaktujte nás</a>
            </li>
         <li id="loggedUser" style="<?php /*print ($userLogged)?'':'display:none'*/?>">
                <a href="">
                  <img id="loggedUserPhoto" class="user-loggeg-photo" src="<?php /*print $photo;*/?>" alt="<?php /*print $firstname.' '.$lastname;*/?>" />
                  <span id="loggedUserName"><?php /*print $firstname. " " .$lastname;*/?></span>
                </a>
              </li>

              <li id="buttonLogout" style="<?php /*print ($userLogged)?'':'display:none'*/?>">
                <a href="/logout">
                  <i class="fa fa-sign-out"></i> Odhlásit</a>
              </li>

                <li id="buttonRegister" style="<?php /*print ($userLogged)?'display:none':''*/?>">
                  <a data-toggle="modal" href="#registrationModal">
                    <i class="fa fa-user-plus"></i> Registrovat se</a>
                </li>

                <li id="buttonLogin" style="<?php /*print ($userLogged)?'display:none':''*/?>">
                  <a data-toggle="modal" href="#loginModal">
                    <i class="fa fa-sign-in"></i> Přihlásit</a>
                </li>
          </ul>

          <!-- Search events by keywords form
          <form class="navbar-form navbar-right" action="index.php" role="search">
            <div class="form-group">
              <input type="text" name="searchLocation" class="form-control" placeholder="Hledání událostí">
            </div>
          </form>

        </div>


      </nav>-->
<!-- END NAVBAR -->



<?php
require_once "./modal-loading.php";
require_once "./modal-login.php";
require_once "./modal-contactus.php";
require_once "./modal-registration.php";
?>