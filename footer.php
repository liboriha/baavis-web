  <hr>
    <!-- FOOTER -->
    <footer class="footer">
        <div class="footer-inner content-width">
            <div class="footer-links">
                <ul class="footer-site-map-list clearfix">
                <li class="footer-site-map-list__item"><a data-toggle="modal" href="#contactModal" target="_self">Kontaktujte nás</a></li>

                <script>
                    function userLoggedIn(firstname, lastname, photo, userrole) {
                        $("#loggedUser #loggedUserPhoto").attr('alt', firstname+' '+lastname);
                        $("#loggedUser #loggedUserPhoto").attr('src', photo);
                        $("#loggedUser #loggedUserName").html(firstname+' '+lastname);
                        $("#loggedUser").show();
                        $("#buttonLogout").show();
                        if(userrole == 'organizer') {
                            $("#buttonAddEvent").show();
                            $("#buttonEventAdministration").show();
                        }
                        $("#buttonRegister").hide();
                        $("#buttonLogin").hide();
                    }

                    function userLoggedOut() {
                        $("#loggedUser").hide();
                        $("#buttonLogout").hide();
                        $("#buttonAddEvent").hide();
                        $("#buttonEventAdministration").hide();
                        $("#buttonRegister").show();
                        $("#buttonLogin").show();
                        $("#loggedUser #loggedUserPhoto").attr('alt', '');
                        $("#loggedUser #loggedUserPhoto").attr('src', );
                        $("#loggedUser #loggedUserName").html('');
                    }
                </script>



                <li class="footer-site-map-list__item" id="buttonLogout" style="<?php print ($userLogged)?'':'display:none'?>">
                    <a href="/logout">Odhlásit</a>
                </li>
                    <li class="footer-site-map-list__item" id="loggedUser" style="<?php print ($userLogged)?'':'display:none'?>">
                        <a href="">
                            <span id="loggedUserName">Profil</span>
                        </a>
                    </li>
                <li class="footer-site-map-list__item" id="buttonRegister" style="<?php print ($userLogged)?'display:none':''?>">
                    <a data-toggle="modal" href="#registrationModal">Registrovat se</a>
                </li>

                <li class="footer-site-map-list__item" id="buttonLogin" style="<?php print ($userLogged)?'display:none':''?>">
                    <a data-toggle="modal" href="#loginModal">Přihlásit</a>
                </li>
                </ul>
            <span class="footer-terms"><a href="/assets/files/obchodni-podminky.pdf" target="_blank">Zásady ochrany osobních údajů</a>  |  <a href="#" target="_self">Podmínky poskytovaní sluzby</a> | <a href="#" id="provozovatel" target="_self">Provozovatel</a></span>
        </div>
        <div class="footer-info">
            <a href="#" data-toggle="tooltip" data-html="true" 
                title="<p>Pavel Hunal</p>
                <p>Kubelíkova 1224/42</p>
                <p>130 00 Praha 3</p>
                <p>IČO: 49055798</p>
                <p>DIČ: CZ 6704111139</p>
                <p>email:baavis.com@gmail.com</p>
                <p>mobil: +420 602 259 170</p>">
                Provozovatel
            </a>
        </div>
        
        <a href="index.php" target="_self"><img src="./assets/img/logo.png" alt="footer-logo" class="footer-logo"></a>
            <ul class="footer-social">
                <li class="menu-list__social-buttons">
                    <span class="social-button"><a href="#" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></span>
                    <span class="social-button"><a href="#" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></span>
                    <span class="social-button"><a href="#" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></span>
                </li>
            </ul>
        </div>
    </footer>
  <script>
      $(document).ready(function() {
          $("#provozovatel").click(function(){
              alert("Fakturační údaje \n Pavel Hunal \n Kubelíkova 1224/42 \n 130 00 Praha 3 \n IČO: 49055798 \n DIČ: CZ 6704111139 \n email: info@baavis.com \n mobil: +420 602 259 170");

          });
      });
  </script>

</body>
</html>