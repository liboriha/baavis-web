

$(document).ready(function () {
    checkSkyscraperAvailable();
});

function checkSkyscraperAvailable()
{
    var mainHeight = $("#mainContent").height();
    var mainWidth = $("#mainContent").width();
    if (mainHeight>650 && mainWidth>767)
    {
        $("#left_skyscraper").show();
        $("#right_skyscraper").show();
    }
    else
    {
        $("#left_skyscraper").hide();
        $("#right_skyscraper").hide();
    }
}

function getQueryVariable(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}

function dateRange(startDate, endDate) {
    var start      = startDate.split('-');
    var end        = endDate.split('-');
    var startYear  = parseInt(start[0]);
    var endYear    = parseInt(end[0]);
    var dates      = [];

    for(var i = startYear; i <= endYear; i++) {
        var endMonth = i != endYear ? 11 : parseInt(end[1]) - 1;
        var startMon = i === startYear ? parseInt(start[1])-1 : 0;
        for(var j = startMon; j <= endMonth; j = j > 12 ? j % 12 || 11 : j+1) {
            var month = j+1;
            var displayMonth = month < 10 ? '0'+month : month;
            dates.push([i, displayMonth, '01'].join('-'));
        }
    }
    return dates;
}