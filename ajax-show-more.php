<?php
use Parse\ParseException;
use Parse\ParseGeoPoint;
use Parse\ParseQuery;

/* Variables */
$banners = array();
    $query = new ParseQuery("Banners");
    $query->equalTo("show", true);
    
    // Find objects
    $bannerArray = $query->find();
    for ($i = 0; $i < count($bannerArray); $i++) {
        // Get Parse Object
        $bannerObject = $bannerArray[$i];
        $banners[$bannerObject->get('type')] = $bannerObject;
    }

$searchLocation = $_POST['searchLocation'];
if(empty($searchLocation))
    $searchLocation = $_GET['searchLocation'];
$category = "";
$currDate = new DateTime();
$skipCount= 0;

if(isset($_POST['loadMore'])) {
    $pageNumber = $_POST['pageNumber'];
    $skipCount = $_POST['eventsCount'];
}

// QUERY EVENTS -----------------------------------
try {
    if(isset($_POST["category"]))
        $category = $_POST["category"];
    $lng = null;
    $lat = null;
    // Find premium
    $queryPremiumSpots = new ParseQuery("Events");
    $queryPremiumSpots->equalTo("premiumSpotPayed", true);
    $queryPremiumSpots->greaterThanOrEqualTo("endDate", $currDate);

    if(!empty($category))
        $queryPremiumSpots->ascending("categoryPremiumSpot");
    else
        $queryPremiumSpots->ascending("premiumSpot");

    if (!is_null($searchLocation) && !empty($searchLocation)) {
        $url = str_replace('<ADDRESS>', urlencode($searchLocation), GEOCODING_API_URL);
        $response = file_get_contents($url);
        $json = json_decode($response, true);
        $lat = $json['results'][0]['geometry']['location']['lat'];
        $lng = $json['results'][0]['geometry']['location']['lng'];
        $city = $_POST['cCity'];
        $street = $_POST['cStreet'];
    }
    if(!empty($category))
    {
        $catFilter = new ParseQuery("EventCategories");
        $catFilter->equalTo("name",$category);
        $categoryArr = $catFilter->find();
    }

    if ($lat && $lng) {
        try {
            $searchGeoPoint = new ParseGeoPoint($lat, $lng);
            $queryPremiumSpots->withinKilometers('locationGps', $searchGeoPoint, 0);
        }
        catch (Exception $e) {
        }
    }

    if($categoryArr)
    {
        $queryPremiumSpots->equalTo("categories",$categoryArr[0]);
    }
    $evPremiumArray = $queryPremiumSpots->find();
    $evPremiumIDArray = [];
    $premiumCount = count($evPremiumArray);
    for($i=0;$i < $premiumCount;$i++) {
        $eObj = $evPremiumArray[$i];
        $isPremium = false;

        $premiumMonthsArr = $eObj->get('paidPremium');
        if (($premiumForCat && !empty($category)) || (!$premiumForCat && empty($category))) {
            for ($j = 0; $j < count($premiumMonthsArr); $j++) {
                $premiumMonth = date("M-yyyy", strtotime($premiumMonthsArr[$j]));
                $currentMonth = $currDate->format("M-yyyy");

                if ($premiumMonth == $currentMonth) {
                    $premiumForCat = $evPremiumArray[$i]->get('premiumSpotForCategory');
                    $evPremiumArray[$i]->set('showAsPremium', true);
                    $evPremiumIDArray[] = $evPremiumArray[$i]->getObjectId();
                    $isPremium = true;
                }
                break;
            }
            if(!$isPremium)
                unset($evPremiumArray[$i]);
        }
    }
    $evPremiumArrayByLocation=[];
    //Find premium spots by location(not gps)
    if((!empty($city) && !is_null($city)) || (!empty($street) && !is_null($street))) {
        $queryPremiumSpotsByLocationName = new ParseQuery("Events");
        if (!empty($city))
            $queryPremiumSpotsByLocationName->contains("city", $city);
        if (!empty($street))
            $queryPremiumSpotsByLocationName->contains("street", $street);
        $queryPremiumSpotsByLocationName->equalTo("premiumSpotPayed", true);
        $queryPremiumSpotsByLocationName->lessThanOrEqualTo("premiumSpotStartDate", $currDate);
        $queryPremiumSpotsByLocationName->greaterThanOrEqualTo("premiumSpotEndDate", $currDate);
        $queryPremiumSpotsByLocationName->greaterThanOrEqualTo("endDate", $currDate);
        $queryPremiumSpotsByLocationName->ascending("premiumSpot");
        $queryPremiumSpotsByLocationName->notContainedIn("objectId", $evPremiumIDArray);

        if ($categoryArr) {
            $queryPremiumSpotsByLocationName->equalTo("categories", $categoryArr[0]);
        }
        $evPremiumArrayByLocation = $queryPremiumSpotsByLocationName->find();
        $premiumCount = count($evPremiumArrayByLocation);
        for ($i = 0; $i < $premiumCount; $i++) {
            $premiumForCat = $evPremiumArrayByLocation[$i]->get('premiumSpotForCategory');

            if (($premiumForCat && !empty($category)) || (!$premiumForCat && empty($category))) {
                $evPremiumArrayByLocation[$i]->set('showAsPremium', true);
                $evPremiumIDArray[] = $evPremiumArrayByLocation[$i]->getObjectId();
            } else {
                unset($evPremiumArrayByLocation[$i]);
            }
        }
    }

    // Find non premium
    $queryNonPremium = new ParseQuery("Events");
    $queryNonPremium->notContainedIn("objectId", $evPremiumIDArray);
    $queryNonPremium->ascending("startDate");
    $queryNonPremium->greaterThanOrEqualTo("endDate", $currDate);
    if (EVENT_MUST_BE_ACCEPTED_BY_ADMIN) {
        $queryNonPremium->equalTo("isPending", false);
    }
    if ($lat && $lng) {
        try {
            $searchGeoPoint = new ParseGeoPoint($lat, $lng);
            $queryNonPremium->withinKilometers('locationGps', $searchGeoPoint, 0);
        }
        catch (Exception $e) {
        }
    }
    if($categoryArr) {
        $queryNonPremium->equalTo("categories", $categoryArr[0]);
    }

    $evCount = 0;
    if(isset($pageNumber)) {
        if ($pageNumber != 0) {
            $queryNonPremium->skip($skipCount - count($evPremiumIDArray) + $skipCount * $pageNumber);
            $evCount = $skipCount - count($evPremiumIDArray) + $skipCount * $pageNumber;
        }
        else {
            $queryNonPremium->skip($skipCount - count($evPremiumIDArray));
            $evCount = $skipCount - count($evPremiumIDArray);
        }
    }

    $evNonPremiumArray = $queryNonPremium->find();
    $evNonPremiumIdArray = [];
    for($i = 0;$i<count($evNonPremiumArray); $i++)
    {
        $evNonPremiumIdArray[] = $evNonPremiumArray[$i]->getObjectId();
    }
    $evNonPremiumIdArray = array_merge($evNonPremiumIdArray,$evPremiumIDArray);
    //Find Non-premium by location

    $evNonPremiumArrayByLocation = [];
    if(!empty($city) || !empty($street)) {
        $queryNonPremiumByLocation = new ParseQuery("Events");
        $queryNonPremiumByLocation->notContainedIn("objectId", $evNonPremiumIdArray);
        $queryNonPremiumByLocation->ascending("startDate");
        $queryNonPremiumByLocation->greaterThanOrEqualTo("endDate", $currDate);
        if (!empty($city))
            $queryNonPremiumByLocation->contains("city", $city);
        if (!empty($street))
            $queryNonPremiumByLocation->contains("street", $street);
        if (EVENT_MUST_BE_ACCEPTED_BY_ADMIN) {
            $queryNonPremiumByLocation->equalTo("isPending", false);
        }

        if ($categoryArr) {
            $queryNonPremiumByLocation->equalTo("categories", $categoryArr[0]);
        }

        if(isset($pageNumber)) {
            if ($pageNumber != 0)
                $queryNonPremiumByLocation->skip($skipCount - count($evPremiumIDArray) + $skipCount*$pageNumber);
            else
                $queryNonPremiumByLocation->skip($skipCount - count($evPremiumIDArray));
        }
        $evNonPremiumArrayByLocation = $queryNonPremiumByLocation->find();
    }

    if(AJAX_REQUEST && isset($_POST['loadMore']))
        $evArray = $evNonPremiumArray;
    else {
        $evArray = array_merge($evPremiumArray, $evPremiumArrayByLocation);
        $evArray = array_merge($evArray, $evNonPremiumArray);
        $evArray = array_merge($evArray, $evNonPremiumArrayByLocation);
    }

    if(empty($evArray) || (is_null($lat) && is_null($lng) && !empty($searchLocation) && !is_null($searchLocation)))
    {
        $queryNonPremium = new ParseQuery("Events");
        $queryNonPremium->notContainedIn("objectId", $evPremiumIDArray);
        $queryNonPremium->ascending("startDate");
        $queryNonPremium->greaterThanOrEqualTo("endDate", $currDate);
        if (EVENT_MUST_BE_ACCEPTED_BY_ADMIN) {
            $queryNonPremium->equalTo("isPending", false);
        }

        if($categoryArr) {
            $queryNonPremium->equalTo("categories", $categoryArr[0]);
        }

        if(isset($pageNumber)) {
            if ($pageNumber != 0)
                $queryNonPremium->skip($skipCount - count($evPremiumIDArray) + $skipCount*$pageNumber);
            else
                $queryNonPremium->skip($skipCount - count($evPremiumIDArray));
        }

        $evNonPremiumArray = $queryNonPremium->find();
        $evArray = $evNonPremiumArray;

    }

    if(count($evArray)>2)
        $count = $skipCount;
    else
        $count = count($evArray);

    for ($i = 0; $i < $count; $i++) {
        // Get Parse Object
        $eObj = $evArray[$i];
        if(!isset($eObj))
            continue;
        $eObjID = $eObj->getObjectId();

        // Get image
        $file = $eObj->get('image');
        $imageURL = $file->getURL();
        // Get title
        $title = $eObj->get('title');
        $title = substr($title, 0, 25);

        // Get location
        $location = $eObj->get('location');

        // Get cost
        $cost = $eObj->get('cost');

        // Get start date
        $sDate = $eObj->get('startDate');
        $startDate = date_format($sDate, "d.m.Y h:i\h");

        // Get end date
        $eDate = $eObj->get('endDate');
        $endDate = date_format($eDate, "d.m.Y h:i\h");

        $cat = $eObj->get('categories');

        // Get description
        $description = $eObj->get('description');
        $description = substr($description, 0, 80);

        $likesCount = $eObj->get("likes");

        $liked = false;

        $currentUser = \Parse\ParseUser::getCurrentUser();
        if(isset($currentUser)) {
            $query = new ParseQuery("UserEventLikes");
            $query->equalTo("user_id", $currentUser->getObjectId());
            $query->equalTo("event_id", $eObj->getObjectId());

            $result = $query->find();

            if (count($result) > 0) {
                $liked = true;
            }
        }

        if($i+$evCount%4==0 && $i+$evCount!=0)
        {
            ?>
            <!-- Advertisement cell -->
            <div class="event-tab">
            <?php
                    if (isset($banners['leaderboard-top'])) {
                        echo '<a href="//' . $banners['leaderboard-top']->get('link') . '"><img src="' . $banners['leaderboard-top']->get('image')->getUrl() . '" alt="' . $banners['leaderboard-top']->get('alternative_text') . '" /></a>';
                    }?>
            </div><!-- end cell -->
        <? }
        ?>

							<!-- Event cell -->
							<div class="<?php print ($eObj->get('showAsPremium'))? 'premium-event ': ' '; ?> event-tab">
                                <div class="premium-img"></div>
                                <img class="event-tab__image" src="<?php print $imageURL;?>">
                                <div class="event-tab-center">
									<h3 class="event-tab-center__title"><?php print $title;?></h3>
                                    <p class="event-tab-center__location">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i><?php print ' '.$location.' ';?>
                                       <span class="event-tab-center__time">
                                           <i class="fa fa-clock-o"></i><?php print ' '.$startDate.' ';?> -
                                           <i class="fa fa-clock-o"></i><?php print ' '.$endDate;?>
                                       </span>
                                    </p>
									<p class="event-tab-center_p"><?php print $description;?></p>
                                </div>
                                <div class="event-tab-right">
                                    <a href="/eventdetail/<?php print $eObjID;?>"><button class="universal-button universal-button--red">Zobrazit detaily</button></a>
                                    <div class="clearfix"></div>
                                    <button class="btn btn-primary" style="margin: 3px" data-eventId="<?=$eObjID?>" data-likes="<?=$likesCount?>" data-like="<?=$liked?'true':'false'?>" onclick="likeBtnClick(this)"><i class="fa <?= $liked?'fa-check':'fa-heart-o'?>"></i> <?=$likesCount?></button>
                                </div><!-- end Event cell -->
                            </div>
<?php
    } // end FOR loop

    // error in query
} catch (ParseException $e) {echo $e->getMessage();}?>