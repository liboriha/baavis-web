
<?php
use Parse\ParseException;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseUser;

/* Variables */
$eObjID = $params[0];

$eObj = new ParseObject("Events", $eObjID);
$eObj->fetch();

// If revieved, save
if ($action == 'attendantreviewsave') {
  try {
      // Check if logged user is between event attenders
      $query = new ParseQuery('Events');
      $query->equalTo('attenders', ParseUser::getCurrentUser());
      $events = $query->find();
      if(count($events) != 1) {
        print '<script>alert(\'Nastal problém při ukládání Vašeho hodnocení. Prosím, zkuste to později.\');</script>';
        return;  
      }

      $review = new ParseObject('Reviews');
      $review->set('event', $events[0]);
      $review->set('reviewer', ParseUser::getCurrentUser());
      $review->set('stars', intval($_POST['cStars']));
      $review->set('comment', $_POST['cMessage']);
      $review->save();

      echo '
    <div class="text-center">
        <div class="alert alert-success">Váš komentář byl úspěšně uložen.</div>
    </div>';
  } catch (ParseException $ex) {
      print '<script>alert(\'Nastal problém při ukládání Vašeho hodnocení. Prosím, zkuste to později.\');</script>';
  }
}

function TranslateMonth($monthNumber)
{
    $months = ['Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosinec'];
    return $months[$monthNumber];
}

// Get parse Object by its objectID
$eObj = new ParseObject("Events", $eObjID);
$eObj->fetch();

// Get title
$title = $eObj->get('title');

// Get image
$file = $eObj->get('image');
$imageURL = $file->getURL();

// Get cost
$cost = $eObj->get('cost');

// Get start date
$sDate = $eObj->get('startDate');
$startDate = date_format($sDate, "d ") . TranslateMonth(intval(date_format($sDate, "m"))) . date_format($sDate, " Y | @H:i A");
$startDateString = date_format($sDate, "dmYHi");
$startDateForCalendar = date_format($sDate, "Ymd\THis\Z");

// Get end date
$eDate = $eObj->get('endDate');
$endDate = date_format($eDate, "d ") . TranslateMonth(intval(date_format($eDate, "m"))) . date_format($eDate, " Y | @H:i A");
$endDateString = date_format($eDate, "dmYHi");
$endDateForCalendar = date_format($eDate, "Ymd\THis\Z");

// Get month, day and year of the Start Date
$month = date_format($sDate, "m");
$day = date_format($sDate, "d");
$year = date_format($sDate, "Y");

// Get website
$website = $eObj->get('website');

// Get description
$description = $eObj->get('description');

// Get location
$location = $eObj->get('location');
$locationNoSpaces = preg_replace('/\s+/', '+', $location);
?>
      <!-- TITLE -->
      <div class="text-center">
        <h3><?php print $title;?></h4>
       </div>
      <br>
      <br>

      <!-- EVENT IMAGE -->
      <div class="thumbnail col-sm-offset-1 col-sm-10">
        <img src="<?php print $imageURL;?>" alt="<?php print $title;?>">
      </div>

      <!-- DESCRIPTION -->
      <div class="col-lg-8 col-sm-offset-1 col-sm-6">
      <p>
<?php
$descArray = preg_split('/\n+/', $description);
for ($i = 0; $i < count($descArray); $i++) {
    echo $descArray[$i] . '<br>';
}
?>
       </p>
       <br>

       <form id="formReview" class="form-horizontal" action="/attendantreviewsave/<?php print $eObjID;?>" method="post">


<!-- STARS -->
<div class="form-group">
  <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
    <br>
    <i class="fa fa-star-o clickable-review-star" star="1" style="color:#fdd600;"></i>
    <i class="fa fa-star-o clickable-review-star" star="2" style="color:#fdd600;"></i>
    <i class="fa fa-star-o clickable-review-star" star="3" style="color:#fdd600;"></i>
    <i class="fa fa-star-o clickable-review-star" star="4" style="color:#fdd600;"></i>
    <i class="fa fa-star-o clickable-review-star" star="5" style="color:#fdd600;"></i>

  <input type="hidden" name="cStars" value="0" />
  <script>
    $(".clickable-review-star").on("click", function () {
     if($(this).hasClass("fa-star-o")) {
       $("#formReview input[name='cStars']").val(0);
       for(var i=1;i <= $(this).attr("star");i++) {
        $("[star='"+i+"']").addClass("fa-star");
        $("[star='"+i+"']").removeClass("fa-star-o");
        $("#formReview input[name='cStars']").val(parseInt($("#formReview input[name='cStars']").val()) + 1);
       }
     }
     else if($(this).hasClass('fa-star')) {
      for(var i=5;i >= 1;i--) {
        $("[star='"+i+"']").addClass("fa-star-o");
        $("[star='"+i+"']").removeClass("fa-star");
       }
       $("#formReview input[name='cStars']").val(0);
    }
    });
    </script>
  </div>
  </div>


<!-- MESSAGE -->
<div class="form-group">
  <div class="col-lg-10 col-sm-10 col-md-10 col-lg-offset-1">
    <strong>KOMENTÁŘ</strong>
    <br>
    <textarea class="form-control" rows="3" name="cMessage" placeholder="Komentář"></textarea>
  </div>
  </div>


<!-- SEND MESSAGE BUTTON -->
<div class="form-group">
    <div class="text-center">
      <button type="submit" class="btn btn-primary">ODESLAT HODNOCENÍ</button>
    </div>
</div>

</form>

      </div>