<?php
use Parse\ParseException;
use Parse\ParseQuery;
use Parse\ParseUser;

/* Variables */
$ownerObject = ParseUser::getCurrentUser();

if(empty($params[0])) {
    echo '<script>alert("Nastala chyba při vykonávání operace. Opakujte, prosím, akci později.");</script>';
}
else {
// QUERY REVIEWS -----------------------------------
try {
    $query = new ParseQuery('Events');
    $query->equalTo('owner', $ownerObject);
    $query->equalTo('objectId', $params[0]);
    $events = $query->find();
    if(count($events) != 1) {
        echo '<script>alert("Událost nebyla nalezena.");</script>';
        return;
    }

    $event = $events[0];

    $query = new ParseQuery('Reviews');
    $query->equalTo('event', $event);
    $query->descending('createdAt');

    // Find objects
    $reviews = $query->find();

    for ($i = 0; $i < count($reviews); $i++) {
        // Get Parse Object
        $eObj = $reviews[$i];
        $eObjID = $eObj->getObjectId();

        // Get reviewer
        $reviewer = $eObj->get('reviewer');
        $reviewer->fetch();

        // Get date and time
        $datetime = date_format($eObj->getCreatedAt(), " d.m.Y h:i\h");

        // Get stars
        $stars = $eObj->get('stars');

        // Get comment
        $comment = $eObj->get('comment');
        ?>

							<!-- Event cell -->
							<div class="col-lg-12 col-sm-12">
                            
								<div class="panel panel-default">
			       				
									<div class="panel-body">
										<h4>
                                        <?php
                                        for($i=1;$i<=5;$i++) {
                                            if($i <= $stars) print "<i class=\"fa fa-star\" style=\"color:#fdd600;\"></i>";
                                            else print "<i class=\"fa fa-star-o\" style=\"color:#fdd600;\"></i>";
                                        }
                                        ?>
                                        </h4>
										<h5><?php print $reviewer->get('firstname').' '.$reviewer->get('lastname');?></h5>
                                        <h6><i class="fa fa-hourglass-start"></i><?php print $datetime;?><br>
										<p><?php print $comment;?>...</p>

					        		</div><!-- end panel body -->
					      		</div>
							</div><!-- end Event cell -->
	<?php

    } // end FOR loop

    // error in query
} catch (ParseException $e) {echo $e->getMessage();}
}
?>